#version 410

in vec3 fragNormal;
in vec3 fragVertex;
in vec2 fragTexCoord;
out vec4 frag_color;

uniform sampler2D testTexture;

void main ()
{
	vec4 mytextureColor = texture(testTexture, fragTexCoord);
	
	vec3 normal = normalize(fragNormal);
	
	vec3 L = normalize(vec3(0.0, .0, 10.0) - fragVertex);
	vec3 E = normalize(-fragVertex);
	vec3 H = normalize(L + E);
	//vec4 ambientTerm = vec4(0.55, 0.77, 0.25, 1.0);
	vec4 diffuseTerm = vec4(0.05, 0.05, 0.05, 1) * clamp(dot(normal, L), 0.0, 1.0);
	//vec4 ambientTerm = vec4(0.0, 0.3, 0.0, 1.0);
	diffuseTerm = clamp(diffuseTerm, 0.0, 1.0);
	
	float fresnel = pow(1.0 - max(0.0, dot(H, E)), 5.0);
	
	float specAngle = max(dot(H, normal), 0.0);
	float specular = pow(specAngle, 0.05 * 4000);
	frag_color = mytextureColor + diffuseTerm + mix(vec4(0.5, 0.5, 0.5, 1.0), vec4(1.0), fresnel) * specular;
	//frag_color = mytextureColor + diffuseTerm + vec4(0.4, 0.4, 0.4, 1.0) * specular;
}
