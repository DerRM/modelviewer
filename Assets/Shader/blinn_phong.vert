#version 410

in vec4 position;
in vec3 normal;
in vec2 texCoord;
in vec4 boneIndex;
in vec4 boneWeight;
out vec3 fragNormal;
out vec3 fragVertex;
out vec2 fragTexCoord;
uniform mat4 modelMatrix;
uniform mat4 bones[44];
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 normalMatrix;

void main()
{
	fragVertex = vec3(viewMatrix * modelMatrix * position);
	fragNormal = vec3((normalMatrix * vec4(normal, 0.0)));
	fragTexCoord = texCoord;

	vec4 transform_pos = ((bones[int(boneIndex.x)] * position) * boneWeight.x) +
						 ((bones[int(boneIndex.y)] * position) * boneWeight.y) + 
						 ((bones[int(boneIndex.z)] * position) * boneWeight.z) +
						 ((bones[int(boneIndex.w)] * position) * boneWeight.w);

	gl_Position = projectionMatrix * viewMatrix * modelMatrix * transform_pos;// * position;
}
