#version 410

out vec4 frag_color;

uniform vec4 out_color;

void main()
{
	frag_color = out_color;
}
