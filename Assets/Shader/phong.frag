#version 450

in vec3 fragNormal;
in vec3 fragVertex;
in vec2 fragTexCoord;
out vec4 frag_color;

uniform sampler2D testTexture;

void main ()
{
	vec4 mytextureColor = texture2D(testTexture, fragTexCoord);
	
	vec3 normal = normalize(fragNormal);
	
	vec3 L = normalize(vec3(0.0, .0, 10.0) - fragVertex);
	vec3 E = normalize(-fragVertex);
	vec3 R = normalize(-reflect(L, normal));
	//vec4 ambientTerm = vec4(0.55, 0.77, 0.25, 1.0);
	vec4 diffuseTerm = vec4(0.1, 0.1, 0.1, 1) * clamp(dot(normal, L), 0.0, 1.0);
	//vec4 ambientTerm = vec4(0.0, 0.3, 0.0, 1.0);
	diffuseTerm = clamp(diffuseTerm, 0.0, 1.0);
	vec4 specularTerm = vec4(0.4,0.4,0.4,1.0) * pow(max(dot(R,E), 0.0), 0.05* 1000);
	specularTerm = clamp(specularTerm, 0.0, 1.0);  
	frag_color = mytextureColor + diffuseTerm + specularTerm;
}