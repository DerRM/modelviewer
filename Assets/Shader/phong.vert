#version 450

in vec4 position;
in vec3 normal;
in vec2 texCoord;
out vec3 fragNormal;
out vec3 fragVertex;
out vec2 fragTexCoord;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 normalMatrix;

void main()
{
	fragVertex = vec3(viewMatrix * modelMatrix * position);
	fragNormal = vec3((normalMatrix * vec4(normal, 0.0)));
	fragTexCoord = texCoord;
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;
}