#version 100

attribute vec4 position;
attribute vec3 normal;
attribute vec2 texCoord;
attribute vec4 boneIndex;
attribute vec4 boneWeight;
varying vec3 fragNormal;
varying vec3 fragVertex;
varying vec2 fragTexCoord;
uniform mat4 modelMatrix;
uniform mat4 bones[44];
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 normalMatrix;

void main()
{
	fragVertex = vec3(viewMatrix * modelMatrix * position);
	fragNormal = vec3((normalMatrix * vec4(normal, 0.0)));
	fragTexCoord = texCoord;
	
	vec4 transform_pos = ((bones[int(boneIndex.x)] * position) * boneWeight.x) +
						 ((bones[int(boneIndex.y)] * position) * boneWeight.y) + 
						 ((bones[int(boneIndex.z)] * position) * boneWeight.z) +
						 ((bones[int(boneIndex.w)] * position) * boneWeight.w);
	
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * transform_pos;
}