#pragma once

#include "Common.h"
#include "Parser.h"

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_set>
#include <map>
#include <limits>

#include <cstdio>
#include <cstring>
#include <cfloat>


struct equstr
{
    bool operator()(const char* s1, const char* s2) const
    {
        return strcmp(s1, s2) == 0;
    }
};

class ObjParser : public Parser
{
public:
    ObjParser();
    ~ObjParser();

    Bool32 openFile(const char* pFilename);
    void readFile();
    void closeFile();

    PositionArray getPositionArray() { return m_positionArray; }
    NormalArray getNormalArray() { return m_normalArray; }
    TexCoord2Array getTexCoordArray() { return m_texCoordArray; }
    IndexArray getIndexArray() { return m_indexArray; }

    chs::Vector3 getMaxVector() { return m_maxVector; }
    chs::Vector3 getMinVector() { return m_minVector; }

private:
    std::ifstream* m_file;
    Int32 m_vertexCount;
    Int32 m_texCoordCount;
    Int32 m_normalCount;
    Int32 m_faceCount;
    Int32 m_currentIndex;

    chs::Vector3 m_minVector;
    chs::Vector3 m_maxVector;

    chs::Vector3 parseVector3(StringArray& stringArray);
    Vector2 parseVector2(StringArray& stringArray);
    void parseFace(StringArray& stringArray);

    PositionArray m_positionArray;
    TexCoord2Array m_texCoordArray;
    NormalArray m_normalArray;
    IndexArray m_indexArray;
    StringArray m_stringArray;

    PositionArray m_tempPositionArray;
    TexCoord2Array m_tempTexCoordArray;
    NormalArray m_tempNormalArray;

    std::map<std::string, Uint32> m_stringIndexHashSet;
    bool m_hasNormals;

    std::unordered_set<std::string> m_hashSet;

    bool contains(const std::unordered_set<std::string>& hashSet, std::string& word);

    void finalizeData();
};

