#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <vector>
#include <assert.h>

#ifdef __ANDROID__
#include <android_native_app_glue.h>
#endif

#include "Math/Vector3.h"
#include "Math/Vector4.h"
#include "Math/Matrix3x3.h"
#include "Math/Matrix4x4.h"
#include "Math/Quaternion.h"

#define OPENGL_45
//#define OPENGLES_2
//#define OPENGLES_2

#if defined (OPENGL_45)
#include "Renderer/OpenGL45.h"
#elif defined (OPENGLES_2)
#include "Renderer/OpenGLES2.h"
#elif defined (OPENGLES_31)
#include "Renderer/OpenGLES31.h"
#endif

typedef unsigned int Bool32;
typedef float Float32;
typedef double Float64;
typedef char Int8;
typedef unsigned char Uint8;
typedef short Int16;
typedef unsigned short Uint16;
typedef unsigned int Uint32;
typedef int Int32;
typedef unsigned long long Uint64;
typedef long long Int64;

typedef struct
{
	float x;
	float y;
} Vector2;

//typedef struct
//{
//	std::vector<int> vertexIndices;
//	std::vector<int> normalIndices;
//	std::vector<int> texCoordIndices;
//} Face;

//typedef struct
//{
//	std::vector<Face> faces;
//} SubMesh;

void Log(const char* format, ...);
void LogRelease(const char* format, ...);
Uint64 hashString(const char* string);
Bool32 isDelimiter(const char c, const std::string& delimiters);
std::vector<std::string> tokenizeString(const std::string& string, const std::string& delimiters);
char* OpenTextFile(const char* filename);

extern bool wasDPressed;

#ifdef __ANDROID__
extern ANativeActivity* nativeActivity;
#endif

#endif
