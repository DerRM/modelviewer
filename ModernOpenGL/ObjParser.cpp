#include "ObjParser.h"


ObjParser::ObjParser()
    : m_vertexCount(0),
    m_texCoordCount(0),
    m_normalCount(0),
    m_faceCount(0),
    m_currentIndex(0),
    m_minVector(chs::Vector3(FLT_MAX, FLT_MAX, FLT_MAX)),
    m_maxVector(chs::Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX)),
    m_hasNormals(false)
{
}


ObjParser::~ObjParser()
{
}

Bool32 ObjParser::openFile(const char* pFilename)
{
    std::ifstream file(pFilename);

    m_file = &file;    

    return file.is_open();
}

void ObjParser::readFile()
{
    std::string line;

    while (std::getline(*m_file, line))
    {
        StringArray elements = tokenizeString(line, " ");

        if (elements.size() > 0)
        {
            if (elements[0] == "v")
            {
                Position vertex = parseVector3(elements);
                m_positionArray.push_back(vertex);
                ++m_vertexCount;
            } 
            else if (elements[0] == "vt")
            {
                Vector2 texCoord = parseVector2(elements);
                m_texCoordArray.push_back(texCoord);
                ++m_texCoordCount;
            }
            else if (elements[0] == "vn")
            {
                chs::Vector3 normal = parseVector3(elements);
                m_normalArray.push_back(normal);
                ++m_normalCount;
            }
            else if (elements[0] == "f")
            {
                parseFace(elements);
                ++m_faceCount;
            }
        }
    }

    finalizeData();
}

chs::Vector3 ObjParser::parseVector3(StringArray& stringArray)
{
    chs::Vector3 vec;
    vec.x = strtof(stringArray[1].c_str(), nullptr);
    vec.y = strtof(stringArray[2].c_str(), nullptr);
    vec.z = strtof(stringArray[3].c_str(), nullptr);
    return vec;
}

Vector2 ObjParser::parseVector2(StringArray& stringArray)
{
    Vector2 vec;
    vec.x = strtof(stringArray[1].c_str(), nullptr);
    vec.y = strtof(stringArray[2].c_str(), nullptr);
    return vec;
}

void ObjParser::parseFace(StringArray& stringArray)
{

    for (size_t i = 1; i < stringArray.size(); ++i)
    {
        if (i > 3)
        {
            Uint32 indexFirst = m_stringIndexHashSet.find(stringArray[1])->second;
            Uint32 indexPrevious = m_stringIndexHashSet.find(stringArray[i - 1])->second;

            m_indexArray.push_back(indexFirst);
            m_indexArray.push_back(indexPrevious);
        }

        if (contains(m_hashSet, stringArray[i]))
        {
            Uint32 index = m_stringIndexHashSet.find(stringArray[i])->second;
            m_indexArray.push_back(index);
        }
        else
        {
            m_hashSet.insert(stringArray[i]);
            m_stringIndexHashSet.insert(std::make_pair(stringArray[i], m_currentIndex));
            m_indexArray.push_back(m_currentIndex);
            m_currentIndex++;
        
            StringArray v = tokenizeString(stringArray[i], "/");

            if (v[0].size() > 0) // position
            {
                Int32 vertexIndex = (Int32)std::strtol(v[0].c_str(), nullptr, 0);
                
                Position position = m_positionArray[vertexIndex < 0 ? m_vertexCount + vertexIndex : vertexIndex - 1];

                m_minVector.x = fmin(m_minVector.x, position.x);
                m_minVector.y = fmin(m_minVector.y, position.y);
                m_minVector.z = fmin(m_minVector.z, position.z);

                m_maxVector.x = fmax(m_maxVector.x, position.x);
                m_maxVector.y = fmax(m_maxVector.y, position.y);
                m_maxVector.z = fmax(m_maxVector.z, position.z);

                m_tempPositionArray.push_back(position);
            }

            if (v.size() > 1 && v[1].size() > 0) // texCoord
            {
                Int32 texCoordIndex = (Int32)std::strtol(v[1].c_str(), nullptr, 0);
                m_tempTexCoordArray.push_back(m_texCoordArray[texCoordIndex < 0 ? m_texCoordCount + texCoordIndex : texCoordIndex - 1]);
            }

            if (v.size() > 2 && v[2].size() > 0) // normal
            {
                Int32 normalIndex = (Int32)std::strtol(v[2].c_str(), nullptr, 0);
                m_tempNormalArray.push_back(m_normalArray[normalIndex < 0 ? m_normalCount + normalIndex : normalIndex - 1]);
                m_hasNormals = true;
            }
        }
    }
}

void ObjParser::finalizeData()
{
    if (!m_hasNormals)
    {
        if (m_tempNormalArray.size() == 0 || m_tempNormalArray.size() < m_tempPositionArray.size())
        {
            m_tempNormalArray.resize(m_tempPositionArray.size());
        }

        for (size_t i = 0; i < m_indexArray.size(); i += 3)
        {
            int index0 = m_indexArray[i];
            int index1 = m_indexArray[i + 1];
            int index2 = m_indexArray[i + 2];

            Position pos1 = m_tempPositionArray[index0];
            Position pos2 = m_tempPositionArray[index1];
            Position pos3 = m_tempPositionArray[index2];

            chs::Vector3 vec1 = pos2 - pos1;
            chs::Vector3 vec2 = pos3 - pos1;
            chs::Vector3 normal = vec1.cross(vec2);

            m_tempNormalArray[index0] += normal;
            m_tempNormalArray[index1] += normal;
            m_tempNormalArray[index2] += normal;
        }
    }

    m_positionArray = m_tempPositionArray;
    m_texCoordArray = m_tempTexCoordArray;
    m_normalArray = m_tempNormalArray;

    for (size_t i = 0; i < m_normalArray.size(); ++i)
    {
        m_normalArray[i] = m_normalArray[i].normalize();
    }

    m_tempPositionArray.clear();
    m_tempNormalArray.clear();
    m_tempTexCoordArray.clear();
}

bool ObjParser::contains(const std::unordered_set<std::string>& hashSet, std::string& word)
{
    std::unordered_set<std::string>::const_iterator it = hashSet.find(word);

    return it != hashSet.end();
}


void ObjParser::closeFile()
{
    m_file->close();
}
