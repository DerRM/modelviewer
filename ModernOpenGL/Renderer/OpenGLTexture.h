#pragma once

#include "../Common.h"
#include "../Resource/JPEG.h"
#include "../Resource/ResourceLoader.h"

struct TGAHeader
{
    Int8 idLength;          // Length of the image ID field
    Int8 colorMapType;      // Whether a color map is included
    Int8 imageType;         // Compression and color types
    Int8 colorMapSpec[5]; 	// Describes the color map
    Int8 imageSpec[10];     // Image dimensions and format
};

class OpenGLTexture
{
public:
    OpenGLTexture();
    ~OpenGLTexture();

    GLuint loadTGAFromFile(const char* fileName);
    GLuint loadPNGFromFile(const char* fileName);
    GLuint loadJPGFromFile(const char* fileName);

private:
    GLuint initOpenGLTexture();

    Uint16 m_imageWidth;
    Uint16 m_imageHeight;
    Uint8 m_pixelDepth;

    Uint8* m_pImageData;
};
