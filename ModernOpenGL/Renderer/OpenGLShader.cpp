#include "OpenGLShader.h"

OpenGLShader::OpenGLShader()
{
}


OpenGLShader::~OpenGLShader()
{
}

GLuint OpenGLShader::createShaderProgram(const char* vertexShader, const char* fragmentShader)
{
    GLuint vertexShaderHandle = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShaderHandle, 1, &vertexShader, nullptr);
    glCompileShader(vertexShaderHandle);

    GLint isCompiled = 0;
    glGetShaderiv(vertexShaderHandle, GL_COMPILE_STATUS, &isCompiled);

    if (isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(vertexShaderHandle, GL_INFO_LOG_LENGTH, &maxLength);
        
        std::vector<GLchar> buffer(maxLength);
        glGetShaderInfoLog(vertexShaderHandle, maxLength, nullptr, &buffer[0]);

        Log("VertexShader Error: %s", &buffer[0]);

        glDeleteShader(vertexShaderHandle);
        return 0;
    }

    GLuint fragmentShaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShaderHandle, 1, &fragmentShader, nullptr);
    glCompileShader(fragmentShaderHandle);

    glGetShaderiv(fragmentShaderHandle, GL_COMPILE_STATUS, &isCompiled);
    
    if (isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(fragmentShaderHandle, GL_INFO_LOG_LENGTH, &maxLength);

        std::vector<GLchar> buffer(maxLength);
        glGetShaderInfoLog(fragmentShaderHandle, maxLength, nullptr, &buffer[0]);

        Log("FragmentShader Error: %s", &buffer[0]);
        
        glDeleteShader(fragmentShaderHandle);
        return 0;
    }

    m_shaderProgramm = glCreateProgram();
    glAttachShader(m_shaderProgramm, vertexShaderHandle);
    glAttachShader(m_shaderProgramm, fragmentShaderHandle);

    glBindAttribLocation(m_shaderProgramm, 0, "position");
    glBindAttribLocation(m_shaderProgramm, 1, "normal");
    glBindAttribLocation(m_shaderProgramm, 2, "texCoord");
    glBindAttribLocation(m_shaderProgramm, 3, "boneIndex");
    glBindAttribLocation(m_shaderProgramm, 4, "boneWeight");

    glLinkProgram(m_shaderProgramm);


    GLint isLinked = 0;
    glGetProgramiv(m_shaderProgramm, GL_LINK_STATUS, &isLinked);
    
    if (isLinked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(m_shaderProgramm, GL_INFO_LOG_LENGTH, &maxLength);

        std::vector<GLchar> buffer(maxLength);
        glGetProgramInfoLog(m_shaderProgramm, maxLength, nullptr, &buffer[0]);

        Log("Program Linking Error: %s", &buffer[0]);
        
        glDeleteProgram(m_shaderProgramm);
        return 0;
    }
    
    /*GLint numUniforms = 0;
    glGetProgramInterfaceiv(m_shaderProgramm, GL_UNIFORM, GL_ACTIVE_RESOURCES, &numUniforms);

    const GLenum properties[] = {GL_BLOCK_INDEX, GL_TYPE, GL_NAME_LENGTH, GL_LOCATION};

    for (size_t uniformIndex = 0; uniformIndex < (size_t)numUniforms; ++uniformIndex)
    {
        GLint values[4];
        glGetProgramResourceiv(m_shaderProgramm, GL_UNIFORM, uniformIndex, 4, properties, 4, NULL, values);

        if (values[0] != -1)
        {
            continue;
        }

        std::vector<GLchar> nameData(values[2]);
        glGetProgramResourceName(m_shaderProgramm, GL_UNIFORM, uniformIndex, nameData.size(), NULL, &nameData[0]);
        std::string name(nameData.begin(), nameData.end() - 1);

        WinLog("Name: %s Type: %s", name.c_str(), typeToString(values[1]));
    }*/

    //GLint activeUniforms;
    //glGetProgramiv(programHandle, GL_ACTIVE_UNIFORMS, &activeUniforms);

    //GLint uniformNameMaxLength;
    //glGetProgramiv(programHandle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &uniformNameMaxLength);

    //for (size_t uniformIndex = 0; uniformIndex < activeUniforms; ++uniformIndex)
    //{
    //    GLsizei uniformLength;
    //    GLint uniformSize;
    //    GLenum uniformType;
    //    GLchar* uniformName = (GLchar*)malloc(uniformNameMaxLength);
    //    glGetActiveUniform(programHandle, uniformIndex, uniformNameMaxLength, &uniformLength, &uniformSize, &uniformType, uniformName);
    //    WinLog("Name: %s Type: %s", uniformName, typeToString(uniformType));
    //    free(uniformName);
    //}

    //GLint activeAttributes;
    //glGetProgramiv(programHandle, GL_ACTIVE_ATTRIBUTES, &activeAttributes);

    //GLint attributeNameMaxLength;
    //glGetProgramiv(programHandle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &attributeNameMaxLength);

    //for (size_t attributeIndex = 0; attributeIndex < activeAttributes; ++attributeIndex)
    //{
    //    GLsizei attribLength;
    //    GLint attribSize;
    //    GLenum attribType;
    //    GLchar* attribName = (GLchar*)malloc(attributeNameMaxLength);
    //    glGetActiveAttrib(programHandle, attributeIndex, attributeNameMaxLength, &attribLength, &attribSize, &attribType, attribName);
    //    WinLog("Name: %s Type: %s", attribName, typeToString(attribType));
    //    free(attribName);
    //}

    return m_shaderProgramm;
}

void OpenGLShader::deleteShaderProgram()
{
    glUseProgram(0);
    glDeleteProgram(m_shaderProgramm);
    m_shaderProgramm = 0;
}

const GLchar* OpenGLShader::typeToString(GLenum type)
{
    switch (type)
    {
    case GL_FLOAT_VEC3:
        return "vec3";
    case GL_FLOAT_VEC4:
        return "vec4";
    case GL_FLOAT_MAT4:
        return "mat4";
    case GL_SAMPLER_2D:
        return "sampler2D";
    default:
        return "unknown";
    }
}

void OpenGLShader::setInt(const char* name, Int32 value)
{
    GLint floatLocation = glGetUniformLocation(m_shaderProgramm, name);
    glUniform1i(floatLocation, value);
}

void OpenGLShader::setVector3(const char* name, const chs::Vector3& value)
{
    GLint vectorLocation = glGetUniformLocation(m_shaderProgramm, name);
    glUniform3fv(vectorLocation, 1, &value.x);
}

void OpenGLShader::setVector4(const char* name, const chs::Vector4& value)
{
    GLint vectorLocation = glGetUniformLocation(m_shaderProgramm, name);
    glUniform4fv(vectorLocation, 1, &value.x);
}

void OpenGLShader::setMatrix4(const char* name, const chs::Matrix4x4& value)
{
    GLint matrixLocation = glGetUniformLocation(m_shaderProgramm, name);
    glUniformMatrix4fv(matrixLocation, 1, GL_FALSE, value.getData());
}

void OpenGLShader::setMatrix4Array(const char* name, const std::vector<chs::Matrix4x4>& value)
{
	GLint matrixArrayLocation = glGetUniformLocation(m_shaderProgramm, name);
	glUniformMatrix4fv(matrixArrayLocation, value.size(), GL_FALSE, value[0].getData());
}
