#include "OpenGLTexture.h"

OpenGLTexture::OpenGLTexture()
    : m_imageWidth(0),
    m_imageHeight(0),
    m_pixelDepth(0)
{
}


OpenGLTexture::~OpenGLTexture()
{
}

GLuint OpenGLTexture::loadTGAFromFile(const char* fileName)
{
    TGAHeader* pTgaFile = (TGAHeader*)ResourceLoader::openFile(fileName);

    m_imageWidth = *((Uint16*)&pTgaFile->imageSpec[4]);
    m_imageHeight = *((Uint16*)&pTgaFile->imageSpec[6]);
    m_pixelDepth = pTgaFile->imageSpec[8];
	
	Log("TGA width: %d, height: %d, depth: %d", m_imageWidth, m_imageHeight, m_pixelDepth);

    m_pImageData = ((Uint8*)pTgaFile) + sizeof(TGAHeader);

    return initOpenGLTexture();
}

GLuint OpenGLTexture::loadPNGFromFile(const char* fileName)
{
    return 0;
}

GLuint OpenGLTexture::loadJPGFromFile(const char* fileName)
{
    JPEG jpeg;
    jpeg.loadFile(fileName);

    m_pImageData = jpeg.GetImageData();
    m_imageHeight = jpeg.GetImageHeight();
    m_imageWidth = jpeg.GetImageWidth();

    return initOpenGLTexture();
}

GLuint OpenGLTexture::initOpenGLTexture()
{
    GLuint textureObjectId;
    glGenTextures(1, &textureObjectId);

    glBindTexture(GL_TEXTURE_2D, textureObjectId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_imageWidth, m_imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, m_pImageData);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8, m_imageWidth, m_imageHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, m_pImageData);
    //glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);

    return textureObjectId;
}


