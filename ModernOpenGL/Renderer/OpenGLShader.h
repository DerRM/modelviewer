#pragma once

#include <vector>

#include "../Common.h"

class OpenGLShader
{
public:
    OpenGLShader();
    ~OpenGLShader();

    GLuint createShaderProgram(const char* vertexShader, const char* fragmentShader);
    const GLchar* typeToString(GLenum type);
    void deleteShaderProgram();

    void setInt(const char* name, Int32 value);
    void setVector3(const char* name, const chs::Vector3& value);
    void setVector4(const char* name, const chs::Vector4& value);
    void setMatrix4(const char* name, const chs::Matrix4x4& value);
	void setMatrix4Array(const char* name, const std::vector<chs::Matrix4x4>& value);

    GLuint getShaderProgram() { return m_shaderProgramm; }
private:
   // GLuint m_vertexShader;
   // GLuint m_fragmentShader;
    GLuint m_shaderProgramm;
};
