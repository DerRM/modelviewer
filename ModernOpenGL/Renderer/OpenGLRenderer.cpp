#include "OpenGLRenderer.h"
#include "../ObjParser.h"
#include "../Collada/ColladaParser.h"

#ifdef __ANDROID__
#include <android/log.h>
#endif

OpenGLRenderer::OpenGLRenderer(IOpenGLContext* context)
    : m_context(context)
{

}

OpenGLRenderer::~OpenGLRenderer(void)
{
}

void OpenGLRenderer::initRenderer(void)
{
    glClearColor(0.13f, 0.3f, 0.85f, 1.0f);
    
    //glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    //glFrontFace(GL_CW);
    
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    
    glEnable(GL_FRAMEBUFFER_SRGB);
    //glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
}

static GLuint arrayId;
static GLuint indexBufferId;
static GLsizei indexCount;
static chs::Vector3 minVec, maxVec;
static GLuint textureId;

static ColladaParser parser;

static OpenGLShader lineShader;

static GLuint vertexBufferId;
static GLuint vertexCount;

void OpenGLRenderer::setupScene()
{
    lineShader.createShaderProgram(OpenTextFile("../Assets/Shader/color.vert"), OpenTextFile("../Assets/Shader/color.frag"));

    if (parser.openFile("../Assets/astroBoy_walk_Max.dae"))
    {
        parser.readFile();
    }
    else
    {
#ifdef __ANDROID__
        __android_log_print(ANDROID_LOG_ERROR, "Test", "could not load xml file for some reason");
#endif
    }

    parser.closeFile();
    parser.prepareAnimation();
    parser.prepareSkeleton();
    parser.prepareSkeletonForOGL();
    parser.prepareSkinning();

    parser.parseMesh();
    parser.makeOpenGlReady();
    //parser.applySkinning();

    PositionArray& posArray = parser.getPositionArray();
    NormalArray& normalArray = parser.getNormalArray();
    IndexArray& indexArray = parser.getIndexArray();
    TexCoord2Array& texCoordArray = parser.getTexCoordArray();

	BoneIndexArray& boneIndexArray = parser.getBoneIndexArray();
	BoneWeightArray& boneWeightArray = parser.getBoneWeightArray();

    //ObjParser parser;

    //if (parser.openFile("../Assets/alfa147.obj"))
    //{
    //    parser.readFile();
    //}

    //parser.closeFile();

    OpenGLTexture texture;
    textureId = texture.loadTGAFromFile("../Assets/boy_10.tga");
    //textureId = texture.loadJPGFromFile("../Assets/boy_10.JPG");

    indexCount = static_cast<GLsizei>(indexArray.size());
    vertexCount = static_cast<GLuint>(posArray.size());
    //minVec = parser.getMinVector();
    //maxVec = parser.getMaxVector();

    // required for GL context >= 3.0
    glGenVertexArrays(1, &arrayId);
    glBindVertexArray(arrayId);
    
    //vertexBufferId;
    glGenBuffers(1, &vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Position), &posArray[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    GLuint normalBufferId;
    glGenBuffers(1, &normalBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, normalBufferId);
    glBufferData(GL_ARRAY_BUFFER, static_cast<GLuint>(normalArray.size()) * sizeof(Normal), &normalArray[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    
    glBindBuffer(GL_ARRAY_BUFFER, normalBufferId);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    GLuint textureBufferId;
    glGenBuffers(1, &textureBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, textureBufferId);
    glBufferData(GL_ARRAY_BUFFER, static_cast<GLuint>(texCoordArray.size()) * sizeof(Texcoord2), &texCoordArray[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, textureBufferId);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	GLuint boneIndexBufferId;
	glGenBuffers(1, &boneIndexBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, boneIndexBufferId);
    glBufferData(GL_ARRAY_BUFFER, static_cast<GLuint>(boneIndexArray.size()) * sizeof(BoneIndex), &boneIndexArray[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER, boneIndexBufferId);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

	GLuint boneWeightBufferId;
	glGenBuffers(1, &boneWeightBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, boneWeightBufferId);
    glBufferData(GL_ARRAY_BUFFER, static_cast<GLuint>(boneWeightArray.size()) * sizeof(BoneWeight), &boneWeightArray[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(4);

	glBindBuffer(GL_ARRAY_BUFFER, boneWeightBufferId);
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

    glGenBuffers(1, &indexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLuint>(indexArray.size()) * sizeof(Int32), &indexArray[0], GL_STATIC_DRAW);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

static float delta = 0.0f;

void OpenGLRenderer::renderScene(OpenGLShader& shader, Float64 frameTime)
{
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glClearColor(0.13f, 0.3f, 0.85f, 1.0f);

    parser.updateSkeleton(frameTime);

    chs::Matrix3x3 rotation = chs::fromEuler(chs::Vector3(0.0f, delta, 0.0f)).toMatrix3x3() * chs::fromEuler(chs::Vector3(-90.0f, 0.0f, 0.0f)).toMatrix3x3();
    chs::Matrix4x4 modelMatrix = chs::Matrix4x4::rotTrans(rotation, chs::Vector3(0.0f, -3.0f, 0.0f));
    //chs::Matrix4x4 viewMatrix = chs::Matrix4x4::lookAt(chs::Vector3(0.0f, (minVec.y + maxVec.y) * 0.5f, (maxVec.z + maxVec.z)), (minVec + maxVec) * 0.5f, chs::Vector3(0.0f, 1.0f, 0.0f));
    chs::Matrix4x4 viewMatrix = chs::Matrix4x4::lookAt(chs::Vector3(0.0f, 0.0f, 7.0f), chs::Vector3::zero(), chs::Vector3(0.0f, 1.0f, 0.0f));
    chs::Matrix4x4 projectionMatrix = chs::Matrix4x4::perspectiveProjection(90.0f, m_context->getWindow()->getWidth() / static_cast<float>(m_context->getWindow()->getHeight()), 0.01f, 1000.0f);
    
	std::vector<chs::Matrix4x4>& bones = parser.getBones();

    glUseProgram(shader.getShaderProgram());    
    shader.setMatrix4("modelMatrix", modelMatrix);
    shader.setMatrix4("viewMatrix", viewMatrix);
    shader.setMatrix4("normalMatrix", (viewMatrix * modelMatrix).inverse().transpose());
    shader.setMatrix4("projectionMatrix", projectionMatrix);

    shader.setMatrix4Array("bones", bones);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
    shader.setInt("testTexture", 0);
    //shader.setMatrix4("projectionMatrix", chs::Matrix4x4::ortographicProjection(-4.0f/3.0f, 4.0f/3.0f, -1.0f, 1.0f, -1.0f, 1.0f));
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
    glBindVertexArray(arrayId);
    glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    /*
    if (wasDPressed)
    {
        glDisable(GL_DEPTH_TEST);

        glUseProgram(lineShader.getShaderProgram());
        lineShader.setMatrix4("modelMatrix", modelMatrix);
        lineShader.setMatrix4("viewMatrix", viewMatrix);
        lineShader.setMatrix4("projectionMatrix", projectionMatrix);
        lineShader.setVector4("out_color", chs::Vector4(1.0f, 0.0f, 0.0f, 1.0f));
        parser.renderSkeleton();
        glUseProgram(0);

        glEnable(GL_DEPTH_TEST);
    }
    */

    m_context->swapBuffers();

    delta += 50.0f * static_cast<Float32>(frameTime);
}

void OpenGLRenderer::reshapeWindow(int width, int height)
{
    glViewport(0, 0, width, height);
}
