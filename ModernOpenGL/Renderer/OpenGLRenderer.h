#pragma once

#include "../Common.h"
#include "OpenGLShader.h"
#include "OpenGLTexture.h"
#include "OpenGLContext.h"

class OpenGLRenderer
{
public:
    OpenGLRenderer(IOpenGLContext* context);
    ~OpenGLRenderer(void);

    void initRenderer(void);
    void setupScene(void);
    static void reshapeWindow(int width, int height);
    void renderScene(OpenGLShader& program, Float64 frameTime);

protected:

    IOpenGLContext *m_context;
};
