#ifndef OPENGLES2_H_
#define OPENGLES2_H_

#ifdef _WIN32
#include <Windows.h>
#elif __ANDROID__
#include <EGL/egl.h>
#elif __linux__
#include <X11/Xlib.h>
#elif __EMSCRIPTEN__
#include <emscripten.h>
#endif

#if ! (defined(__ANDROID__) || defined(__EMSCRIPTEN__))
#define GL_GLES_PROTOTYPES 0
#endif

#include <string>
#include <vector>
#include <algorithm>
#if _WIN32
#include <GL/gl.h>
#endif
//#include <GL/glcorearb.h>
//#include <GL/glext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#ifdef _WIN32
#include <GL/wglext.h>
#elif __linux__ && !__ANDROID__
#include <GL/glx.h>
#elif __EMSCRIPTEN__
#include <EGL/egl.h>
#endif
#include "../Common.h"

#if !(__ANDROID__ || __EMSCRIPTEN__)
#ifdef _WIN32
extern PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB;
extern PFNWGLGETEXTENSIONSSTRINGARBPROC wglGetExtensionsStringARB;
extern PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;
extern PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;
#endif
extern PFNGLATTACHSHADERPROC glAttachShader;
extern PFNGLBINDBUFFERPROC glBindBuffer;
extern PFNGLBUFFERDATAPROC glBufferData;
extern PFNGLCOMPILESHADERPROC glCompileShader;
extern PFNGLCREATEPROGRAMPROC glCreateProgram;
extern PFNGLCREATESHADERPROC glCreateShader;
extern PFNGLDELETEBUFFERSPROC glDeleteBuffers;
extern PFNGLDELETEPROGRAMPROC glDeleteProgram;
extern PFNGLDELETESHADERPROC glDeleteShader;
extern PFNGLDETACHSHADERPROC glDetachShader;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
extern PFNGLGENBUFFERSPROC glGenBuffers;
extern PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation;
extern PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
extern PFNGLGETPROGRAMIVPROC glGetProgramiv;
extern PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
extern PFNGLGETSHADERIVPROC glGetShaderiv;
extern PFNGLLINKPROGRAMPROC glLinkProgram;
extern PFNGLSHADERSOURCEPROC glShaderSource;
extern PFNGLUSEPROGRAMPROC glUseProgram;
extern PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
extern PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation;
extern PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
extern PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
#ifdef _WIN32
extern PFNGLACTIVETEXTUREPROC glActiveTexture;
#endif
extern PFNGLUNIFORM1IPROC glUniform1i;
extern PFNGLGENERATEMIPMAPPROC glGenerateMipmap;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;
extern PFNGLUNIFORM3FVPROC glUniform3fv;
extern PFNGLUNIFORM4FVPROC glUniform4fv;
extern PFNGLGETACTIVEUNIFORMPROC glGetActiveUniform;
extern PFNGLGETACTIVEATTRIBPROC glGetActiveAttrib;
#endif

#if !__EMSCRIPTEN__
extern PFNGLMAPBUFFEROESPROC glMapBufferOES;
extern PFNGLUNMAPBUFFEROESPROC glUnmapBufferOES;
#endif 
bool InitOpenGlExtensions();
bool InitOpenGlES2CoreProfile();
bool IsGLExtSupported(const std::string& extension);

#ifdef _WIN32
bool IsWGLExtSupported(const std::string& extension, HDC hdc);
#endif

#endif
