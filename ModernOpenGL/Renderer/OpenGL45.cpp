#include "OpenGL45.h"

#ifdef _WIN32
PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;
PFNWGLGETEXTENSIONSSTRINGARBPROC wglGetExtensionsStringARB = NULL;
PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = NULL;
PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = NULL;
PFNGLACTIVETEXTUREPROC glActiveTexture = NULL;
#endif
#ifndef __APPLE__
PFNGLGETSTRINGIPROC glGetStringi = NULL;
PFNGLATTACHSHADERPROC glAttachShader = NULL;
PFNGLBINDBUFFERPROC glBindBuffer = NULL;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = NULL;
PFNGLBUFFERDATAPROC glBufferData = NULL;
PFNGLCOMPILESHADERPROC glCompileShader = NULL;
PFNGLCREATEPROGRAMPROC glCreateProgram = NULL;
PFNGLCREATESHADERPROC glCreateShader = NULL;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = NULL;
PFNGLDELETEPROGRAMPROC glDeleteProgram = NULL;
PFNGLDELETESHADERPROC glDeleteShader = NULL;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays = NULL;
PFNGLDETACHSHADERPROC glDetachShader = NULL;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = NULL;
PFNGLGENBUFFERSPROC glGenBuffers = NULL;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = NULL;
PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation = NULL;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = NULL;
PFNGLGETPROGRAMIVPROC glGetProgramiv = NULL;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = NULL;
PFNGLGETSHADERIVPROC glGetShaderiv = NULL;
PFNGLLINKPROGRAMPROC glLinkProgram = NULL;
PFNGLSHADERSOURCEPROC glShaderSource = NULL;
PFNGLUSEPROGRAMPROC glUseProgram = NULL;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = NULL;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = NULL;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = NULL;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = NULL;
PFNGLUNIFORM1IPROC glUniform1i = NULL;
PFNGLGENERATEMIPMAPPROC glGenerateMipmap = NULL;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = NULL;
PFNGLUNIFORM3FVPROC glUniform3fv = NULL;
PFNGLUNIFORM4FVPROC glUniform4fv = NULL;
PFNGLGETACTIVEUNIFORMPROC glGetActiveUniform = NULL;
PFNGLGETACTIVEATTRIBPROC glGetActiveAttrib = NULL;
PFNGLGETPROGRAMINTERFACEIVPROC glGetProgramInterfaceiv = NULL;
PFNGLGETPROGRAMRESOURCEIVPROC glGetProgramResourceiv = NULL;
PFNGLGETPROGRAMRESOURCENAMEPROC glGetProgramResourceName = NULL;
PFNGLMAPBUFFERPROC glMapBuffer = NULL;
//PFNGLMAPNAMEDBUFFERPROC glMapNamedBuffer = NULL;
PFNGLUNMAPBUFFERPROC glUnmapBuffer = NULL;
PFNGLUNMAPNAMEDBUFFERPROC glUnmapNamedBuffer = NULL;
#endif

#ifdef _WIN32
#define GetProcAddr(fun) wglGetProcAddress(fun)
#elif __linux__
#define GetProcAddr(fun) glXGetProcAddressARB((const GLubyte *)fun)
#endif

bool InitOpenGl45CoreProfile()
{
#ifndef __APPLE__
    glGetStringi = (PFNGLGETSTRINGIPROC)GetProcAddr("glGetStringi");
    if (!glGetStringi)
    {
        return false;
    }

    glAttachShader = (PFNGLATTACHSHADERPROC)GetProcAddr("glAttachShader");
    if (!glAttachShader)
    {
        return false;
    }

    glBindBuffer = (PFNGLBINDBUFFERPROC)GetProcAddr("glBindBuffer");
    if (!glBindBuffer)
    {
        return false;
    }

    glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)GetProcAddr("glBindVertexArray");
    if (!glBindVertexArray)
    {
        return false;
    }

    glBufferData = (PFNGLBUFFERDATAPROC)GetProcAddr("glBufferData");
    if (!glBufferData)
    {
        return false;
    }

    glCompileShader = (PFNGLCOMPILESHADERPROC)GetProcAddr("glCompileShader");
    if (!glCompileShader)
    {
        return false;
    }

    glCreateProgram = (PFNGLCREATEPROGRAMPROC)GetProcAddr("glCreateProgram");
    if (!glCreateProgram)
    {
        return false;
    }

    glCreateShader = (PFNGLCREATESHADERPROC)GetProcAddr("glCreateShader");
    if (!glCreateShader)
    {
        return false;
    }

    glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)GetProcAddr("glDeleteBuffers");
    if (!glDeleteBuffers)
    {
        return false;
    }

    glDeleteProgram = (PFNGLDELETEPROGRAMPROC)GetProcAddr("glDeleteProgram");
    if (!glDeleteProgram)
    {
        return false;
    }

    glDeleteShader = (PFNGLDELETESHADERPROC)GetProcAddr("glDeleteShader");
    if (!glDeleteShader)
    {
        return false;
    }

    glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)GetProcAddr("glDeleteVertexArrays");
    if (!glDeleteVertexArrays)
    {
        return false;
    }

    glDetachShader = (PFNGLDETACHSHADERPROC)GetProcAddr("glDetachShader");
    if (!glDetachShader)
    {
        return false;
    }

    glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)GetProcAddr("glEnableVertexAttribArray");
    if (!glEnableVertexAttribArray)
    {
        return false;
    }

    glGenBuffers = (PFNGLGENBUFFERSPROC)GetProcAddr("glGenBuffers");
    if (!glGenBuffers)
    {
        return false;
    }

    glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)GetProcAddr("glGenVertexArrays");
    if (!glGenVertexArrays)
    {
        return false;
    }

    glGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC)GetProcAddr("glGetAttribLocation");
    if (!glGetAttribLocation)
    {
        return false;
    }

    glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)GetProcAddr("glGetProgramInfoLog");
    if (!glGetProgramInfoLog)
    {
        return false;
    }

    glGetProgramiv = (PFNGLGETPROGRAMIVPROC)GetProcAddr("glGetProgramiv");
    if (!glGetProgramiv)
    {
        return false;
    }

    glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)GetProcAddr("glGetShaderInfoLog");
    if (!glGetShaderInfoLog)
    {
        return false;
    }

    glGetShaderiv = (PFNGLGETSHADERIVPROC)GetProcAddr("glGetShaderiv");
    if (!glGetShaderiv)
    {
        return false;
    }

    glLinkProgram = (PFNGLLINKPROGRAMPROC)GetProcAddr("glLinkProgram");
    if (!glLinkProgram)
    {
        return false;
    }

    glShaderSource = (PFNGLSHADERSOURCEPROC)GetProcAddr("glShaderSource");
    if (!glShaderSource)
    {
        return false;
    }

    glUseProgram = (PFNGLUSEPROGRAMPROC)GetProcAddr("glUseProgram");
    if (!glUseProgram)
    {
        return false;
    }

    glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)GetProcAddr("glVertexAttribPointer");
    if (!glVertexAttribPointer)
    {
        return false;
    }

    glBindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC)GetProcAddr("glBindAttribLocation");
    if (!glBindAttribLocation)
    {
        return false;
    }

    glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)GetProcAddr("glGetUniformLocation");
    if (!glGetUniformLocation)
    {
        return false;
    }

    glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)GetProcAddr("glUniformMatrix4fv");
    if (!glUniformMatrix4fv)
    {
        return false;
    }
#ifdef _WIN32
    glActiveTexture = (PFNGLACTIVETEXTUREPROC)GetProcAddr("glActiveTexture");
    if (!glActiveTexture)
    {
        return false;
    }
#endif

    glUniform1i = (PFNGLUNIFORM1IPROC)GetProcAddr("glUniform1i");
    if (!glUniform1i)
    {
        return false;
    }

    glGenerateMipmap = (PFNGLGENERATEMIPMAPPROC)GetProcAddr("glGenerateMipmap");
    if (!glGenerateMipmap)
    {
        return false;
    }

    glDisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)GetProcAddr("glDisableVertexAttribArray");
    if (!glDisableVertexAttribArray)
    {
        return false;
    }

    glUniform3fv = (PFNGLUNIFORM3FVPROC)GetProcAddr("glUniform3fv");
    if (!glUniform3fv)
    {
        return false;
    }

    glUniform4fv = (PFNGLUNIFORM4FVPROC)GetProcAddr("glUniform4fv");
    if (!glUniform4fv)
    {
        return false;
    }

    glGetActiveUniform = (PFNGLGETACTIVEUNIFORMPROC)GetProcAddr("glGetActiveUniform");
    if (!glGetActiveUniform)
    {
        return false;
    }

    glGetActiveAttrib = (PFNGLGETACTIVEATTRIBPROC)GetProcAddr("glGetActiveAttrib");
    if (!glGetActiveAttrib)
    {
        return false;
    }

    glGetProgramInterfaceiv = (PFNGLGETPROGRAMINTERFACEIVPROC)GetProcAddr("glGetProgramInterfaceiv");
    if (!glGetProgramInterfaceiv)
    {
        return false;
    }

    glGetProgramResourceiv = (PFNGLGETPROGRAMRESOURCEIVPROC)GetProcAddr("glGetProgramResourceiv");
    if (!glGetProgramResourceiv)
    {
        return false;
    }

    glGetProgramResourceName = (PFNGLGETPROGRAMRESOURCENAMEPROC)GetProcAddr("glGetProgramResourceName");
    if (!glGetProgramResourceName)
    {
        return false;
    }

    glMapBuffer = (PFNGLMAPBUFFERPROC)GetProcAddr("glMapBuffer");
    if (!glMapBuffer)
    {
        return false;
    }

    /*glMapNamedBuffer = (PFNGLMAPNAMEDBUFFERPROC)GetProcAddr("glMapNamedBuffer");
    if (!glMapNamedBuffer)
    {
        return false;
    }*/

    glUnmapBuffer = (PFNGLUNMAPBUFFERPROC)GetProcAddr("glUnmapBuffer");
    if (!glUnmapBuffer)
    {
        return false;
    }

    /*glUnmapNamedBuffer = (PFNGLUNMAPNAMEDBUFFERPROC)GetProcAddr("glUnmapNamedBuffer");
    if (!glUnmapNamedBuffer)
    {
        return false;
    }*/
#endif
    return true;
}

bool InitOpenGlExtensions()
{
#ifdef _WIN32
    wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)GetProcAddr("wglCreateContextAttribsARB");
    if (!wglCreateContextAttribsARB)
    {
        return false;
    }

    wglGetExtensionsStringARB = (PFNWGLGETEXTENSIONSSTRINGARBPROC)GetProcAddr("wglGetExtensionsStringARB");
    if (!wglGetExtensionsStringARB)
    {
        return false;
    }

    wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)GetProcAddr("wglChoosePixelFormatARB");
    if (!wglChoosePixelFormatARB)
    {
        return false;
    }

    wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)GetProcAddr("wglSwapIntervalEXT");
    if (!wglSwapIntervalEXT)
    {
        return false;
    }
#endif
    return true;
}

bool IsGLExtSupported(const std::string& extension)
{
    std::vector<std::string> extensions;
    GLint numExtensions = 0;

    glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

//    if (glGetStringi != NULL)
    {
        for (GLint i = 0; i < numExtensions; i++)
        {
            std::string extensionString = (const char*)glGetStringi(GL_EXTENSIONS, i);
            extensions.push_back(extensionString);
        }
    }

    return std::find(extensions.begin(), extensions.end(), extension) != extensions.end();
}
#ifdef _WIN32
bool IsWGLExtSupported(const std::string& extension, HDC hdc)
{
    std::vector<std::string> extensions;

    std::string extensionString = wglGetExtensionsStringARB(hdc);
    extensions = tokenizeString(extensionString, " ");

    return std::find(extensions.begin(), extensions.end(), extension) != extensions.end();
}
#endif
