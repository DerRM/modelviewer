#pragma once

#include "../Common/Window.h"

class IOpenGLContext {

public:
    virtual ~IOpenGLContext() {}

    virtual bool createOpenGLContext() = 0;
    virtual void destroyOpenGLContext() = 0;
    virtual void swapBuffers() = 0;
    virtual IWindow* getWindow() = 0;

};