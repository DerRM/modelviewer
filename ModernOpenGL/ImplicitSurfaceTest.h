#pragma once

#include "Math/Vector3.h"
#include <vector>
#include "Common.h"

typedef struct {
    chs::Vector3 p[3];
} TRIANGLE;

typedef struct {
    chs::Vector3 p[8];
    double val[8];
} GRIDCELL;

class ImplicitSurfaceTest
{
public:
    ImplicitSurfaceTest();
    ~ImplicitSurfaceTest();

    void implicitFunction(float x, float y, float z, 
        float radius, float* densities, float dz, int numberSamples);

    int Polygonise(GRIDCELL grid, double isolevel, 
        std::vector<TRIANGLE>& triangles, std::vector<Uint32>& indices, std::vector<chs::Vector3>& normals);

    void prepareForOpenGL();

    std::vector<TRIANGLE>& getTriangles() { return m_triangles; }
    std::vector<Uint32>& getIndices() { return m_indices; }
    std::vector<chs::Vector3>& getNormals() { return m_normals; }

private:
    chs::Vector3 VertexInterp(double isolevel, chs::Vector3 p1, chs::Vector3 p2, double valp1, double valp2);
    
    std::vector<TRIANGLE> m_triangles;
    std::vector<Uint32> m_indices;
    std::vector<chs::Vector3> m_normals;
};

