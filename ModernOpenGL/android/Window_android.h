#pragma once

#include "../Common/Window.h"

#include <android/native_window.h>

class Window : public IWindow {

public:
    Window(const char* title, int width, int height);

    virtual void createWindow() override;
    virtual void* getNativeHandle() override;
    virtual inline const char* getTitle() override;
    virtual inline int getWidth() override;
    virtual inline int getHeight() override;
    virtual void showWindow() override;
    virtual void hideWindow() override;
    virtual void destroyWindow() override;
    virtual bool shouldClose() override;
    virtual void inline setWidth(int width) override;
    virtual void inline setHeight(int height) override;

    virtual void setReshapeFunction(reshapeFunc function) override;

    void setNativeWindow(ANativeWindow* window) { m_window = window; }

private:
    const char* m_title;
    int m_width;
    int m_height;
    reshapeFunc m_reshapeFunc;

    ANativeWindow* m_window;
};

const char* Window::getTitle()
{
    return m_title;
}

int Window::getWidth()
{
    return m_width;
}

int Window::getHeight()
{
    return m_height;
}

void Window::setWidth(int width)
{
    m_width = width;
}

void Window::setHeight(int height)
{
    m_height = height;
}