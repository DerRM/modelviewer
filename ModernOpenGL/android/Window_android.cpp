#include "../android/Window_android.h"

Window::Window(const char * title, int width, int height)
    : m_title(title),
    m_width(width),
    m_height(height),
    m_reshapeFunc(nullptr),
    m_window(nullptr)
{
}

void Window::createWindow()
{

}

void * Window::getNativeHandle()
{
    return (void*)m_window;
}

void Window::showWindow()
{
}

void Window::hideWindow()
{
}

void Window::destroyWindow()
{

}

bool Window::shouldClose()
{
    return false;
}

void Window::setReshapeFunction(reshapeFunc function)
{
    m_reshapeFunc = function;
}
