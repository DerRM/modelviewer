#include "../../android/GL/OpenGLContext_android.h"
#include "../../Renderer/OpenGLES2.h"

#include <android/native_window.h>

OpenGLES2Context::OpenGLES2Context(IWindow * window)
    : m_window(window),
      m_display(EGL_NO_DISPLAY),
      m_surface(EGL_NO_SURFACE),
      m_context(EGL_NO_CONTEXT)
{
}

bool OpenGLES2Context::createOpenGLContext()
{
    const EGLint attribs[] = {
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_DEPTH_SIZE, 16,
        EGL_NONE
    };

    EGLint format, w, h;
    EGLConfig config;
    EGLint numConfigs;

    m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    eglInitialize(m_display, 0, 0);
    eglChooseConfig(m_display, attribs, &config, 1, &numConfigs);
    eglGetConfigAttrib(m_display, config, EGL_NATIVE_VISUAL_ID, &format);

    ANativeWindow_setBuffersGeometry((ANativeWindow*)m_window->getNativeHandle(), 0, 0, format);

    m_surface = eglCreateWindowSurface(m_display, config, (ANativeWindow*)m_window->getNativeHandle(), NULL);

    EGLint attribList[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };

    m_context = eglCreateContext(m_display, config, NULL, attribList);

    if (eglMakeCurrent(m_display, m_surface, m_surface, m_context) == EGL_FALSE)
    {
        return false;
    }

    eglQuerySurface(m_display, m_surface, EGL_WIDTH, &w);
    eglQuerySurface(m_display, m_surface, EGL_HEIGHT, &h);

    m_window->setWidth(w);
    m_window->setHeight(h);

    InitOpenGlExtensions();

    return true;
}

void OpenGLES2Context::destroyOpenGLContext()
{
    if (m_display != EGL_NO_DISPLAY)
    {
        eglMakeCurrent(m_display, EGL_NO_DISPLAY, EGL_NO_SURFACE, EGL_NO_SURFACE);

        if (m_context != EGL_NO_CONTEXT)
        {
            eglDestroyContext(m_display, m_context);
        }

        if (m_surface != EGL_NO_SURFACE)
        {
            eglDestroySurface(m_display, m_surface);
        }

        eglTerminate(m_display);
    }

    m_display = EGL_NO_DISPLAY;
    m_context = EGL_NO_CONTEXT;
    m_surface = EGL_NO_SURFACE;
}

void OpenGLES2Context::swapBuffers()
{
    eglSwapBuffers(m_display, m_surface);
}
