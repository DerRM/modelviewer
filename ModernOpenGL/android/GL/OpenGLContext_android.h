#pragma once

#include "../../Renderer/OpenGLContext.h"
#include "../../android/Window_android.h"

#include <EGL/egl.h>

class OpenGLES2Context : public IOpenGLContext {

public:
    OpenGLES2Context(IWindow* window);

    virtual bool createOpenGLContext() override;
    virtual void destroyOpenGLContext() override;
    virtual void swapBuffers() override;
    virtual inline IWindow* getWindow() override;

private:

private:
    IWindow *m_window;
    EGLDisplay m_display;
    EGLSurface m_surface;
    EGLContext m_context;
};

IWindow* OpenGLES2Context::getWindow()
{
    return m_window;
}