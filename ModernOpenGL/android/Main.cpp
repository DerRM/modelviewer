#ifndef MAIN_CPP
#define MAIN_CPP

#include <android/asset_manager.h>
#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>

#include "../Common.h"

#include "../android/Window_android.h"
#include "../android/GL/OpenGLContext_android.h"
#include "../Renderer/OpenGLRenderer.h"

OpenGLRenderer* openglRenderer;
Window* window;
OpenGLES2Context* context;
ANativeActivity* nativeActivity = nullptr;
OpenGLShader shader;

bool wasDPressed = false;

struct app_state
{
    struct android_app* app;
};

bool createWindow(const char* title, int width, int height)
{
    window = new Window(title, width, height);
    window->createWindow();

    return true;
}

bool createAndroidOpenGLContext()
{
    context = new OpenGLES2Context(window);
    return context->createOpenGLContext();
}

static void app_handle_cmd(struct android_app* app, int32_t cmd)
{
    struct app_state* state = (struct app_state*)app->userData;

    switch (cmd)
    {
    case APP_CMD_INIT_WINDOW:
        if (state->app->window != NULL)
        {
            createWindow("OpenGL 4.5 Project", 1024, 768);
            window->setNativeWindow(state->app->window);
            createAndroidOpenGLContext();
            openglRenderer = new OpenGLRenderer(context);
            openglRenderer->initRenderer();
            
            openglRenderer->setupScene();

            char* vertexShader = OpenTextFile("Assets/Shader/blinn_phong_es2.vert");
            char* fragmentShader = OpenTextFile("Assets/Shader/blinn_phong_es2.frag");
            shader.createShaderProgram(vertexShader, fragmentShader);

           /* init_display(state);

            state->renderer.onCreate();
            state->renderer.onDrawFrame();
            eglSwapBuffers(state->display, state->surface);*/
        }
        break;
    case APP_CMD_TERM_WINDOW:
        context->destroyOpenGLContext();
        break;
    default:
        break;
    }
}

void FindAllModifiedFiles(const char*)
{

}

bool HasFileChanged(const char*)
{
    return false;
}

char* OpenTextFile(const char* filename)
{
    AAssetManager* assetManager = nativeActivity->assetManager;
    AAsset* asset = AAssetManager_open(assetManager, filename, AASSET_MODE_UNKNOWN);
    size_t size = (size_t)AAsset_getLength(asset);
    char* buffer = (char*)malloc(size + 1);
    AAsset_read(asset, buffer, size);
    buffer[size] = '\0';

    __android_log_print(ANDROID_LOG_VERBOSE, "Shader output", "%s", buffer);

    return buffer;
}

struct app_state app_state;

void android_main(struct android_app* state) 
{
    __android_log_print(ANDROID_LOG_VERBOSE, "Test", "test");

    nativeActivity = state->activity;

    state->userData = &app_state;
    state->onAppCmd = app_handle_cmd;
    app_state.app = state;

    FindAllModifiedFiles("Assets/Shader/*.vert");
    FindAllModifiedFiles("Assets/Shader/*.frag"); 

    static Float64 elapsedSeconds = 0.0;
    bool running = true;

    while (running)
    {
        int events;
        struct android_poll_source* source;

        while ((ALooper_pollAll(0, NULL, &events, (void**) &source)) >= 0)
        {
            if (source != NULL)
            {
                source->process(state, source);
            }

            if (state->destroyRequested != 0)
            {
                context->destroyOpenGLContext();
                running = false;
            }
        }

        if (openglRenderer)
        {
            struct timeval t1, t2;
            gettimeofday(&t1, nullptr);

            openglRenderer->renderScene(shader, elapsedSeconds);

            gettimeofday(&t2, nullptr);

            elapsedSeconds = (t2.tv_sec - t1.tv_sec);
            elapsedSeconds += (t2.tv_usec - t1.tv_usec) / (1000.0 * 1000.0);

            Log("FPS: %f", 1.0f / elapsedSeconds);
        }
    }

    //openglRenderer->deinitOGLContext();
}

#endif
