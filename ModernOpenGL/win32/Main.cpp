#pragma once



#include "../Common.h"
#include <vector>
#include <strsafe.h>

#include "../Collada/ColladaParser.h"
#include "../ObjParser.h"
#include "../win32/Window_win32.h"
#include "../Renderer/OpenGLRenderer.h"
#include "../Renderer/OpenGLShader.h"
#include "../Renderer/OpenGLTexture.h"

#if defined (OPENGL_45)
#include "../win32/GL/OpenGL45Context_win32.h"
#elif defined (OPENGLES_2)
#include "../win32/GL/OpenGLES2Context_win32.h"
#elif defined (OPENGLES_31)
#include "../win32/GL/OpenGLES31Context_win32.h"
#endif

bool running = true;
OpenGLRenderer* openglRenderer;
Window* window;
IOpenGLContext* context;

bool createWindow(LPCSTR title, int width, int height)
{
    window = new Window(title, width, height);
    window->createWindow();

    return true;
}

bool createOpenGLContext()
{
#if defined (OPENGL_45)
    context = new OpenGL45Context(window);
#elif defined (OPENGLES_2)
    context = new OpenGLES2Context(window);
#elif defined (OPENGLES_31)
    context = new OpenGLES31Context(window);
#endif
    return context->createOpenGLContext();
}

void FindAllModifiedFiles(const char* directory)
{
    WIN32_FIND_DATA find_data;
    HANDLE fileHandle = FindFirstFile(directory, &find_data);

    if (fileHandle != INVALID_HANDLE_VALUE)
    {
        Log("%s", find_data.cFileName);

        while (FindNextFile(fileHandle, &find_data))
        {
            Log("%s", find_data.cFileName);
        }
    }
}

bool HasFileChanged(const char* filename)
{
    WIN32_FIND_DATA find_data;
    HANDLE fileHandle = FindFirstFile(filename, &find_data);

    static FILETIME fileTime = {};

    if (CompareFileTime(&fileTime, &find_data.ftLastWriteTime) != 0)
    {
        fileTime = find_data.ftLastWriteTime;

        FindClose(fileHandle);
        return true;
    }

    FindClose(fileHandle);
    return false;
}


// TODO: this function is a complete hack and needs to be redone
char* OpenTextFile(const char* filename)
{

    const DWORD MAXTRIES = 5;
    const DWORD RETRYDELAY = 250;
    DWORD bytesRead = 0;

    char* result = NULL;

    HANDLE fileHandle = INVALID_HANDLE_VALUE;
    DWORD dwRetries = 0;
    BOOL bSuccess = FALSE;

    do
    {
        fileHandle = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

        if (fileHandle == INVALID_HANDLE_VALUE)
        {
            DWORD errorCode = GetLastError();
         
            if (ERROR_SHARING_VIOLATION == errorCode)
            {
                ++dwRetries;
                Sleep(RETRYDELAY);
                continue;
            }
            else
            {
                LPVOID lpMsgBuf;
                LPVOID lpDiplayBuf;

                FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR) &lpMsgBuf,0, NULL);
        
                lpDiplayBuf = LocalAlloc(LMEM_ZEROINIT, (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)"CreateFile") + 40) * sizeof(TCHAR));
                StringCchPrintf((LPTSTR)lpDiplayBuf, LocalSize(lpDiplayBuf) / sizeof(TCHAR), TEXT("%s failed with error %d: %s"), "CreateFile", errorCode, lpMsgBuf);
        
                MessageBox(NULL, (LPCTSTR)lpDiplayBuf, TEXT("ERROR"), MB_OK);

                LocalFree(lpMsgBuf);
                LocalFree(lpDiplayBuf);
                return NULL;
            }
        }

        Sleep(1);
        bSuccess = TRUE;
        break;
    } while (dwRetries < MAXTRIES);

    if (bSuccess)
    {
        LARGE_INTEGER fileSize;

        BOOL fileSizeSuccess = GetFileSizeEx(fileHandle, &fileSize);
        if (fileSizeSuccess)
        {
            char* buffer = (char*)VirtualAlloc(NULL, (SIZE_T)fileSize.QuadPart, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

            if (ReadFile(fileHandle, buffer, (DWORD)fileSize.QuadPart, &bytesRead, NULL))
            {
                result = buffer;
            }
        }

        CloseHandle(fileHandle);
    }

    return result;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;

    createWindow("OpenGL 4.5 Project", 1024, 768);
    createOpenGLContext();
    openglRenderer = new OpenGLRenderer(context);
    openglRenderer->initRenderer();

    window->setReshapeFunction(&openglRenderer->reshapeWindow);

    FindAllModifiedFiles("../Assets/Shader/*.vert");
    FindAllModifiedFiles("../Assets/Shader/*.frag");

    char* vertexShader = OpenTextFile("../Assets/Shader/blinn_phong.vert");
    char* fragmentShader = OpenTextFile("../Assets/Shader/blinn_phong.frag");

    OpenGLShader shader;
    GLuint program = shader.createShaderProgram(vertexShader, fragmentShader);

    openglRenderer->setupScene();

    LARGE_INTEGER performanceFrequency;
    QueryPerformanceFrequency(&performanceFrequency);

    Float64 elapsedSeconds = 0.0;

    while (running)
    {
        LARGE_INTEGER startCounter;
        QueryPerformanceCounter(&startCounter);

        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        
        if (msg.message == WM_QUIT)
        {
            running = false;
        }
        else
        {
            // TODO: this needs work
            if (HasFileChanged("../Assets/Shader/blinn_phong.frag"))
            {
                shader.deleteShaderProgram();
                fragmentShader = OpenTextFile("../Assets/Shader/blinn_phong.frag");
                program = shader.createShaderProgram(vertexShader, fragmentShader);
            }
         
            openglRenderer->renderScene(shader, elapsedSeconds);

            LARGE_INTEGER endCounter;
            QueryPerformanceCounter(&endCounter);

            elapsedSeconds = (Float64)(endCounter.QuadPart - startCounter.QuadPart) / (Float64)performanceFrequency.QuadPart;
        }

        Log("FPS: %f", 1.0f / elapsedSeconds);
    }

	window->destroyWindow();
	
	delete openglRenderer;
	delete window;
	
    return 0;
}
