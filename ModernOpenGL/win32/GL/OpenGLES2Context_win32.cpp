#include "../../win32/GL/OpenGLES2Context_win32.h"
#include "../../Renderer/OpenGLES2.h"

OpenGLES2Context::OpenGLES2Context(IWindow * window)
    : m_window(window),
      m_renderingContext(NULL),
      m_deviceContext(NULL),
      m_hwnd(NULL)
{
}

bool OpenGLES2Context::createTempOGLContext(HWND hwnd)
{
    HDC deviceContext = GetDC(hwnd);

    PIXELFORMATDESCRIPTOR pfd;
    BOOL bResult = SetPixelFormat(deviceContext, 1, &pfd);

    if (!bResult)
    {
        return false;
    }

    HGLRC tempOpenGLContext = wglCreateContext(deviceContext);
    wglMakeCurrent(deviceContext, tempOpenGLContext);

    if (!InitOpenGlES2CoreProfile())
    {
        Log("Error while initializing OpenGL 4.5 Core Profile calls\n");
        //return false;
    }

    if (!InitOpenGlExtensions())
    {
        Log("Error while intializing OpenGL extension calls\n");
        return false;
    }

    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(tempOpenGLContext);
    tempOpenGLContext = NULL;

    ReleaseDC(hwnd, deviceContext);
    deviceContext = 0;

    return true;
}

bool OpenGLES2Context::createModernOGLContext(HWND hwnd)
{
    m_hwnd = hwnd;

    m_deviceContext = GetDC(hwnd);

    int attributeList[] =
    {
        WGL_SUPPORT_OPENGL_ARB, TRUE,
        WGL_DRAW_TO_WINDOW_ARB, TRUE,
        WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
        WGL_COLOR_BITS_ARB, 24,
        WGL_DEPTH_BITS_ARB, 24,
        WGL_DOUBLE_BUFFER_ARB, TRUE,
        WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
        WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
        WGL_STENCIL_BITS_ARB, 8,
        0
    };

    PIXELFORMATDESCRIPTOR pixelFormatDesc;
    unsigned int formatCount;
    int pixelFormat;
    BOOL bResult = wglChoosePixelFormatARB(m_deviceContext, attributeList, NULL, 1, &pixelFormat, &formatCount);

    if (!bResult)
    {
        return false;
    }

    bResult = SetPixelFormat(m_deviceContext, pixelFormat, &pixelFormatDesc);

    if (!bResult)
    {
        return false;
    }

    int attributes[] =
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, 2,
        WGL_CONTEXT_MINOR_VERSION_ARB, 0,
        WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_ES2_PROFILE_BIT_EXT,
        0
    };

    if (IsWGLExtSupported("WGL_ARB_create_context", m_deviceContext))
    {
        m_renderingContext = wglCreateContextAttribsARB(m_deviceContext, NULL, attributes);
        wglMakeCurrent(m_deviceContext, m_renderingContext);
    }

    glClearColor(0.13f, 0.3f, 0.85f, 1.0f);

    //glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    //glFrontFace(GL_CW);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    //glEnable(GL_FRAMEBUFFER_SRGB);
    //glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    wglSwapIntervalEXT(0);

    //int glVersion[2] = { -1, -1 };
    //glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
    //glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);

    //Log("Using OpenGL: %d.%d\n", glVersion[0], glVersion[1]);
    Log("%s", glGetString(GL_VERSION));

    return true;
}


bool OpenGLES2Context::createOpenGLContext()
{
    m_window->hideWindow();
    createTempOGLContext((HWND)m_window->getNativeHandle());
    m_window->destroyWindow();
    m_window->createWindow();
    createModernOGLContext((HWND)m_window->getNativeHandle());
    m_window->showWindow();

    return true;
}

void OpenGLES2Context::destroyOpenGLContext()
{
    wglMakeCurrent(m_deviceContext, 0);
    wglDeleteContext(m_renderingContext);

    ReleaseDC(m_hwnd, m_deviceContext);
}

void OpenGLES2Context::swapBuffers()
{
    SwapBuffers(m_deviceContext);
}
