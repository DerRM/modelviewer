#pragma once

#include "../../Renderer/OpenGLContext.h"
#include "../../win32/Window_win32.h"

#include <Windows.h>

class OpenGLES31Context : public IOpenGLContext {

public:
    OpenGLES31Context(IWindow* window);

    virtual bool createOpenGLContext() override;
    virtual void destroyOpenGLContext() override;
    virtual void swapBuffers() override;
    virtual inline IWindow* getWindow() override;

private:
    bool createTempOGLContext(HWND hwnd);
    bool createModernOGLContext(HWND hwnd);

private:
    IWindow *m_window;
    HGLRC m_renderingContext; // Rendering context
    HDC m_deviceContext; // Device context
    HWND m_hwnd; // Window identifier
};

IWindow* OpenGLES31Context::getWindow()
{
    return m_window;
}