#pragma once

#include "../Common/Window.h"

#include <Windows.h>

class Window : public IWindow {

public:
    Window(const char* title, int width, int height);

    virtual void createWindow() override;
    virtual void* getNativeHandle() override;
    virtual inline const char* getTitle() override;
    virtual inline int getWidth() override;
    virtual inline int getHeight() override;
    virtual void showWindow() override;
    virtual void hideWindow() override;
    virtual void destroyWindow() override;
    virtual bool shouldClose() override;

    virtual void setReshapeFunction(reshapeFunc function) override;

private:
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

    virtual void inline setWidth(int width) override;
    virtual void inline setHeight(int height) override;

    HWND m_hWnd;
    HINSTANCE m_hInstance;
    reshapeFunc m_reshapeFunc;

    const char* m_title;
    int m_width;
    int m_height;
};

const char* Window::getTitle()
{
    return m_title;
}

int Window::getWidth()
{
    return m_width;
}

int Window::getHeight()
{
    return m_height;
}

void Window::setWidth(int width)
{
    m_width = width;
}

void Window::setHeight(int height)
{
    m_height = height;
}