#include "../Common.h"

#include <Windows.h>

void Log(const char* format, ...)
{
#ifdef _DEBUG
    char dst[1024 * 10];
    va_list arglist;
    va_start(arglist, format);
    vsprintf_s(dst, format, arglist);
    va_end(arglist);
    strcat_s(dst, "\n");
    OutputDebugStringA(dst);
#endif
}

void LogRelease(const char* format, ...)
{
    char dst[1024 * 10];
    va_list arglist;
    va_start(arglist, format);
    vsprintf_s(dst, format, arglist);
    va_end(arglist);
    strcat_s(dst, "\n");
    OutputDebugStringA(dst);
}

static const Uint64 basis = 14695981039346656037ULL;
static const Uint64 prime = 1099511628211ULL;

Uint64 hashString(const char* string)
{
    Uint64 hash = basis;

    while (*string != '\0')
    {
        hash ^= string[0];
        hash *= prime;
        ++string;
    }

    return hash;
}

Bool32 isDelimiter(const char c, const std::string& delimiters)
{
    return delimiters.find_first_of(c) != std::string::npos;
}

std::vector<std::string> tokenizeString(const std::string& string, const std::string& delimiters)
{
    std::vector<std::string> tokens;
    std::string token;
    const char* c_string = string.c_str();

    while (*c_string != '\0')
    {
        token.clear();

        while (*c_string != '\0' && !isDelimiter(*c_string, delimiters))
        {
            token.push_back(*c_string);
            c_string++;
        }

        if (token.length() > 0)
        {
            tokens.push_back(token);
        }

        while (*c_string != '\0' && isDelimiter(*c_string, delimiters))
        {
            if (delimiters != " " && isDelimiter(*c_string, delimiters) && isDelimiter(*(c_string + 1), delimiters))
            {
                tokens.push_back("");
            }

            c_string++;
        }
    }

    return tokens;
}
