#include "../win32/Window_win32.h"

bool wasDPressed = false;

LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    Window* window = (Window*)(GetWindowLongPtr(hWnd, GWLP_USERDATA));

    switch (message)
    {
    case WM_SIZE:
    {
        //openglRenderer.reshapeWindow(LOWORD(lParam), HIWORD(lParam));

        int width = LOWORD(lParam);
        int height = HIWORD(lParam);

        if (window->m_reshapeFunc) {
            window->m_reshapeFunc(width, height);
        }

        window->setWidth(width);
        window->setHeight(height);

    } break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case 'D':
            //wasDPressed = true;
            break;

        default:
            break;
        }
        break;
    case WM_KEYUP:
        switch (wParam)
        {
        case 'D':
            wasDPressed = !wasDPressed;
            break;
        default:
            break;
        }
        break;
    case WM_CLOSE:
        PostQuitMessage(0);
        return 0;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}


Window::Window(const char * title, int width, int height)
    : m_title(title),
      m_width(width),
      m_height(height),
      m_reshapeFunc(nullptr)
{
}

void Window::createWindow()
{
    WNDCLASSEX windowClass;
    HWND hWnd;

    DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

    m_hInstance = GetModuleHandle(NULL);

    windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    windowClass.lpfnWndProc = (WNDPROC)Window::WndProc;
    windowClass.cbClsExtra = 0;
    windowClass.cbWndExtra = 0;
    windowClass.hInstance = m_hInstance;
    windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    windowClass.hIconSm = windowClass.hIcon;
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    windowClass.hbrBackground = NULL;
    windowClass.lpszMenuName = NULL;
    windowClass.lpszClassName = m_title;
    windowClass.cbSize = sizeof(WNDCLASSEX);

    RegisterClassEx(&windowClass);

    m_hWnd = CreateWindowEx(dwExStyle, m_title, m_title, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, m_width, m_height, NULL, NULL, m_hInstance, NULL);
    SetWindowLongPtr(m_hWnd, GWLP_USERDATA, (LONG_PTR)this);
}

void * Window::getNativeHandle()
{
    return (void*)m_hWnd;
}

void Window::showWindow()
{
    ShowWindow(m_hWnd, SW_SHOW);
    SetForegroundWindow(m_hWnd);
    SetFocus(m_hWnd);
}

void Window::hideWindow()
{
    ShowWindow(m_hWnd, SW_HIDE);
}

void Window::destroyWindow()
{
    DestroyWindow(m_hWnd);
    m_hWnd = NULL;
}

bool Window::shouldClose()
{
    return false;
}

void Window::setReshapeFunction(reshapeFunc function)
{
    m_reshapeFunc = function;
}
