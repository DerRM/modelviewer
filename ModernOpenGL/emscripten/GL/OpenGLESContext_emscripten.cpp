#include "../../emscripten/GL/OpenGLESContext_emscripten.h"
#include "../../Renderer/OpenGLES2.h"

OpenGLES2Context::OpenGLES2Context(IWindow * window)
    : m_window(window),
      m_display(EGL_NO_DISPLAY),
      m_surface(EGL_NO_SURFACE),
      m_context(EGL_NO_CONTEXT)
{
}

bool OpenGLES2Context::createOpenGLContext()
{
    const EGLint attribs[] = {
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_DEPTH_SIZE, 8,
        EGL_NONE
    };

    EGLint format, w, h;
    EGLConfig config;
    EGLint numConfigs;

    m_display = eglGetDisplay((EGLNativeDisplayType) ((WindowEMS*)m_window)->getDisplay());
	if (m_display == EGL_NO_DISPLAY)
	{
		return false;
	}
	
    if (!eglInitialize(m_display, 0, 0)) 
	{
		return false;
	}
	
	if (!eglGetConfigs(m_display, nullptr, 0, &numConfigs))
	{
		return false;
	}
	
    if (!eglChooseConfig(m_display, attribs, &config, 1, &numConfigs))
	{
		return false;
	}
	
    m_surface = eglCreateWindowSurface(m_display, config, (EGLNativeWindowType)m_window->getNativeHandle(), NULL);
	
	if (m_surface == EGL_NO_SURFACE)
	{
		return false;
	}

    EGLint attribList[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };

    m_context = eglCreateContext(m_display, config, NULL, attribList);
	if (m_context == EGL_NO_CONTEXT)
	{
		return false;
	}
	
    if (eglMakeCurrent(m_display, m_surface, m_surface, m_context) == EGL_FALSE)
    {
        return false;
    }

    eglQuerySurface(m_display, m_surface, EGL_WIDTH, &w);
    eglQuerySurface(m_display, m_surface, EGL_HEIGHT, &h);

    m_window->setWidth(w);
    m_window->setHeight(h);

    //InitOpenGlExtensions();

    return true;
}

void OpenGLES2Context::destroyOpenGLContext()
{
    if (m_display != EGL_NO_DISPLAY)
    {
        eglMakeCurrent(m_display, EGL_NO_DISPLAY, EGL_NO_SURFACE, EGL_NO_SURFACE);

        if (m_context != EGL_NO_CONTEXT)
        {
            eglDestroyContext(m_display, m_context);
        }

        if (m_surface != EGL_NO_SURFACE)
        {
            eglDestroySurface(m_display, m_surface);
        }

        eglTerminate(m_display);
    }

    m_display = EGL_NO_DISPLAY;
    m_context = EGL_NO_CONTEXT;
    m_surface = EGL_NO_SURFACE;
}

void OpenGLES2Context::swapBuffers()
{
    eglSwapBuffers(m_display, m_surface);
}
