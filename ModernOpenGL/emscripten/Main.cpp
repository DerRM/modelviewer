#ifndef MAIN_CPP
#define MAIN_CPP

#include "../Common.h"
#include <vector>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <unistd.h>
#include <fstream>
#include <sys/time.h>

#include "../Collada/ColladaParser.h"
#include "../ObjParser.h"
#include "../emscripten/Window_emscripten.h"
#include "../Renderer/OpenGLRenderer.h"
#include "../Renderer/OpenGLShader.h"
#include "../Renderer/OpenGLTexture.h"

#include "../emscripten/GL/OpenGLESContext_emscripten.h"

OpenGLRenderer* openglRenderer;
WindowEMS* window;
IOpenGLContext* context;

bool wasDPressed = false;

static bool createWindow(const char* title, int width, int height)
{
	window = new WindowEMS(title, width, height);
	window->createWindow();
	
    return true;
}

static bool createOpenGLContext() 
{
	context = new OpenGLES2Context(window);
	return context->createOpenGLContext();
}

void FindAllModifiedFiles(const char* directory)
{

}

bool HasFileChanged(const char* filename)
{
    return false;
}

char* OpenTextFile(const char* filename)
{
    char* result = NULL;

    std::ifstream ifs(filename, std::ios::in | std::ios::binary);
    ifs.seekg(0, std::ios::end);
    long fileLength = ifs.tellg();

    result = new char[fileLength + 1];
    ifs.seekg(0, std::ios::beg);
    ifs.read(result, fileLength);
    ifs.close();

    result[fileLength] = 0;

    return result;
}

OpenGLShader shader;
Float64 elapsedSeconds = 0.0;


void infinite_loop()
{
	while(XPending(window->getDisplay()))
	{
		XEvent xevent;
		XNextEvent(window->getDisplay(), &xevent);

		if (xevent.type == DestroyNotify)
		{
			emscripten_cancel_main_loop();
		}
	}

	//struct timeval t1, t2;
	//gettimeofday(&t1, nullptr);

	elapsedSeconds = (1.0 / 60.0);
	
	openglRenderer->renderScene(shader, elapsedSeconds);

	//gettimeofday(&t2, nullptr);

	
	//elapsedSeconds = (t2.tv_sec - t1.tv_sec);
	//elapsedSeconds += (t2.tv_usec - t1.tv_usec) / (1000.0 * 1000.0);

	 
	
	//Log("FPS: %f", 1.0f / elapsedSeconds);
}

int main(int argc, char* argv[])
{
    createWindow("OpenGLES 2.0 Project", 1024, 768);
	createOpenGLContext();
	
	openglRenderer = new OpenGLRenderer(context);
	openglRenderer->initRenderer();

	window->setReshapeFunction(&openglRenderer->reshapeWindow);
	
    FindAllModifiedFiles("Assets/Shader/*.vert");
    FindAllModifiedFiles("Assets/Shader/*.frag");

    char* vertexShader = OpenTextFile("Assets/Shader/blinn_phong_es2.vert");
	//printf("vertex shader: %s\n", vertexShader);
    char* fragmentShader = OpenTextFile("Assets/Shader/blinn_phong_es2.frag");
	//printf("fragment shader: %s\n", fragmentShader);
	
    GLuint program = shader.createShaderProgram(vertexShader, fragmentShader);

    openglRenderer->setupScene();

	emscripten_set_main_loop(infinite_loop, 60, 1);
    
	delete openglRenderer;
	window->destroyWindow();
	delete window;

    return 0;
}

#endif
