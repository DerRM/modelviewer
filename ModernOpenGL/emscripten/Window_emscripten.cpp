#include "../emscripten/Window_emscripten.h"



WindowEMS::WindowEMS(const char* title, int width, int height)
	: m_display(nullptr),
	  m_win(None),
	  m_title(title),
	  m_width(width),
	  m_height(height)
{
}

void WindowEMS::createWindow()
{
	m_display = XOpenDisplay(nullptr);

    if (!m_display)
    {
      //  Log("Failed to open X display");
      //  exit(1);
	  return;
    }

	Window root = DefaultRootWindow(m_display);
	
	XSetWindowAttributes swa;
	swa.event_mask = ExposureMask | PointerMotionMask | KeyPressMask;
	
    //Log("Creating window");

    m_win = XCreateWindow(m_display, root, 0, 0, m_width, m_height, 0, CopyFromParent, InputOutput, CopyFromParent, CWEventMask, &swa);

    if (!m_win)
    {
        //Log("Failed to create window");
        //exit(1);
		return;
    }
	
	XSetWindowAttributes xattr;
	xattr.override_redirect = False;
	XChangeWindowAttributes(m_display, m_win, CWOverrideRedirect, &xattr);
	
	XWMHints hints;
	hints.input = True;
	hints.flags = InputHint;
	XSetWMHints(m_display, m_win, &hints);

    //Log("Mapping window");
    XMapWindow(m_display, m_win);
	XStoreName(m_display, m_win, m_title);
	
    Atom wm_state = XInternAtom(m_display, "_NET_WM_STATE", False);

	XEvent xev;
	//memset(&xev, 0, sizeof(xev));
	xev.type = ClientMessage;
	xev.xclient.window = m_win;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = 1;
	xev.xclient.data.l[1] = False;
	
	XSendEvent(m_display, DefaultRootWindow(m_display), False, SubstructureNotifyMask, &xev);
}

void WindowEMS::destroyWindow()
{
	XDestroyWindow(m_display, m_win);
    XCloseDisplay(m_display);
}

void* WindowEMS::getNativeHandle()
{
	return (void*)m_win;
}

void WindowEMS::showWindow()
{
}

void WindowEMS::hideWindow()
{
}

bool WindowEMS::shouldClose()
{
	return false;
}

void WindowEMS::setReshapeFunction(reshapeFunc function)
{
	
}
