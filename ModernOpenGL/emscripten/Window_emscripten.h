#ifndef __WINDOW_EMSCRIPTEN_H__
#define __WINDOW_EMSCRIPTEN_H__

#include "../Common/Window.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <emscripten.h>

class WindowEMS : public IWindow {

public:
    WindowEMS(const char* title, int width, int height);

	virtual void createWindow() override;
    virtual void* getNativeHandle() override;
    virtual inline const char* getTitle() override;
    virtual inline int getWidth() override;
    virtual inline int getHeight() override;
    virtual void showWindow() override;
    virtual void hideWindow() override;
    virtual void destroyWindow() override;
    virtual bool shouldClose() override;

	virtual void setReshapeFunction(reshapeFunc function) override;
	
	inline Display* getDisplay();
	
private:
    virtual void inline setWidth(int width) override;
    virtual void inline setHeight(int height) override;

	Display* m_display;
	Window m_win;

	const char* m_title;
	int m_width;
	int m_height;
};

int WindowEMS::getWidth() 
{
	return m_width;
}

int WindowEMS::getHeight() 
{
	return m_height;
}

const char* WindowEMS::getTitle()
{
	return m_title;
}

Display* WindowEMS::getDisplay() 
{
	return m_display;
}

void WindowEMS::setWidth(int width)
{
	m_width = width;
}

void WindowEMS::setHeight(int height)
{
	m_height = height;
}

#endif