#pragma once

#include "Common.h"
#include <vector>
#include <string>

typedef chs::Vector3 Position;
typedef Vector2 Texcoord2;
typedef chs::Vector3 Normal;
typedef chs::Vector4 BoneIndex;
typedef chs::Vector4 BoneWeight;
typedef std::vector<std::string> StringArray;
typedef std::vector<Position> PositionArray;
typedef std::vector<Texcoord2> TexCoord2Array;
typedef std::vector<Normal> NormalArray;
typedef std::vector<BoneIndex> BoneIndexArray;
typedef std::vector<BoneWeight> BoneWeightArray;
typedef std::vector<Uint32> IndexArray;


class Parser
{
public:
    Parser();
    virtual ~Parser();

    virtual Bool32 openFile(const char* pFilename) = 0;
    virtual void readFile() = 0;
    virtual void closeFile() = 0;
};

