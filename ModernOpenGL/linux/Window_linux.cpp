#include "Window_linux.h"
#include "../Common.h"

WindowX11::WindowX11(const char* title, int width, int height)
    : m_reshapeFunc(nullptr)
    , m_title(title)
    , m_width(width)
    , m_height(height)
    , m_display(nullptr)
    , m_win(None)
    , m_cmap(None)
    , m_wmDeleteMessage(0)
{
}

void  WindowX11::createWindow()
{
    m_display = XOpenDisplay(nullptr);

    if (!m_display)
    {
        Log("Failed to open X display");
        exit(1);
    }

    static int visual_attribs[] =
    {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8,
        GLX_DOUBLEBUFFER, True,
        None
    };

    int glx_major, glx_minor;

    if (!glXQueryVersion(m_display, &glx_major, &glx_minor) || ((glx_major == 1) && (glx_minor < 3)) || (glx_major < 1))
    {
        Log("Invalid GLX version");
        exit(1);
    }

    Log("Getting matching framebuffer configs");

    int fbcount;
    GLXFBConfig* fbc = glXChooseFBConfig(m_display, DefaultScreen(m_display), visual_attribs, &fbcount);

    if (!fbc)
    {
        Log("Failed to retrieve a framebuffer config");
        exit(1);
    }

    Log("Found %d matching FP configs.", fbcount);

    Log("Getting XVisualInfos");
    int best_fbc = -1, worst_fbc = -1, best_num_samp = -1, worst_num_samp = 999;

    for (int i = 0; i < fbcount; ++i)
    {
        XVisualInfo* vi = glXGetVisualFromFBConfig(m_display, fbc[i]);
        if (vi)
        {
            int samp_buf, samples;
            glXGetFBConfigAttrib(m_display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
            glXGetFBConfigAttrib(m_display, fbc[i], GLX_SAMPLES, &samples);

            Log("   Matching fbconfig %d, visual ID 0x%2x: SAMPLE_BUFFERS = %d, SAMPLES = %d",i, vi->visualid, samp_buf, samples);

            if (best_fbc < 0 || (samp_buf && samples > best_num_samp))
            {
                best_fbc = i; best_num_samp = samples;
            }
            if (worst_fbc < 0 || !samp_buf || samples < worst_num_samp)
            {
                worst_fbc = i; worst_num_samp = samples;
            }
        }

        XFree(vi);
    }

    m_fbc = fbc[best_fbc];
    XFree(fbc);

    XVisualInfo* vi = glXGetVisualFromFBConfig(m_display, m_fbc);
    Log("Chose visual ID = 0x%x", vi->visualid);

    Log("Creating colormap");

    XSetWindowAttributes swa;
    swa.colormap = m_cmap = XCreateColormap(m_display, RootWindow(m_display, vi->screen), vi->visual, AllocNone);
    swa.background_pixmap = None;
    swa.border_pixel = 0;
    swa.event_mask = StructureNotifyMask | ExposureMask | KeyPressMask;

    Log("Creating window");

    m_win = XCreateWindow(m_display, RootWindow(m_display, vi->screen), 0, 0, static_cast<unsigned int>(m_width), static_cast<unsigned int>(m_height), 0, vi->depth, InputOutput, vi->visual, CWBorderPixel | CWColormap | CWEventMask, &swa);

    if (!m_win)
    {
        Log("Failed to create window");
        exit(1);
    }

    m_wmDeleteMessage = XInternAtom(m_display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(m_display, m_win, &m_wmDeleteMessage, 1);

    XFree(vi);

    XStoreName(m_display, m_win, m_title);

    Log("Mapping window");
    XMapWindow(m_display, m_win);
}

void* WindowX11::getNativeHandle()
{
    return reinterpret_cast<void*>(m_win);
}

void  WindowX11::showWindow()
{

}

void  WindowX11::hideWindow()
{

}

void  WindowX11::destroyWindow()
{
    XDestroyWindow(m_display, m_win);
    XFreeColormap(m_display, m_cmap);
    XCloseDisplay(m_display);
}

bool  WindowX11::shouldClose()
{
    while(XPending(m_display))
    {
        XEvent xevent;
        XNextEvent(m_display, &xevent);

        if (xevent.type == Expose)
        {
            XWindowAttributes xwinAttr;
            XGetWindowAttributes(m_display, m_win, &xwinAttr);
            m_width = xwinAttr.width;
            m_height = xwinAttr.height;
            if (m_reshapeFunc) {
                m_reshapeFunc(m_width, m_height);
            }
            //openglRenderer->reshapeWindow(xwinAttr.width, xwinAttr.height);
        }

        if (xevent.type == ClientMessage)
        {
            if (static_cast<Atom>(xevent.xclient.data.l[0]) == m_wmDeleteMessage)
            {
                return false;
            }
        }
    }

    return true;
}

void WindowX11::setReshapeFunction(reshapeFunc function)
{
    m_reshapeFunc = function;
}
