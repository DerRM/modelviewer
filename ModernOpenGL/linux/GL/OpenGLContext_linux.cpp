#include "../../linux/GL/OpenGLContext_linux.h"
#include "../../Renderer/OpenGL45.h"

#include <cstring>

typedef GLXContext(*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

static bool isExtensionSupported(const char* extList, const char* extension)
{
    const char* start;
    const char* where, *terminator;

    where = strchr(extension, ' ');
    if (where || *extension == '\0')
    {
        return false;
    }

    for (start = extList;;)
    {
        where = strstr(start, extension);

        if (!where)
        {
            break;
        }

        terminator = where + strlen(extension);

        if (where == start || *(where - 1) == ' ')
        {
            if (*terminator == ' ' || *terminator == '\0')
            {
                return true;
            }
        }

        start = terminator;
    }

    return false;
}

static bool ctxErrorOccured = false;
static int ctxErrorHandler(Display* /*dpy*/, XErrorEvent* /*ev*/)
{
    ctxErrorOccured = true;
    return 0;
}

#ifdef DefaultScreen
#undef DefaultScreen
#define DefaultScreen(dpy) 	((reinterpret_cast<_XPrivDisplay>(dpy))->default_screen)
#endif

bool OpenGL45Context::createModernOGLContext(Display* display, Window window, GLXFBConfig bestFbc)
{
    m_display = display;
    m_win = window;

    const char* glxExts = glXQueryExtensionsString(display, DefaultScreen(display));

    glXCreateContextAttribsARBProc glXCreateContextAttribsARB = nullptr;
    glXCreateContextAttribsARB = reinterpret_cast<glXCreateContextAttribsARBProc>(glXGetProcAddressARB(reinterpret_cast<const GLubyte *>("glXCreateContextAttribsARB")));

    ctxErrorOccured = false;

    int(*oldHandler)(Display*, XErrorEvent*) = XSetErrorHandler(&ctxErrorHandler);

    if (!isExtensionSupported(glxExts, "GLX_ARB_create_context") || !glXCreateContextAttribsARB)
    {
        Log("glXCreateContextAttribsARB() not found ... using old-style GLX context");
        m_ctx = glXCreateNewContext(display, bestFbc, GLX_RGBA_TYPE, nullptr, True);
    }
    else
    {
        int context_attribs[] =
        {
            GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
            GLX_CONTEXT_MINOR_VERSION_ARB, 6,
            None
        };

        Log("Creating context");

        m_ctx = glXCreateContextAttribsARB(display, bestFbc, nullptr, True, context_attribs);

        XSync(display, False);

        if (!ctxErrorOccured && m_ctx)
        {
            Log("Created valid OpenGL context");
        }
        else
        {
            context_attribs[1] = 1;
            context_attribs[3] = 0;

            Log("Failed to create valid OpenGL context ... using old-style GLX context");
            m_ctx = glXCreateContextAttribsARB(display, bestFbc, nullptr, True, context_attribs);
        }
    }

    XSync(display, False);
    XSetErrorHandler(oldHandler);

    if (ctxErrorOccured || !m_ctx)
    {
        Log("Failed to create an OpenGL context");
        exit(1);
    }

    if (!glXIsDirect(display, m_ctx))
    {
        Log("Indirect GLX rendering context obtained");
    }
    else
    {
        Log("Direct GLX rendering context obtained");
    }

    Log("Making context current");

    glXMakeCurrent(display, window, m_ctx);

    if (!InitOpenGl45CoreProfile())
    {
        Log("Error while initializing OpenGL 4.5 Core Profile calls\n");
        return false;
    }

    if (!InitOpenGlExtensions())
    {
        Log("Error while intializing OpenGL extension calls\n");
        return false;
    }

    glClearColor(0.13f, 0.3f, 0.85f, 1.0f);

    //glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    //glFrontFace(GL_CW);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_FRAMEBUFFER_SRGB);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    int glVersion[2] = { -1, -1 };
    glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
    glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);

    Log("Using OpenGL: %d.%d\n", glVersion[0], glVersion[1]);

    return true;
}

OpenGL45Context::OpenGL45Context(IWindow* window)
    : m_window(window)
    , m_display(nullptr)
    , m_win(None)
    , m_ctx(nullptr)
    , m_fbc(nullptr)
{
    WindowX11* tmp_window = reinterpret_cast<WindowX11*>(m_window);
    m_display = tmp_window->m_display;
    m_win = tmp_window->m_win;
    m_fbc = tmp_window->m_fbc;
}

bool OpenGL45Context::createOpenGLContext()
{
    return createModernOGLContext(m_display, m_win, m_fbc);
}

void OpenGL45Context::destroyOpenGLContext()
{
    glXMakeCurrent(m_display, 0, nullptr);
    glXDestroyContext(m_display, m_ctx);
}

void OpenGL45Context::swapBuffers()
{
    glXSwapBuffers(m_display, m_win);
}
