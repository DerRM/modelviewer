#pragma once

#include "../../Renderer/OpenGLContext.h"
#include "../../linux/Window_linux.h"

#include <GL/glx.h>

class OpenGL45Context : public IOpenGLContext {

public:
    OpenGL45Context(IWindow* window);

    virtual bool createOpenGLContext() override;
    virtual void destroyOpenGLContext() override;
    virtual void swapBuffers() override;
    virtual inline IWindow* getWindow() override;

private:
    bool createModernOGLContext(Display* display, Window window, GLXFBConfig config);

private:
    IWindow *m_window;
    Display* m_display;
    Window m_win;
    GLXContext m_ctx;
    GLXFBConfig m_fbc;
};

IWindow* OpenGL45Context::getWindow()
{
    return m_window;
}
