#pragma once

#include "../Common/Window.h"

#include <GL/glx.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

class WindowX11 : public IWindow {

    friend class OpenGL45Context;
public:
    WindowX11(const char* title, int width, int height);

    virtual void createWindow() override;
    virtual void* getNativeHandle() override;
    virtual inline const char* getTitle() override;
    virtual inline int getWidth() override;
    virtual inline int getHeight() override;
    virtual void showWindow() override;
    virtual void hideWindow() override;
    virtual void destroyWindow() override;
    virtual bool shouldClose() override;

    virtual void setReshapeFunction(reshapeFunc funtion) override;

private:

    virtual void inline setWidth(int width) override;
    virtual void inline setHeight(int height) override;

    reshapeFunc m_reshapeFunc;
    const char* m_title;
    int m_width;
    int m_height;

    Display* m_display;
    Window m_win;
    Colormap m_cmap;
    Atom m_wmDeleteMessage;
    GLXFBConfig m_fbc;
};

const char* WindowX11::getTitle()
{
    return m_title;
}

int WindowX11::getWidth()
{
    return m_width;
}

int WindowX11::getHeight()
{
    return m_height;
}

void WindowX11::setWidth(int width)
{
    m_width = width;
}

void WindowX11::setHeight(int height)
{
    m_height = height;
}
