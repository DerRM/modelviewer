//
//  Window_cocoa.m
//  ModernOpenGL
//
//  Created by Christopher Sierigk on 05.06.17.
//
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import "Window_cocoa.h"
#import "WindowNS.h"
#include <crt_externs.h>

#if MAC_OS_X_VERSION_MAX_ALLOWED < 101200
#define NSWindowStyleMaskBorderless NSBorderlessWindowMask
#define NSWindowStyleMaskClosable NSClosableWindowMask
#define NSWindowStyleMaskMiniaturizable NSMiniaturizableWindowMask
#define NSWindowStyleMaskResizable NSResizableWindowMask
#define NSWindowStyleMaskTitled NSTitledWindowMask
#define NSEventModifierFlagCommand NSCommandKeyMask
#define NSEventModifierFlagControl NSControlKeyMask
#define NSEventModifierFlagOption NSAlternateKeyMask
#define NSEventModifierFlagShift NSShiftKeyMask
#define NSEventModifierFlagDeviceIndependentFlagsMask NSDeviceIndependentModifierFlagsMask
#define NSEventMaskAny NSAnyEventMask
#define NSEventTypeApplicationDefined NSApplicationDefined
#define NSEventTypeKeyUp NSKeyUp
#endif

StateNS* g_state = NULL;
WindowNS* g_window = NULL;

static const NSRange kEmptyRange = { NSNotFound, 0 };

@interface MyApplication : NSApplication
{
    NSArray* nibObjects;
}
@end

@implementation MyApplication

- (void)sendEvent:(NSEvent *)event
{
    if ([event type] == NSEventTypeKeyUp && ([event modifierFlags] & NSEventModifierFlagCommand))
    {
        [[self keyWindow] sendEvent:event];
    }
    else
    {
        [super sendEvent:event];
    }
}
    
- (void)doNothing:(id)object
{
}

- (void)loadMainMenu
{
#if MAC_OS_X_VERSION_MAX_ALLOWED >= 100800
    [[NSBundle mainBundle] loadNibNamed:@"MainMenu" owner:NSApp topLevelObjects:&nibObjects];
#else
    [[NSBundle mainBundle] loadNibNamed:@"MainMenu" owner:NSApp];
#endif
}

@end

@interface MyApplicationDelegate : NSObject
@end

@implementation MyApplicationDelegate
    
    - (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication*)sender
    {
        return NSTerminateCancel;
    }
    
    - (void)applicationDidChangeScreenParameters:(NSNotification*)notification
    {
        
    }
    
    - (void)applicationDidFinishLaunching:(NSNotification*)notification
    {
        [NSApp stop:nil];
        
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
        NSEvent* event = [NSEvent otherEventWithType:NSEventTypeApplicationDefined
                                            location:NSMakePoint(0, 0)
                                       modifierFlags:0
                                           timestamp:0
                                        windowNumber:0
                                             context:nil
                                             subtype:0
                                               data1:0
                                               data2:0];
        [NSApp postEvent:event atStart:YES];
        [pool drain];
    }
    
    - (void)applicationDidHide:(NSNotification*)notification
    {
        
    }

@end

@interface MyWindow : NSWindow {}
@end

@implementation MyWindow

- (BOOL)canBecomeKeyWindow
{
    return YES;
}
    
- (BOOL)canBecomeMainWindow
{
    return YES;
}

@end

@interface MyWindowDelegate : NSObject<NSWindowDelegate>
{
    WindowNS* window;
}
    
    - (id)initWindow:(WindowNS *)initWindow;
    
@end

@implementation MyWindowDelegate

- (id)initWindow:(WindowNS *)initWindow
{
    self = [super init];
    if (self != nil)
        window = initWindow;
    
    return self;
}

- (void)windowDidResignKey:(NSNotification*)notification
{
    printf("Window did resign key\n");
    [window->object miniaturize:nil];
}

- (void)windowDidResize:(NSNotification*)notification
{
    printf("Window did resize\n");
    
    [g_context->object update];
    
    const NSRect contentRect = [window->view frame];
    if (window->reshapeFunc)
        window->reshapeFunc(contentRect.size.width, contentRect.size.height);
}

- (void)windowDidMove:(NSNotification*)notification
{
    printf("Window did move\n");
}

-(void)windowDidMiniaturize:(NSNotification *)notification
{
    printf("Window did miniaturize\n");
}

- (BOOL)windowShouldClose:(id)sender
{
    printf("Window should close\n");
    window->shouldClose = true;
    return NO;
}

- (void)windowDidDeminiaturize:(NSNotification *)notification
{
    printf("Window did deminiaturize\n");
}

- (void)windowDidBecomeKey:(NSNotification *)notification
{
    printf("Window did become key\n");
}

@end

@interface MyContentView : NSView <NSTextInputClient>
    {
        WindowNS* window;
        NSTrackingArea* trackingArea;
        NSMutableAttributedString* markedText;
    }
    
    - (id)initWithWindow:(WindowNS*)initWindow;

@end

@implementation MyContentView

    + (void)initialize
    {
        if (self == [MyContentView class])
        {
        }
    }
    
    - (id)initWithWindow:(WindowNS *)initWindow
    {
        self = [super init];
        if (self != nil)
        {
            window = initWindow;
            trackingArea = nil;
            markedText = [[NSMutableAttributedString alloc] init];
            
            [self updateTrackingAreas];
            [self registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
        }
        
        return self;
    }

    - (void)dealloc
    {
        [trackingArea release];
        [markedText release];
        [super dealloc];
    }
    
    - (BOOL)isOpaque
    {
        printf("is opaque\n");
        return YES;
    }
    
    - (BOOL)canBecomeKeyView
    {
        printf("can become key view\n");
        return YES;
    }
    
    - (BOOL)acceptsFirstResponder
    {
        printf("accepts first responder\n");
        return YES;
    }
    
    - (BOOL)wantsUpdateLayer
    {
        printf("wants update layer\n");
        return YES;
    }
    
    - (id)makeBackingLayer
    {
        printf("make backing layer\n");
        if (window->layer)
            return window->layer;
        
        return [super makeBackingLayer];
    }
    
    - (void)cursorUpdate:(NSEvent *)event
    {
        printf("cursor update\n");
    }
    
    - (void)mouseDown:(NSEvent *)event
    {
        printf("mouse down\n");
    }
    
    - (void)mouseDragged:(NSEvent *)event
    {
        printf("mouse dragged\n");
        [self mouseMoved:event];
    }
    
    - (void)mouseUp:(NSEvent *)event
    {
        printf("mouse up\n");
    }
    
    - (void)mouseMoved:(NSEvent *)event
    {
        printf("mouse moved\n");
    }
    
    - (void)rightMouseDown:(NSEvent *)event
    {
        printf("right mouse down\n");
    }
    
    - (void)rightMouseDragged:(NSEvent *)event
    {
        printf("right mouse dragged\n");
        [self mouseMoved:event];
    }
    
    - (void)rightMouseUp:(NSEvent *)event
    {
        printf("right mouse up\n");
    }
    
    - (void)otherMouseDown:(NSEvent *)event
    {
        printf("other mouse down\n");
    }
    
    - (void)otherMouseDragged:(NSEvent *)event
    {
        printf("other mouse dragged\n");
        [self mouseMoved:event];
    }
    
    - (void)otherMouseUp:(NSEvent *)event
    {
        printf("other mouse up\n");
    }
    
    - (void)mouseExited:(NSEvent *)event
    {
        printf("mouse exited\n");
    }
    
    - (void)mouseEntered:(NSEvent *)event
    {
        printf("mouse entered\n");
    }
    
    - (void)viewDidChangeBackingProperties
    {
        printf("view did change backing properties\n");
    }
    
    - (void)drawRect:(NSRect)rect
    {
        printf("drawRect\n");
    }
    
    - (void)updateTrackingAreas
    {
        printf("update tracking areas\n");
        if (trackingArea != nil)
        {
            [self removeTrackingArea:trackingArea];
            [trackingArea release];
        }
        
        const NSTrackingAreaOptions options = NSTrackingMouseEnteredAndExited |
                                              NSTrackingActiveInKeyWindow |
                                              NSTrackingEnabledDuringMouseDrag |
                                              NSTrackingCursorUpdate |
                                              NSTrackingInVisibleRect |
                                              NSTrackingAssumeInside;
        
        trackingArea = [[NSTrackingArea alloc] initWithRect:[self bounds] options:options owner:self userInfo:nil];
        
        [self addTrackingArea:trackingArea];
        [super updateTrackingAreas];
    }
    
    - (void)keyDown:(NSEvent *)event
    {
        printf("key down\n");
    }
    
    - (void)flagsChanged:(NSEvent *)event
    {
        printf("flags changed\n");
    }
    
    - (void)keyUp:(NSEvent *)event
    {
        printf("key up\n");
    }
    
    -(void)scrollWheel:(NSEvent *)event
    {
        printf("scroll wheel\n");
    }
    
    - (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender
    {
        printf("dragging entered\n");
        if ((NSDragOperationGeneric & [sender draggingSourceOperationMask]) == NSDragOperationGeneric)
        {
            [self setNeedsDisplay:YES];
            return NSDragOperationGeneric;
        }
        
        return NSDragOperationNone;
    }
    
    - (BOOL)prepareForDragOperation:(id<NSDraggingInfo>)sender
    {
        printf("prepare for drag operation\n");
        [self setNeedsDisplay:YES];
        return YES;
    }
    
    - (BOOL)performDragOperation:(id<NSDraggingInfo>)sender
    {
        printf("perform drag operation\n");
        return YES;
    }
    
    - (void)concludeDragOperation:(id<NSDraggingInfo>)sender
    {
        printf("conclude drag operation\n");
        [self setNeedsDisplay:YES];
    }
    
    - (BOOL)hasMarkedText
    {
        printf("has marked text\n");
        return [markedText length] > 0;
    }
    
    - (NSRange)markedRange
    {
        printf("marked range\n");
        if ([markedText length] > 0)
        {
            return NSMakeRange(0, [markedText length] - 1);
        }
        else
        {
            return kEmptyRange;
        }
    }
    
    - (NSRange)selectedRange
    {
        printf("selected range\n");
        return kEmptyRange;
    }
    
    - (void)setMarkedText:(id)string selectedRange:(NSRange)selectedRange replacementRange:(NSRange)replacementRange
    {
        printf("set marked text\n");
        
        if ([string isKindOfClass:[NSAttributedString class]])
        {
            [markedText initWithAttributedString:string];
        }
        else
        {
            [markedText initWithString:string];
        }
    }
    
    - (void)unmarkText
    {
        printf("unmark text\n");
        [[markedText mutableString] setString:@""];
    }
    
    - (NSArray*)validAttributesForMarkedText
    {
        printf("valid attributes for marked text\n");
        return [NSArray array];
    }
    
    - (NSAttributedString*)attributedSubstringForProposedRange:(NSRange)range actualRange:(NSRangePointer)actualRange
    {
        printf("attributed substring for proposed range\n");
        return nil;
    }
    
    - (NSUInteger)characterIndexForPoint:(NSPoint)point
    {
        printf("character index for point\n");
        return 0;
    }
    
    - (NSRect)firstRectForCharacterRange:(NSRange)range actualRange:(NSRangePointer)actualRange
    {
        printf("first rect for character range\n");
        return NSMakeRect(0, 0, 0, 0);
    }
    
    - (void)insertText:(id)string replacementRange:(NSRange)replacementRange
    {
        printf("insert text\n");
    }
    
    - (void)doCommandBySelector:(SEL)selector
    {
        printf("do command by selector\n");
    }
    
@end

static void createMenuBar(void)
{
    size_t i;
    NSString* appName = nil;
    NSDictionary* bundleInfo = [[NSBundle mainBundle] infoDictionary];
    NSString* nameKeys[] =
    {
        @"CFBundleDisplayName",
        @"CFBundleName",
        @"CFBundleExecutable",
    };
    
    for (i = 0; i < sizeof(nameKeys) / sizeof(nameKeys[0]); i++)
    {
        id name = [bundleInfo objectForKey:nameKeys[i]];
        if (name && [name isKindOfClass:[NSString class]] && ![name isEqualToString:@""])
        {
            appName = name;
            break;
        }
    }
    
    if (!appName)
    {
        char** progname = _NSGetProgname();
        if (progname && *progname)
        {
            appName = [NSString stringWithUTF8String:*progname];
        }
        else
        {
            appName = @"MacOS Test Application";
        }
    }
    
    NSMenu* bar = [[NSMenu alloc] init];
    [NSApp setMainMenu:bar];
    
    NSMenuItem* appMenuItem = [bar addItemWithTitle:@"" action:NULL keyEquivalent:@""];
    NSMenu* appMenu = [[NSMenu alloc] init];
    [appMenuItem setSubmenu:appMenu];
    
    [appMenu addItemWithTitle:[NSString stringWithFormat:@"About %@", appName] action:@selector(orderFrontStandardAboutPanel:) keyEquivalent:@""];
    [appMenu addItem:[NSMenuItem separatorItem]];
    NSMenu* servicesMenu = [[NSMenu alloc] init];
    [NSApp setServicesMenu:servicesMenu];
    [[appMenu addItemWithTitle:@"Services" action:NULL keyEquivalent:@""] setSubmenu:servicesMenu];
    [servicesMenu release];
    [appMenu addItem:[NSMenuItem separatorItem]];
    [appMenu addItemWithTitle:[NSString stringWithFormat:@"Hide %@", appName] action:@selector(hide:) keyEquivalent:@"h"];
    [appMenu addItemWithTitle:@"Hide Others" action:@selector(hideOtherApplications:) keyEquivalent:@"h"];
    [appMenu addItemWithTitle:@"Show All" action:@selector(unhideAllApplications:) keyEquivalent:@""];
    [appMenu addItem:[NSMenuItem separatorItem]];
    [appMenu addItemWithTitle:[NSString stringWithFormat:@"Quit %@", appName] action:@selector(terminate:) keyEquivalent:@"q"];
    
    NSMenuItem* windowMenuItem = [bar addItemWithTitle:@"" action:NULL keyEquivalent:@""];
    [bar release];
    NSMenu* windowMenu = [[NSMenu alloc] initWithTitle:@"Window"];
    [NSApp setWindowsMenu:windowMenu];
    [windowMenuItem setSubmenu:windowMenu];
    
    [windowMenu addItemWithTitle:@"Minimize" action:@selector(performMiniaturize:) keyEquivalent:@"m"];
    [windowMenu addItemWithTitle:@"Zoom" action:@selector(performZoom:) keyEquivalent:@""];
    [windowMenu addItem:[NSMenuItem separatorItem]];
    [windowMenu addItemWithTitle:@"Bring All to Front" action:@selector(arrangeInFront:) keyEquivalent:@""];
    
    [windowMenu addItem:[NSMenuItem separatorItem]];
    [[windowMenu addItemWithTitle:@"Enter Full Screen" action:@selector(toggleFullScreen:) keyEquivalent:@"f"] setKeyEquivalentModifierMask:NSEventModifierFlagControl | NSEventModifierFlagCommand];
    
    SEL setAppleMenuSelector = NSSelectorFromString(@"setAppleMenu:");
    [NSApp performSelector:setAppleMenuSelector withObject:appMenu];
}

static bool initializeAppKit(void)
{
    if (NSApp)
        return true;
    
    [MyApplication sharedApplication];
    
    [NSThread detachNewThreadSelector:@selector(doNothing:) toTarget:NSApp withObject:nil];
    
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
    createMenuBar();
    
    g_state->delegate = [[MyApplicationDelegate alloc] init];
    [NSApp setDelegate:g_state->delegate];
    
    [NSApp run];
    
    return true;
}

void initOSXWindow(const char* title, int width, int height)
{
    g_state = calloc(1, sizeof(StateNS));
    g_state->autoreleasePool = [[NSAutoreleasePool alloc] init];
    
    if (!initializeAppKit())
    {
        return;
    }
    
    g_window = calloc(1, sizeof(WindowNS));
    g_window->delegate = [[MyWindowDelegate alloc] initWindow:g_window];
    NSRect contentRect = NSMakeRect(0, 0, width, height);
    NSUInteger styleMask = 0;
    styleMask |= NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable;
    g_window->object = [[MyWindow alloc] initWithContentRect:contentRect styleMask:styleMask backing:NSBackingStoreBuffered defer:NO];
    
    [g_window->object center];
    const NSWindowCollectionBehavior behavior = NSWindowCollectionBehaviorFullScreenPrimary | NSWindowCollectionBehaviorManaged;
    [g_window->object setCollectionBehavior:behavior];
    
    g_window->view = [[MyContentView alloc] initWithWindow:g_window];
    
    [g_window->view setWantsBestResolutionOpenGLSurface:YES];
    [g_window->object setContentView:g_window->view];
    [g_window->object makeFirstResponder:g_window->view];
    [g_window->object setTitle:[NSString stringWithUTF8String:title]];
    [g_window->object setDelegate:g_window->delegate];
    [g_window->object setAcceptsMouseMovedEvents:YES];
    [g_window->object setRestorable:NO];
    [g_window->object orderFront:nil];
    
    [NSApp activateIgnoringOtherApps:YES];
    [g_window->object makeKeyAndOrderFront:nil];
}

int closeOSXWindow()
{
    return g_window->shouldClose;
}

void setOSXReshapeFunction(reshape function)
{
    g_window->reshapeFunc = function;
}

int getOSXWindowWidth()
{
    const NSRect contentRect = [g_window->view frame];
    const NSRect fbRect = [g_window->view convertRectToBacking:contentRect];
    
    return fbRect.size.width;
}

int getOSXWindowHeight()
{
    const NSRect contentRect = [g_window->view frame];
    const NSRect fbRect = [g_window->view convertRectToBacking:contentRect];
    
    return fbRect.size.height;
}
