#pragma once

#include "../Common/Window.h"

class Window : public IWindow {
    
public:
    Window(const char* title, int width, int height);
    
    virtual void createWindow() override;
    virtual void* getNativeHandle() override;
    virtual inline const char* getTitle() override;
    virtual int getWidth() override;
    virtual int getHeight() override;
    virtual void showWindow() override;
    virtual void hideWindow() override;
    virtual void destroyWindow() override;
    
    virtual void setReshapeFunction(reshapeFunc function) override;
    
    virtual bool shouldClose() override;
    
private:
    
    void inline setWidth(int width) override;
    void inline setHeight(int height) override;
    
    const char* m_title;
    int m_width;
    int m_height;
};

const char* Window::getTitle()
{
    return m_title;
}

void Window::setWidth(int width)
{
    m_width = width;
}

void Window::setHeight(int height)
{
    m_height = height;
}
