//
//  Window_cocoa.h
//  ModernOpenGL
//
//  Created by Christopher Sierigk on 05.06.17.
//
//

#ifndef Window_cocoa_h
#define Window_cocoa_h

typedef void (*reshape)(int width, int height);

#if defined __cplusplus
extern "C" {
#endif
    
void initOSXWindow(const char* title, int width, int height);
int closeOSXWindow(void);
void setOSXReshapeFunction(reshape function);
    int getOSXWindowWidth(void);
    int getOSXWindowHeight(void);

#if defined __cplusplus
};
#endif
    
#endif /* Window_cocoa_h */
