#ifndef MAIN_CPP
#define MAIN_CPP

#include "../Common.h"
#include <vector>
#include <unistd.h>
#include <fstream>
#include <sys/time.h>

#include "../Collada/ColladaParser.h"
#include "../ObjParser.h"
#include "../osx/Window_osx.h"
#include "../osx/GL/OpenGLContext_osx.h"
#include "../Renderer/OpenGLRenderer.h"
#include "../Renderer/OpenGLShader.h"
#include "../Renderer/OpenGLTexture.h"

bool running = true;
OpenGLRenderer* openglRenderer;
Window* window;
OpenGLContext* context;

bool wasDPressed = false;

bool createWindow(const char* title, int width, int height)
{
    window = new Window(title, width, height);
    window->createWindow();
    
    return true;
}

bool createOpenGLContext()
{
    context = new OpenGLContext(window);
    return context->createOpenGLContext();
}

void FindAllModifiedFiles(const char* directory)
{

}

bool HasFileChanged(const char* filename)
{
    return false;
}

char* OpenTextFile(const char* filename)
{
    char* result = NULL;

    std::ifstream ifs(filename, std::ios::in | std::ios::binary);
    ifs.seekg(0, std::ios::end);
    long fileLength = ifs.tellg();

    result = new char[fileLength + 1];
    ifs.seekg(0, std::ios::beg);
    ifs.read(result, fileLength);
    ifs.close();

    result[fileLength] = 0;

    return result;
}

int main(int argc, char* argv[])
{
    createWindow("OpenGL 4.5 Project", 1024, 768);
    createOpenGLContext();
    openglRenderer = new OpenGLRenderer(context);
    openglRenderer->initRenderer();

    window->setReshapeFunction(&openglRenderer->reshapeWindow);
    
    FindAllModifiedFiles("../Assets/Shader/*.vert");
    FindAllModifiedFiles("../Assets/Shader/*.frag");

    char* vertexShader = OpenTextFile("../Assets/Shader/blinn_phong.vert");
    char* fragmentShader = OpenTextFile("../Assets/Shader/blinn_phong.frag");

    OpenGLShader shader;
    shader.createShaderProgram(vertexShader, fragmentShader);

    openglRenderer->setupScene();

    Float64 elapsedSeconds = 0.0;

    while (!window->shouldClose())
    {
        struct timeval t1, t2;
        gettimeofday(&t1, nullptr);
        
        openglRenderer->renderScene(shader, elapsedSeconds);
        
        gettimeofday(&t2, nullptr);

        elapsedSeconds = (t2.tv_sec - t1.tv_sec);
        elapsedSeconds += (t2.tv_usec - t1.tv_usec) / (1000.0 * 1000.0);
        
        //Log("FPS: %.2f", 1.0f / elapsedSeconds);
    }

    return 0;
}

#endif
