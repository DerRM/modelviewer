
#ifndef WindowNS_h
#define WindowNS_h

typedef void (*reshape)(int width, int height);

typedef struct StateNS
{
    id delegate;
    id autoreleasePool;
} StateNS;

typedef struct WindowNS
{
    id object;
    id delegate;
    id view;
    id layer;
    bool shouldClose;
    reshape reshapeFunc;
} WindowNS;

typedef struct NativeGL
{
    CFBundleRef framework;
} NativeGL;

typedef struct NativeGLContext
{
    id pixelFormat;
    id object;
} NativeGLContext;

extern StateNS* g_state;
extern WindowNS* g_window;
extern NativeGL* g_nativeGL;
extern NativeGLContext* g_context;

#endif
