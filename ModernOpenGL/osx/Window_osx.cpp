#include "../osx/Window_osx.h"
#include "../osx/Window_cocoa.h"

Window::Window(const char* title, int width, int height)
    : m_title(title),
      m_width(width),
      m_height(height)
{
}

void Window::createWindow()
{
    initOSXWindow(m_title, m_width, m_height);
}

void* Window::getNativeHandle()
{
    return nullptr;
}

void Window::showWindow()
{
    
}

void Window::hideWindow()
{
    
}

void Window::destroyWindow()
{
    
}

void Window::setReshapeFunction(reshapeFunc function)
{
    setOSXReshapeFunction(function);
}

bool Window::shouldClose()
{
    return closeOSXWindow() != 0;
}

int Window::getWidth()
{
    return getOSXWindowWidth();
}

int Window::getHeight()
{
    return getOSXWindowHeight();
}
