
#import <Cocoa/Cocoa.h>
#import "OpenGLContext_native.h"
#import "../WindowNS.h"

NativeGL* g_nativeGL = NULL;
NativeGLContext* g_context = NULL;

int createNativeOpenGLContext()
{
    g_nativeGL = calloc(1, sizeof(NativeGL));
    g_nativeGL->framework = CFBundleGetBundleWithIdentifier(CFSTR("com.apple.opengl"));
    
    unsigned int attributeCount = 0;
    NSOpenGLPixelFormatAttribute attributes[40];
    
    attributes[attributeCount++] = NSOpenGLPFAAccelerated;
    attributes[attributeCount++] = NSOpenGLPFAClosestPolicy;
    
    attributes[attributeCount++] = NSOpenGLPFAOpenGLProfile;
    attributes[attributeCount++] = NSOpenGLProfileVersion4_1Core;
    
    attributes[attributeCount++] = NSOpenGLPFAColorSize;
    attributes[attributeCount++] = 24;
    
    attributes[attributeCount++] = NSOpenGLPFAAlphaSize;
    attributes[attributeCount++] = 8;
    
    attributes[attributeCount++] = NSOpenGLPFADepthSize;
    attributes[attributeCount++] = 24;
    
    attributes[attributeCount++] = NSOpenGLPFAStencilSize;
    attributes[attributeCount++] = 8;
    
    attributes[attributeCount++] = NSOpenGLPFADoubleBuffer;
    
    attributes[attributeCount++] = NSOpenGLPFASampleBuffers;
    attributes[attributeCount++] = 0;
    
    attributes[attributeCount++] = 0;
    
    g_context = calloc(1, sizeof(NativeGLContext));
    g_context->pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
    g_context->object = [[NSOpenGLContext alloc] initWithFormat:g_context->pixelFormat shareContext:NULL];
    [g_context->object setView:g_window->view];
    
    [g_context->object makeCurrentContext];
        
    return 0;
}

void destroyNativeOpenGLContext()
{
    
}

void swapNativeBuffers()
{
    [g_context->object flushBuffer];
    
    for (;;)
    {
        NSEvent* event = [NSApp nextEventMatchingMask:NSEventMaskAny
                                            untilDate:[NSDate distantPast]
                                               inMode:NSDefaultRunLoopMode
                                              dequeue:YES];
        
        if (event == nil)
            break;
        
        [NSApp sendEvent:event];
    }
    
    [g_state->autoreleasePool drain];
    g_state->autoreleasePool = [[NSAutoreleasePool alloc] init];
}
