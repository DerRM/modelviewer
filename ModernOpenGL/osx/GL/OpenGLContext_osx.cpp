#include "../../osx/GL/OpenGLContext_osx.h"
#include "../../osx/GL/OpenGLContext_native.h"
#include "../../Renderer/OpenGL45.h"

OpenGLContext::OpenGLContext(IWindow* window)
    : m_window(window)
{
    
}

bool OpenGLContext::createOpenGLContext()
{
    return createNativeOpenGLContext() == 0;
}

void OpenGLContext::destroyOpenGLContext()
{
    
}

void OpenGLContext::swapBuffers()
{
    swapNativeBuffers();
}
