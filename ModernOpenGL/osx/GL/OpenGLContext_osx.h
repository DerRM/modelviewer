#pragma once

#include "../../Renderer/OpenGLContext.h"
#include "../../osx/Window_osx.h"

class OpenGLContext : public IOpenGLContext {
    
public:
    OpenGLContext(IWindow* window);
    
    virtual bool createOpenGLContext() override;
    virtual void destroyOpenGLContext() override;
    virtual void swapBuffers() override;
    virtual inline IWindow* getWindow() override;
    
private:
    IWindow* m_window;
};

IWindow* OpenGLContext::getWindow()
{
    return m_window;
}
