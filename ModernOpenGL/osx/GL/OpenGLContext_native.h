
#ifndef OpenGLContext_native_h
#define OpenGLContext_native_h

#include "../Window_cocoa.h"

struct WindowNS;

#ifdef __cplusplus
extern "C" {
#endif

    int createNativeOpenGLContext(void);
    void destroyNativeOpenGLContext(void);
    void swapNativeBuffers(void);
    
#ifdef __cplusplus
};
#endif

#endif
