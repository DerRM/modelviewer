#pragma once

#include <vector>
#include <string>
#include <tinyxml.h>

#include "../Common.h"

#include "ColladaDataTypes.h"

typedef std::vector<Geometry> GeometryList;

class ColladaGeometriesLibrary
{
public:
    ColladaGeometriesLibrary();
    ~ColladaGeometriesLibrary();

    void readLibraryGeometry(TiXmlElement* pNode);
    void readGeometry(TiXmlElement* pNode, Geometry& geometry);
    void readMesh(TiXmlElement* pNode, Mesh& mesh);
    void readPrimitiveElements(TiXmlElement* pNode, PrimitiveElements& primitiveElement);
    void readExtra(TiXmlElement* pNode, Extra& extra);
    void readSource(TiXmlElement* pNode, Source& source);
    void readPrimitive(TiXmlElement* pNode, Primitive& primitive);
    void readVertices(TiXmlElement* pNode, Vertices& vertices);

    GeometryList& getGeometryList() { return m_geometryList; }

private:
    GeometryList m_geometryList;
};

