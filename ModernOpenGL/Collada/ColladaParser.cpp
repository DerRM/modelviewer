#include "ColladaParser.h"

static std::unordered_map<Uint64, chs::Matrix4x4> jointMap;

ColladaParser::ColladaParser()
{
}


ColladaParser::~ColladaParser()
{
}

Bool32 ColladaParser::openFile(const char* pFilename)
{
#ifdef __ANDROID__
    AAssetManager* assetManager = nativeActivity->assetManager;
    AAsset* asset = AAssetManager_open(assetManager, pFilename, AASSET_MODE_UNKNOWN);
    size_t size = (size_t)AAsset_getLength(asset);
    char* buffer = (char*)malloc(size + 1);
    AAsset_read(asset, buffer, size);
    buffer[size] = '\0';

    m_pDocument = new TiXmlDocument();
    return (Bool32)(m_pDocument->Parse((const char*)buffer, 0, TIXML_ENCODING_UTF8) != 0);
#else
    m_pDocument = new TiXmlDocument(pFilename);
    return m_pDocument->LoadFile();
#endif
}

void ColladaParser::readFile()
{
    if (!m_pDocument) return;

    readNode(m_pDocument);
}

std::vector<CMesh*>& ColladaParser::parseMesh()
{
    GeometryList& geometryList = m_geometryLibrary.getGeometryList();

    for (size_t geometryIndex = 0; geometryIndex < geometryList.size(); ++geometryIndex)
    {
        CMesh* mesh = new CMesh();

        Geometry& geometry = geometryList[geometryIndex];

        Uint32 indexOffset = 0;
        for (size_t trianglesIndex = 0; trianglesIndex < geometry.mesh.primitiveElementList.size(); ++trianglesIndex)
        {
            uint32_t index = 0;

            SSubMesh* submesh = new SSubMesh();
            submesh->indexBufferOffset = indexOffset;
            mesh->addSubMesh(submesh);

            Triangles& triangles = geometry.mesh.primitiveElementList[trianglesIndex];
            IndexArray& pList = triangles.primitive.indexList;

            size_t inputListSize = triangles.inputList.size();

            Uint32 vertexOffset = UINT32_MAX;
            Uint32 normalOffset = UINT32_MAX;
            Uint32 texCoordOffset = UINT32_MAX;

            Position* pPosition = nullptr;
            Normal* pNormal = nullptr;
            chs::Vector3* pTexCoord = nullptr;

            for (size_t inputIndex = 0; inputIndex < triangles.inputList.size(); ++inputIndex)
            {
                InputShared& input = triangles.inputList[inputIndex];

                if (input.semantic == "VERTEX")
                {
                    vertexOffset = input.offset;

                    Vertices& vertices = geometry.mesh.vertices;

                    for (size_t vertexIndex = 0; vertexIndex < vertices.inputList.size(); ++vertexIndex)
                    {
                        Source& source = geometry.mesh.sourceList[&vertices.inputList[vertexIndex].source[1]];
                        pPosition = reinterpret_cast<Position*>(source.arrayElement.arrayList);
                    }
                }
                else if (input.semantic == "NORMAL")
                {
                    normalOffset = input.offset;
                    Source& source = geometry.mesh.sourceList[&input.source[1]];
                    pNormal = reinterpret_cast<Normal*>(source.arrayElement.arrayList);
                }
                else if (input.semantic == "TEXCOORD")
                {
                    texCoordOffset = input.offset;
                    Source& source = geometry.mesh.sourceList[&input.source[1]];
                    pTexCoord = reinterpret_cast<chs::Vector3*>(source.arrayElement.arrayList);
                }
            }

            for (size_t pListIndex = 0; pListIndex < pList.size(); ++pListIndex)
            {
                Uint32 offset = static_cast<Uint32>(pListIndex % inputListSize);

                if (offset == vertexOffset)
                {
                    submesh->positions.push_back(pPosition[pList[pListIndex]]);
                }
                else if (offset == normalOffset)
                {
                    submesh->normals.push_back(pNormal[pList[pListIndex]]);
                }
                else if (offset == texCoordOffset)
                {
                    submesh->uvs.push_back(*reinterpret_cast<Vector2*>(&pTexCoord[pList[pListIndex]]));
                }

                if (offset == 0)
                {
                    mesh->getIndexBuffer().push_back(index++);
                }
            }

            indexOffset = static_cast<Uint32>(mesh->getIndexBuffer().size());
        }

        m_meshes.push_back(mesh);
    }

    return m_meshes;
}

void ColladaParser::makeOpenGlReady()
{
    GeometryList& geometryList = m_geometryLibrary.getGeometryList();

    Uint32 vertexOffset = UINT32_MAX;
    Uint32 normalOffset = UINT32_MAX;
    Uint32 texCoordOffset = UINT32_MAX;

    for (size_t geometryIndex = 0; geometryIndex < geometryList.size(); ++geometryIndex)
    {
        Geometry& geom = geometryList[geometryIndex];
        uint32_t index = 0;

        for (size_t trianglesIndex = 0; trianglesIndex < geom.mesh.primitiveElementList.size(); ++trianglesIndex)
        {
            Triangles& triangles = geom.mesh.primitiveElementList[trianglesIndex];
            auto& pList = triangles.primitive.indexList;

            size_t inputListSize = triangles.inputList.size();

            Position* pPosition = nullptr;
            Normal* pNormal = nullptr;
            chs::Vector3* pTexCoord = nullptr;

            for (size_t inputIndex = 0; inputIndex < inputListSize; ++inputIndex)
            {
                auto& inputList = triangles.inputList[inputIndex];

                if (inputList.semantic == "VERTEX")
                {
                    vertexOffset = inputList.offset;

                    Vertices& vertices = geom.mesh.vertices;

                    for (size_t verticesIndex = 0; verticesIndex < vertices.inputList.size(); ++verticesIndex)
                    {
                        Source& source = geom.mesh.sourceList[&vertices.inputList[verticesIndex].source[1]];

                        pPosition = reinterpret_cast<Position*>(source.arrayElement.arrayList);
                    }
                }
                else if (inputList.semantic == "NORMAL")
                {
                    normalOffset = inputList.offset;

                    Source& source = geom.mesh.sourceList[&inputList.source[1]];

                    pNormal = reinterpret_cast<Normal*>(source.arrayElement.arrayList);
                }
                else if (inputList.semantic == "TEXCOORD")
                {
                    texCoordOffset = inputList.offset;

                    Source& source = geom.mesh.sourceList[&inputList.source[1]];

                    pTexCoord = reinterpret_cast<chs::Vector3*>(source.arrayElement.arrayList);
                }
            }

            for (size_t pListIndex = 0; pListIndex < pList.size(); ++pListIndex)
            {
                Uint32 offset = static_cast<Uint32>(pListIndex % inputListSize);

                if (offset == vertexOffset)
                {
					Position pos = pPosition[pList[pListIndex]];
					//chs::Vector4 pos_v4 = chs::Vector4(pos.x, pos.y, pos.z, 1.0);
					//BoneIndex boneIndex = m_tempBoneIndexArray[pList[pListIndex]];

					//chs::Matrix4x4& inverseBindPose = jointMap[boneIndex.x];
					//chs::Vector4 tranPos = inverseBindPose * pos_v4;

                    m_positionArray.push_back(pos);
                    m_vertexWeightArray.push_back(m_tempVertexWeightArray[pList[pListIndex]]);

					m_boneWeightArray.push_back(m_tempBoneWeightArray[pList[pListIndex]]);
					m_boneIndexArray.push_back(m_tempBoneIndexArray[pList[pListIndex]]);

                    m_skinningArray.push_back(pPosition[pList[pListIndex]]);
                }
                else if (offset == normalOffset)
                {
                    m_normalArray.push_back(pNormal[pList[pListIndex]]);
                }
                else if (offset == texCoordOffset)
                {
                    m_texCoordArray.push_back(*reinterpret_cast<Vector2*>(&pTexCoord[pList[pListIndex]]));
                }

                if (offset == 0)
                {
                    m_indexArray.push_back(index++);
                }
            }
        }
    }
}

struct AnimationStruct
{
    std::vector<Float32> timerList;
    std::vector<chs::Matrix4x4> matrixList;
};

static Float32 maxTime = 0.0f;
static Uint32 keyFrameCount = 0;
static std::unordered_map<std::string, AnimationStruct> animationMap;

static std::unordered_map<Uint64, chs::Matrix4x4> skeletonMap;



void ColladaParser::prepareSkeleton()
{
    VisualSceneList& visualSceneList = m_visualScenesLibrary.getVisualSceneList();

    VisualScene& visualScene = visualSceneList[0];

    for (size_t sceneNodeIndex = 0; sceneNodeIndex < visualScene.nodeList.size(); ++sceneNodeIndex)
    {
        if (visualScene.nodeList[sceneNodeIndex].id == "astroBoy_newSkeleton_deformation_rig")
        {
            std::stack<Node> nodeStack;
            std::stack<NodeStruct*> nodeStructStack;

            Node& node = visualScene.nodeList[sceneNodeIndex];

            chs::Vector4 vec(0.0f, 0.0f, 0.0f, 1.0f);
            chs::Matrix4x4& matrix = node.matrixList[0];
            
            nodeStack.push(node);

			m_skeleton.nodeRoot = new NodeStruct();
			m_skeleton.nodeRoot->parent = nullptr;
			m_skeleton.nodeRoot->worldMatrix = matrix;

            nodeStructStack.push(m_skeleton.nodeRoot);

            while (!nodeStack.empty())
            {
                Node parentNode = nodeStack.top();
                nodeStack.pop();

                NodeStruct* parentNodeStruct = nodeStructStack.top();
                nodeStructStack.pop();

                chs::Matrix4x4& parentMatrix = parentNodeStruct->worldMatrix;
                chs::Vector4 parentPosition = parentMatrix * vec;
                
                if (parentNode.type == "JOINT")
                {
                    parentNodeStruct->type = parentNode.type;
                    parentNodeStruct->sid = parentNode.sid;
                    skeletonMap[parentNode.sid] = parentMatrix;
                }

                size_t size = parentNode.nodeList.size();
                for (size_t nodeIndex = 0; nodeIndex < size; nodeIndex++)
                {
                    Node childNode = parentNode.nodeList[(size - 1) - nodeIndex];
                    nodeStack.push(childNode);

                    chs::Matrix4x4 childMatrix = (parentMatrix * childNode.matrixList[0]);
                    chs::Vector4 childPosition = childMatrix * vec;

                    NodeStruct* nodeChild = new NodeStruct();
                    nodeChild->worldMatrix = childMatrix;
                    nodeChild->boneMatrix = childNode.matrixList[0];
                    nodeChild->parent = parentNodeStruct;
                    nodeChild->id = childNode.id;
                    nodeStructStack.push(nodeChild);

                    if (parentNode.type == "JOINT")
                    {
                        m_lines.push_back(childPosition);
                        m_lines.push_back(parentPosition);
                    }
                    
                    parentNodeStruct->childList.push_back(nodeChild);
                }
            }

            break;
        }
    }
}

void ColladaParser::prepareSkeletonFrames(Uint32 frameIndex)
{
    Uint32 usedFrameIndex = frameIndex % keyFrameCount;

	VisualSceneList& visualSceneList = m_visualScenesLibrary.getVisualSceneList();

	VisualScene& visualScene = visualSceneList[0];

	for (size_t sceneNodeIndex = 0; sceneNodeIndex < visualScene.nodeList.size(); ++sceneNodeIndex)
	{
		if (visualScene.nodeList[sceneNodeIndex].id == "astroBoy_newSkeleton_deformation_rig")
		{
			std::stack<Node> nodeStack;
			std::stack<NodeStruct*> nodeStructStack;

			Node& node = visualScene.nodeList[sceneNodeIndex];

			chs::Vector4 vec(0.0f, 0.0f, 0.0f, 1.0f);

			auto got = animationMap.find(node.id);

			chs::Matrix4x4& matrix = got == animationMap.end() ? node.matrixList[0] : animationMap[node.id].matrixList[usedFrameIndex];

			nodeStack.push(node);

			SkeletonStruct skeleton;

			skeleton.nodeRoot = new NodeStruct();
			skeleton.nodeRoot->parent = nullptr;
			skeleton.nodeRoot->worldMatrix = matrix;

			nodeStructStack.push(skeleton.nodeRoot);

			while (!nodeStack.empty())
			{
				Node parentNode = nodeStack.top();
				nodeStack.pop();

				NodeStruct* parentNodeStruct = nodeStructStack.top();
				nodeStructStack.pop();

				chs::Matrix4x4& parentMatrix = parentNodeStruct->worldMatrix;

				if (parentNode.type == "JOINT")
				{
					parentNodeStruct->type = parentNode.type;
					parentNodeStruct->sid = parentNode.sid;
					m_skeletonMaps[usedFrameIndex][parentNode.sid] = parentMatrix;
				}

				size_t size = parentNode.nodeList.size();
				for (size_t nodeIndex = 0; nodeIndex < size; nodeIndex++)
				{
					Node childNode = parentNode.nodeList[(size - 1) - nodeIndex];
					nodeStack.push(childNode);

					auto gotNode = animationMap.find(childNode.id);

					chs::Matrix4x4 childMatrix = (parentMatrix * (gotNode == animationMap.end() ? childNode.matrixList[0] : animationMap[childNode.id].matrixList[usedFrameIndex]) );

					NodeStruct* nodeChild = new NodeStruct();
					nodeChild->worldMatrix = childMatrix;
					nodeChild->boneMatrix = gotNode == animationMap.end() ? childNode.matrixList[0] : animationMap[childNode.id].matrixList[usedFrameIndex];
					nodeChild->parent = parentNodeStruct;
					nodeChild->id = childNode.id;
					nodeStructStack.push(nodeChild);

					parentNodeStruct->childList.push_back(nodeChild);
				}
			}

			break;
		}
	}

	ControllerList& controllerList = m_controllersLibrary.getControllerList();

	for (size_t controllerIndex = 0; controllerIndex < controllerList.size(); ++controllerIndex)
	{
		Controller& controller = controllerList[controllerIndex];
		Skin& skin = controller.skin;
		Joints& joints = skin.joints;

		Source* boneSource = nullptr;
		Source* bindMatrixSource = nullptr;

		for (size_t inputIndex = 0; inputIndex < joints.inputList.size(); ++inputIndex)
		{
			InputUnshared& input = joints.inputList[inputIndex];

			if (input.semantic == "JOINT")
			{
				for (size_t sourceIndex = 0; sourceIndex < skin.sourceList.size(); ++sourceIndex)
				{
					std::string link = "#";
					link += skin.sourceList[sourceIndex].id;
					if (input.source == link)
					{
						boneSource = &skin.sourceList[sourceIndex];
					}
				}
			}
			else if (input.semantic == "INV_BIND_MATRIX")
			{
				for (size_t sourceIndex = 0; sourceIndex < skin.sourceList.size(); ++sourceIndex)
				{
					std::string link = "#";
					link += skin.sourceList[sourceIndex].id;
					if (input.source == link)
					{
						bindMatrixSource = &skin.sourceList[sourceIndex];
					}
				}
			}
		}

		if (boneSource && bindMatrixSource)
		{
			Uint32 boneCount = boneSource->arrayElement.count;

            Uint64* boneArray = reinterpret_cast<Uint64*>(boneSource->arrayElement.arrayList);

            Float32* matrixArray = reinterpret_cast<Float32*>(bindMatrixSource->arrayElement.arrayList);

			for (size_t boneIndex = 0; boneIndex < boneCount; ++boneIndex)
			{
				Float32* matrixPointer = &matrixArray[boneIndex * 16];

				Float32 matrix[16];

				for (size_t matrixIndex = 0; matrixIndex < 16; matrixIndex++)
				{
					matrix[matrixIndex] = matrixPointer[((matrixIndex % 4) * 4) + (matrixIndex >> 2)];
				}
				
				chs::Matrix4x4 skeletonMatrix = m_skeletonMaps[usedFrameIndex][boneArray[boneIndex]] * chs::Matrix4x4(matrix);
				m_boneLists[usedFrameIndex].push_back(skeletonMatrix);
			}
		}

	}
}

static Float32 runTime = 0.0f;

chs::Matrix4x4 MatrixLerp(const chs::Matrix4x4 m1, const chs::Matrix4x4 m2, Float32 t)
{
    chs::Matrix4x4 matrix;

    matrix[0] = (1.0f - t) * m1[0] + t * m2[0];
    matrix[1] = (1.0f - t) * m1[1] + t * m2[1];
    matrix[2] = (1.0f - t) * m1[2] + t * m2[2];
    matrix[3] = (1.0f - t) * m1[3] + t * m2[3];
    matrix[4] = (1.0f - t) * m1[4] + t * m2[4];
    matrix[5] = (1.0f - t) * m1[5] + t * m2[5];
    matrix[6] = (1.0f - t) * m1[6] + t * m2[6];
    matrix[7] = (1.0f - t) * m1[7] + t * m2[7];
    matrix[8] = (1.0f - t) * m1[8] + t * m2[8];
    matrix[9] = (1.0f - t) * m1[9] + t * m2[9];
    matrix[10] = (1.0f - t) * m1[10] + t * m2[10];
    matrix[11] = (1.0f - t) * m1[11] + t * m2[11];
    matrix[12] = (1.0f - t) * m1[12] + t * m2[12];
    matrix[13] = (1.0f - t) * m1[13] + t * m2[13];
    matrix[14] = (1.0f - t) * m1[14] + t * m2[14];
    matrix[15] = (1.0f - t) * m1[15] + t * m2[15];

    return matrix;
}

void ColladaParser::updateSkeleton(Float64 frameTime)
{
	Uint32 noOfKeyFrames = keyFrameCount - 1;

    Uint32 animationIndex = (static_cast<Uint32>((runTime / maxTime) * (noOfKeyFrames))) % noOfKeyFrames;
    Float32 dt = ((runTime / maxTime) * noOfKeyFrames) - static_cast<Uint32>((runTime / maxTime) * noOfKeyFrames);

	for (size_t boneIndex = 0; boneIndex < m_bones.size(); ++boneIndex)
	{
		m_bones[boneIndex] = MatrixLerp(m_boneLists[animationIndex][boneIndex], m_boneLists[(animationIndex + 1) % noOfKeyFrames][boneIndex], dt);
		//m_bones[boneIndex] = m_boneLists[animationIndex][boneIndex];
	}

	//Log("animationIndex: %u dt: %f", animationIndex, dt);

    runTime += static_cast<Float32>(frameTime);

	if (runTime > maxTime)
	{
		runTime = runTime - maxTime;
	}
}

void ColladaParser::prepareSkeletonForOGL()
{
    //glGenVertexArrays(1, &m_array);
    //glBindVertexArray(m_array);

    glGenBuffers(1, &m_buffer);

    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(chs::Vector4) * static_cast<GLuint>(m_lines.size()), &m_lines[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

    //glBindVertexArray(0);
}

void ColladaParser::prepareSkinning()
{
    ControllerList& controllerList = m_controllersLibrary.getControllerList();

    for (size_t controllerIndex = 0; controllerIndex < controllerList.size(); controllerIndex++)
    {
        Controller& controller = controllerList[controllerIndex];

        Skin& skin = controller.skin;

        Joints& joints = skin.joints;

        Source* boneSource = nullptr;
        Source* bindMatrixSource = nullptr;

        for (size_t inputIndex = 0; inputIndex < joints.inputList.size(); inputIndex++)
        {
            InputUnshared& input = joints.inputList[inputIndex];

            if (input.semantic == "JOINT")
            {
                for (size_t sourceIndex = 0; sourceIndex < skin.sourceList.size(); ++sourceIndex)
                {
                    std::string link = "#";
                    link += skin.sourceList[sourceIndex].id;
                    if (input.source == link)
                    {
                        boneSource = &skin.sourceList[sourceIndex];
                    }
                }
            }
            else if (input.semantic == "INV_BIND_MATRIX")
            {
                for (size_t sourceIndex = 0; sourceIndex < skin.sourceList.size(); ++sourceIndex)
                {
                    std::string link = "#";
                    link += skin.sourceList[sourceIndex].id;
                    if (input.source == link)
                    {
                        bindMatrixSource = &skin.sourceList[sourceIndex];
                    }
                }
            }
        }

        if (boneSource && bindMatrixSource)
        {
            Uint32 boneCount = boneSource->arrayElement.count;

            Uint64* boneArray = reinterpret_cast<Uint64*>(boneSource->arrayElement.arrayList);

            Float32* matrixArray = reinterpret_cast<Float32*>(bindMatrixSource->arrayElement.arrayList);

            for (size_t boneIndex = 0; boneIndex < boneCount; ++boneIndex)
            {
                Float32* matrixPointer = &matrixArray[boneIndex * 16];

                Float32 matrix[16];

                for (size_t matrixIndex = 0; matrixIndex < 16; matrixIndex++)
                {
                    matrix[matrixIndex] = matrixPointer[((matrixIndex % 4) * 4) + (matrixIndex >> 2)];
                }

				jointMap[boneArray[boneIndex]] = chs::Matrix4x4(matrix);

				chs::Matrix4x4 skeletonMatrix = skeletonMap[boneArray[boneIndex]] * jointMap[boneArray[boneIndex]];

				m_bones.push_back(skeletonMatrix);
            }
        }

        VertexWeights& vertexWeights = skin.vertexWeights;

        boneSource = nullptr;
        Source* weightSource = nullptr;

        for (size_t inputIndex = 0; inputIndex < vertexWeights.inputList.size(); inputIndex++)
        {
            InputShared& input = vertexWeights.inputList[inputIndex];

            if (input.semantic == "JOINT")
            {
                for (size_t sourceIndex = 0; sourceIndex < skin.sourceList.size(); sourceIndex++)
                {
                    std::string link = "#";
                    link += skin.sourceList[sourceIndex].id;
                    if (input.source == link)
                    {
                        boneSource = &skin.sourceList[sourceIndex];
                    }
                }
            }
            else if (input.semantic == "WEIGHT")
            {
                for (size_t sourceIndex = 0; sourceIndex < skin.sourceList.size(); sourceIndex++)
                {
                    std::string link = "#";
                    link += skin.sourceList[sourceIndex].id;
                    if (input.source == link)
                    {
                        weightSource = &skin.sourceList[sourceIndex];
                    }
                }
            }
        }

        if (boneSource && weightSource)
        {
            Uint64* boneArray = reinterpret_cast<Uint64*>(boneSource->arrayElement.arrayList);

            Float32* weightArray = reinterpret_cast<Float32*>(weightSource->arrayElement.arrayList);

            Uint32 vPos = 0;

            for (size_t vCountIndex = 0; vCountIndex < vertexWeights.vCount.size(); ++vCountIndex)
            {
                VertexWeight vertexWeight = {};

                Uint32 length = vertexWeights.vCount[vCountIndex] * 2;

				size_t elementIndex = 0;

				BoneIndex boneIndex;
				BoneWeight boneWeight;

                for (size_t vIndex = vPos; vIndex < (vPos + length); vIndex += 2, ++elementIndex)
                {
                    vertexWeight.bones.push_back(boneArray[vertexWeights.v[vIndex]]);
                    vertexWeight.weights.push_back(weightArray[vertexWeights.v[vIndex + 1]]);

					boneIndex.data[elementIndex] = vertexWeights.v[vIndex];
					boneWeight.data[elementIndex] = weightArray[vertexWeights.v[vIndex + 1]];
                }

				m_tempBoneIndexArray.push_back(boneIndex);
				m_tempBoneWeightArray.push_back(boneWeight);

                vPos += length;

                m_tempVertexWeightArray.push_back(vertexWeight);
            }
        }
    }
}

void ColladaParser::applySkinning()
{
    for (size_t vertexIndex = 0; vertexIndex < m_vertexWeightArray.size(); ++vertexIndex)
    {
        chs::Vector3 pos = m_positionArray[vertexIndex];
        chs::Vector4 v(pos.x, pos.y, pos.z, 1.0f);

        VertexWeight& vertexWeight = m_vertexWeightArray[vertexIndex];

        chs::Vector4 tempVec;

        Float32 totalWeight = 0.0f;

		bool visited = false;

        for (size_t vertexWeightIndex = 0; vertexWeightIndex < vertexWeight.bones.size(); vertexWeightIndex++)
        {
            chs::Matrix4x4& jointWorldSpace = skeletonMap[vertexWeight.bones[vertexWeightIndex]];
            chs::Matrix4x4& inverseBindPose = jointMap[vertexWeight.bones[vertexWeightIndex]];

            tempVec += (jointWorldSpace * (inverseBindPose * v)) * vertexWeight.weights[vertexWeightIndex];
            totalWeight += vertexWeight.weights[vertexWeightIndex];
			
			if (!visited) {
				chs::Vector4 tmp = (inverseBindPose * v);
				m_positionArray[vertexIndex] = chs::Vector3(tmp.x, tmp.y, tmp.z);
				visited = true;
			}
        }

        tempVec /= totalWeight;

        m_skinningArray[vertexIndex] = chs::Vector3(tempVec.x, tempVec.y, tempVec.z);
    }
}

void ColladaParser::prepareAnimation()
{
    AnimationList& animationList = m_animationLibrary.getAnimationList();

    for (size_t animationIndex = 0; animationIndex < animationList.size(); animationIndex++)
    {
        Animation& animation = animationList[animationIndex];

        AnimationStruct animationStruct = {};

        bool isMatrixListInitialized = false;

        if (animation.samplerList.size() == 16)
        {
            for (size_t channelIndex = 0; channelIndex < animation.channelList.size(); channelIndex++)
            {
                Channel& channel = animation.channelList[channelIndex];
                Sampler& sampler = animation.samplerList[&channel.source[1]];

                Source* inputSource = nullptr;
                Source* outputSource = nullptr;

                for (size_t inputIndex = 0; inputIndex < sampler.inputList.size(); inputIndex++)
                {
                    InputUnshared& input = sampler.inputList[inputIndex];

                    if (input.semantic == "INPUT")
                    {
                        inputSource = &animation.sourceList[&input.source[1]];
                    }

                    if (input.semantic == "OUTPUT")
                    {
                        outputSource = &animation.sourceList[&input.source[1]];
                    }

                    //if (input.semantic == "INTERPOLATION")
                    //{
                    //}
                }

                if (!isMatrixListInitialized)
                {
                    keyFrameCount = outputSource->arrayElement.count;
                    for (size_t outputIndex = 0; outputIndex < keyFrameCount; outputIndex++)
                    {
                        animationStruct.matrixList.push_back(chs::Matrix4x4::zero());
                        Float32 timerValue = (reinterpret_cast<Float32*>(inputSource->arrayElement.arrayList))[outputIndex];

                        if (timerValue > maxTime)
                        {
                            maxTime = timerValue;
                        }

                        animationStruct.timerList.push_back(timerValue);
                    }

                    isMatrixListInitialized = true;
                }

                if (outputSource->arrayElement.count == 2)
                {
                    Float32 startValue = (reinterpret_cast<Float32*>(outputSource->arrayElement.arrayList))[0];
                    Float32 endValue = (reinterpret_cast<Float32*>(outputSource->arrayElement.arrayList))[1];

                    for (size_t matrixIndex = 0; matrixIndex < animationStruct.matrixList.size(); matrixIndex++)
                    {
                        Float32 t = static_cast<Float32>(matrixIndex) / static_cast<Float32>(animationStruct.matrixList.size());

                        Float32 value = (1.0f - t) * startValue + t * endValue;

                        if (channelIndex == 15)
                        {
                            animationStruct.matrixList[matrixIndex][15] = 1.0f;
                        }
                        else
                        {
                            animationStruct.matrixList[matrixIndex][static_cast<Int32>((channelIndex % 4) * 4 + (channelIndex >> 2))] = value;
                        }
                    }
                }
                else
                {
                    for (size_t inputIndex = 0; inputIndex < inputSource->arrayElement.count; inputIndex++)
                    {
                        if (channelIndex == 15)
                        {
                            animationStruct.matrixList[inputIndex][15] = 1.0f;
                        }
                        else
                        {
                            animationStruct.matrixList[inputIndex][static_cast<Int32>((channelIndex % 4) * 4 + (channelIndex >> 2))] =
                                (reinterpret_cast<Float32*>(outputSource->arrayElement.arrayList))[inputIndex];
                        }
                    }
                    
                }
                
                if (channelIndex == 0) { assert(inputSource->arrayElement.count == 36); }
                assert(inputSource->arrayElement.count == 36 || inputSource->arrayElement.count == 2);
            }
        }

        Uint32 firstIndex = static_cast<Uint32>(animation.channelList[0].target.find_first_of("/"));
        std::string key = animation.channelList[0].target.substr(0, firstIndex);
        animationMap[key] = animationStruct;
    }

	for (size_t keyframeIndex = 0; keyframeIndex < keyFrameCount; ++keyframeIndex)
	{
		SkeletonMap skeletonMap;
		m_skeletonMaps.push_back(skeletonMap);
		m_boneLists.push_back(std::vector<chs::Matrix4x4>());
        prepareSkeletonFrames(static_cast<Uint32>(keyframeIndex));
	}
}

void ColladaParser::renderSkeleton()
{
    //prepareSkeleton();

    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
#ifndef __EMSCRIPTEN__
    //void* map = glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);

    //memcpy(map, &m_lines[0], m_lines.size() * sizeof(chs::Vector4));

    //glUnmapBufferOES(GL_ARRAY_BUFFER);
#endif
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //glBindVertexArray(m_array);
    glLineWidth(2.0f);
    glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(m_lines.size()));
    glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(m_lines.size()));
    //glBindVertexArray(0);
}

void ColladaParser::readNode(TiXmlNode* pNode)
{
    int type = pNode->Type();

    switch (type)
    {
    case TiXmlNode::TINYXML_DOCUMENT:
        //WinLog("Document\n");
        break;
    case TiXmlNode::TINYXML_TEXT:
        break;

    case TiXmlNode::TINYXML_ELEMENT:
    {
        //WinLog("Element : %s\n", pNode->Value());

        TiXmlElement* pElement = pNode->ToElement();

        std::string elementName = pElement->Value();

        if (elementName == "library_geometries")
        {
            m_geometryLibrary.readLibraryGeometry(pElement);
        }
        else if (elementName == "library_animations")
        {
            m_animationLibrary.readLibraryAnimation(pElement);
        }
        else if (elementName == "library_visual_scenes")
        {
            m_visualScenesLibrary.readLibraryVisualScenes(pElement);
        }
        else if (elementName == "library_controllers")
        {
            m_controllersLibrary.readLibraryControllers(pElement);
        }
    }
    break;

    default:
        break;
    }

    TiXmlNode* pChild = nullptr;

    for (pChild = pNode->FirstChild(); pChild != nullptr; pChild = pChild->NextSibling())
    {
        readNode(pChild);
    }
}

void ColladaParser::closeFile()
{
    delete m_pDocument;
}
