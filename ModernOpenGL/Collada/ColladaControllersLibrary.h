#pragma once

#include <string>
#include <vector>
#include <tinyxml.h>

#include "../Common.h"
#include "ColladaDataTypes.h"

typedef std::vector<Controller> ControllerList;

class ColladaControllersLibrary
{
public:
    ColladaControllersLibrary();
    ~ColladaControllersLibrary();

    void readLibraryControllers(TiXmlElement* pNode);
    void readController(TiXmlElement* pNode, Controller& controller);
    void readSkin(TiXmlElement* pNode, Skin& skin);
    void readSource(TiXmlElement* pNode, Source& source);
    void readJoints(TiXmlElement* pNode, Joints& joints);
    void readVertexWeights(TiXmlElement* pNode, VertexWeights& vertexWeights);

    ControllerList& getControllerList() { return m_controllerList; }

private:
    ControllerList m_controllerList;
};

