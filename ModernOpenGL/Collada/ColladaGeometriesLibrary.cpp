#include "ColladaGeometriesLibrary.h"


ColladaGeometriesLibrary::ColladaGeometriesLibrary()
{
}


ColladaGeometriesLibrary::~ColladaGeometriesLibrary()
{
}

void ColladaGeometriesLibrary::readLibraryGeometry(TiXmlElement* pNode)
{
    TiXmlElement* pChild = pNode->FirstChildElement();

    std::string elementName = pChild->Value();

    if (elementName == "geometry")
    {
        Geometry geometry = {};

        geometry.id = hashString(pChild->Attribute("id"));
        geometry.name = pChild->Attribute("name");

        readGeometry(pChild, geometry);
        m_geometryList.push_back(geometry);
    }
}

void ColladaGeometriesLibrary::readGeometry(TiXmlElement* pNode, Geometry& geometry)
{
    geometry.id = hashString(pNode->Attribute("id"));
    geometry.name = pNode->Attribute("name");
    
    TiXmlElement* pChild = pNode->FirstChild()->ToElement();
    std::string elementName = pChild->Value();

    if (elementName == "asset")
    {
        // TODO implement asset
    }
    else if (elementName == "mesh")
    {
        geometry.mesh = {};

        readMesh(pChild, geometry.mesh);
    }
    else if (elementName == "convex_mesh")
    {
        // TODO implement convex_mesh
    }
    else if (elementName == "spline")
    {
        // TODO implement spline
    }
    else if (elementName == "extra")
    {
        // TODO implement extra
    }
}

void ColladaGeometriesLibrary::readMesh(TiXmlElement* pNode, Mesh& mesh)
{
    TiXmlElement* pMeshElement = nullptr;
    
    for (pMeshElement = pNode->FirstChildElement(); pMeshElement != nullptr; pMeshElement = pMeshElement->NextSiblingElement())
    {
        std::string elementName = pMeshElement->Value();

        if (elementName == "source")
        {
            Source source = {};
            readSource(pMeshElement, source);
            mesh.sourceList.insert(std::make_pair(source.id, source));
        }
        else if (elementName == "vertices")
        {
            readVertices(pMeshElement, mesh.vertices);
        }
        else if (elementName == "triangles")
        {
            Triangles triangles = {};
            readPrimitiveElements(pMeshElement, triangles);
            mesh.primitiveElementList.push_back(triangles);
        }
        //else if (elementName == "lines")
        //{
        //    Lines lines = {};
        //    readPrimitiveElements(pMeshElement, lines);
        //    mesh.primitiveElementList.push_back(lines);
        //}
        //else if (elementName == "linestrips")
        //{
        //    LineStrips lineStrips = {};
        //    readPrimitiveElements(pMeshElement, lineStrips);
        //    mesh.primitiveElementList.push_back(lineStrips);
        //}
        //else if (elementName == "polygons")
        //{
        //    Polygons polygons = {};
        //    readPrimitiveElements(pMeshElement, polygons);
        //    mesh.primitiveElementList.push_back(polygons);
        //}
        //else if (elementName == "polylist")
        //{
        //    PolyList polyList = {};
        //    readPrimitiveElements(pMeshElement, polyList);
        //    mesh.primitiveElementList.push_back(polyList);
        //}
        //else if (elementName == "trifans")
        //{
        //    Trifans trifans = {};
        //    readPrimitiveElements(pMeshElement, trifans);
        //    mesh.primitiveElementList.push_back(trifans);
        //}
        //else if (elementName == "tristrips")
        //{
        //    Tristrips tristrips = {};
        //    readPrimitiveElements(pMeshElement, tristrips);
        //    mesh.primitiveElementList.push_back(tristrips);
        //}
        else if (elementName == "extra")
        {
            Extra extra = {};
            readExtra(pMeshElement, extra);
            mesh.extraList.push_back(extra);
        }
    }
}

void ColladaGeometriesLibrary::readPrimitiveElements(TiXmlElement* pNode, PrimitiveElements& primitiveElement)
{
    // TODO implement readPrimitveElements
    std::string elementName = pNode->Value();

    TiXmlElement* pPrimitiveElement = nullptr;

    if (elementName == "triangles")
    {
        Triangles& triangles = (Triangles&)primitiveElement;

        triangles.materialId = pNode->Attribute("material");
        triangles.count = (Uint32)std::strtol(pNode->Attribute("count"), nullptr, 0);

        for (pPrimitiveElement = pNode->FirstChildElement();
            pPrimitiveElement != nullptr;
            pPrimitiveElement = pPrimitiveElement->NextSiblingElement())
        {
            std::string primitiveElementName = pPrimitiveElement->Value();

            if (primitiveElementName == "input")
            {
                InputShared input = {};

                input.offset = (Uint32)std::strtol(pPrimitiveElement->Attribute("offset"), nullptr, 0);
                input.semantic = pPrimitiveElement->Attribute("semantic");
                input.source = pPrimitiveElement->Attribute("source");

                if (const char* set = pPrimitiveElement->Attribute("set"))
                {
                    input.set = (Uint32)std::strtol(set, nullptr, 0);
                }

                triangles.inputList.push_back(input);
            }
            else if (primitiveElementName == "p")
            {
                std::string elementText = pPrimitiveElement->GetText();
                std::vector<std::string> stringValues = tokenizeString(elementText, " ");

                for (size_t i = 0; i < stringValues.size(); ++i)
                {
                    triangles.primitive.indexList.push_back((Uint32)std::strtol(stringValues[i].c_str(), nullptr, 0));
                }
            }
        }
    }
    else if (elementName == "lines")
    {

    }
    else if (elementName == "linestrips")
    {

    }
    else if (elementName == "polygons")
    {

    }
    else if (elementName == "polylist")
    {

    }
    else if (elementName == "trifans")
    {

    }
    else if (elementName == "tristrips")
    {

    }
}

void ColladaGeometriesLibrary::readExtra(TiXmlElement* pNode, Extra& extra)
{
    // TODO implement readExtra
}

void ColladaGeometriesLibrary::readSource(TiXmlElement* pNode, Source& source)
{
    TiXmlElement* pSourceElement = nullptr;

    source.id = pNode->Attribute("id");

    for (pSourceElement = pNode->FirstChildElement(); pSourceElement != nullptr; pSourceElement = pSourceElement->NextSiblingElement())
    {
        std::string sourceElementName = pSourceElement->Value();

        if (sourceElementName == "asset")
        {
            // TODO implement asset
        }
        else if (sourceElementName == "float_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::FLOAT_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Float32* floatValues = new Float32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                floatValues[i] = strtof(stringValues[i].c_str(), 0);
            }

            source.arrayElement.arrayList = (void*)floatValues;
        }
        else if (sourceElementName == "IDREF_array" || sourceElementName == "Name_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = sourceElementName == "IDREF_array" ? ArrayElementType::IDREF_ARRAY : ArrayElementType::NAME_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Int64* hashValues = new Int64[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                hashValues[i] = hashString(stringValues[i].c_str());
            }

            source.arrayElement.arrayList = (void*)hashValues;
        }
        else if (sourceElementName == "bool_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::BOOL_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Bool32* boolValues = new Bool32[source.arrayElement.count];
            
            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                boolValues[i] = stringValues[i] == "true" ? true : false;
            }

            source.arrayElement.arrayList = (void*)boolValues;
        }
        else if (sourceElementName == "int_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::INT_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Int32* intValues = new Int32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                intValues[i] = (Int32)std::strtol(stringValues[i].c_str(), nullptr, 0);
            }

            source.arrayElement.arrayList = (void*)intValues;
        }
        else if (sourceElementName == "technique_common")
        {
            // TODO implement technique_common
        }
    }
}

void ColladaGeometriesLibrary::readPrimitive(TiXmlElement* pNode, Primitive& primitive)
{
    // TODO implement readPrimitive
}

void ColladaGeometriesLibrary::readVertices(TiXmlElement* pNode, Vertices& vertices)
{
    TiXmlElement* pVerticesElement = nullptr;

    vertices.id = hashString(pNode->Attribute("id"));
    //vertices.name = pNode->Attribute("name");

    for (pVerticesElement = pNode->FirstChildElement(); pVerticesElement != nullptr; pVerticesElement = pVerticesElement->NextSiblingElement())
    {
        std::string elementName = pVerticesElement->Value();

        if (elementName == "input")
        {
            InputUnshared input = {};

            input.semantic = pVerticesElement->Attribute("semantic");
            input.source = pVerticesElement->Attribute("source");

            vertices.inputList.push_back(input);
        }
        else if (elementName == "extra")
        {

        }
    }
}
