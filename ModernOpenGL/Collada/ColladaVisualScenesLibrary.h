#pragma once

#include <string>
#include <tinyxml.h>

#include "ColladaDataTypes.h"

typedef std::vector<VisualScene> VisualSceneList;

class ColladaVisualScenesLibrary
{
public:
    ColladaVisualScenesLibrary();
    ~ColladaVisualScenesLibrary();

    void readLibraryVisualScenes(TiXmlElement* pNode);
    void readVisualScene(TiXmlElement* pNode, VisualScene& visualScene);
    void readNode(TiXmlElement* pNode, Node& node);
    void readExtra(TiXmlElement* pNode, Extra& extra);

    VisualSceneList& getVisualSceneList() { return m_visualSceneList; }

private:
    VisualSceneList m_visualSceneList;
};

