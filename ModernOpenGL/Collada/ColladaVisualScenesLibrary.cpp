#include "ColladaVisualScenesLibrary.h"


ColladaVisualScenesLibrary::ColladaVisualScenesLibrary()
{
}


ColladaVisualScenesLibrary::~ColladaVisualScenesLibrary()
{
}

void ColladaVisualScenesLibrary::readLibraryVisualScenes(TiXmlElement* pNode)
{
    TiXmlElement* pChild = pNode->FirstChildElement();

    for (pChild = pNode->FirstChildElement(); pChild != nullptr; pChild = pChild->NextSiblingElement())
    {    
        std::string elementName = pChild->Value();

        if (elementName == "visual_scene")
        {
            VisualScene visualScene = {};

            readVisualScene(pChild, visualScene);
            m_visualSceneList.push_back(visualScene);
        }
    }
}

void ColladaVisualScenesLibrary::readVisualScene(TiXmlElement* pNode, VisualScene& visualScene)
{
    TiXmlElement* pVisualSceneElement = nullptr;

    for (pVisualSceneElement = pNode->FirstChildElement(); pVisualSceneElement != nullptr; pVisualSceneElement = pVisualSceneElement->NextSiblingElement())
    {
        std::string elementName = pVisualSceneElement->Value();

        if (elementName == "node")
        {
            Node node = {};
            
            const char* id = pVisualSceneElement->Attribute("id");
            node.id = id ? id : "";
            
            const char* name = pVisualSceneElement->Attribute("name");
            node.name = name ? name : "";

            const char* sid = pVisualSceneElement->Attribute("sid");
            node.sid = sid ? hashString(sid) : 0;

            const char* type = pVisualSceneElement->Attribute("type");
            node.type = type ? type : "";

            readNode(pVisualSceneElement, node);
            
            visualScene.nodeList.push_back(node);
        }
        else if (elementName == "extra")
        {
            Extra extra = {};
            
            readExtra(pVisualSceneElement, extra);
            visualScene.extraList.push_back(extra);
        }
    }
}

void ColladaVisualScenesLibrary::readNode(TiXmlElement* pNode, Node& node)
{
    TiXmlElement* pNodeElement = nullptr;

    for (pNodeElement = pNode->FirstChildElement(); pNodeElement != nullptr; pNodeElement = pNodeElement->NextSiblingElement())
    {
        std::string elementName = pNodeElement->Value();

        if (elementName == "node")
        {
            Node localNode = {};

            const char* id = pNodeElement->Attribute("id");
            localNode.id = id ? id : "";

            const char* name = pNodeElement->Attribute("name");
            localNode.name = name ? name : "";

            const char* sid = pNodeElement->Attribute("sid");
            localNode.sid = sid ? hashString(sid) : 0;

            const char* type = pNodeElement->Attribute("type");
            localNode.type = type ? type : "";

            readNode(pNodeElement, localNode);
            node.nodeList.push_back(localNode);
        }
        else if (elementName == "matrix")
        {
            
            std::string elementText = pNodeElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            float values[16];

            for (size_t i = 0; i < 16; ++i)
            {
                values[i] = strtof(stringValues[((i % 4) * 4) + (i >> 2)].c_str(), 0);
            }

            chs::Matrix4x4 matrix(values);

            node.matrixList.push_back(matrix);
        }
        else if (elementName == "extra")
        {
            Extra extra = {};

            readExtra(pNodeElement, extra);
            node.extraList.push_back(extra);
        }
    }
}

void ColladaVisualScenesLibrary::readExtra(TiXmlElement* pNode, Extra& extra)
{

}
