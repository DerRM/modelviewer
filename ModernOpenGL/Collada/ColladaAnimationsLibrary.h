#pragma once

#include <vector>
#include <string>
#include <tinyxml.h>

#include "../Common.h"
#include "ColladaDataTypes.h"

typedef std::vector<Animation> AnimationList;

class ColladaAnimationsLibrary
{
public:
    ColladaAnimationsLibrary();
    ~ColladaAnimationsLibrary();

    void readLibraryAnimation(TiXmlElement* pNode);
    void readAnimation(TiXmlElement* pNode, Animation& animation);
    void readSource(TiXmlElement* pNode, Source& source);
    void readSampler(TiXmlElement* pNode, Sampler& sampler);
    void readChannel(TiXmlElement* pNode, Channel& channel);

    AnimationList& getAnimationList() { return m_animationList; }

private:
    AnimationList m_animationList;
};

