#include "ColladaControllersLibrary.h"


ColladaControllersLibrary::ColladaControllersLibrary()
{
}


ColladaControllersLibrary::~ColladaControllersLibrary()
{
}

void ColladaControllersLibrary::readLibraryControllers(TiXmlElement * pNode)
{
    TiXmlElement* pChild = pNode->FirstChildElement();
    
    for (pChild = pNode->FirstChildElement(); pChild != nullptr; pChild = pChild->NextSiblingElement())
    {
        std::string elementName = pChild->Value();

        if (elementName == "controller")
        {
            Controller controller = {};

            readController(pChild, controller);
            m_controllerList.push_back(controller);
        }
    }
}

void ColladaControllersLibrary::readController(TiXmlElement * pNode, Controller & controller)
{
    controller.id = pNode->Attribute("id");

    TiXmlElement* pControllerElement = nullptr;

    for (pControllerElement = pNode->FirstChildElement(); pControllerElement != nullptr; pControllerElement = pControllerElement->NextSiblingElement())
    {
        std::string elementName = pControllerElement->Value();

        if (elementName == "skin")
        {
            Skin skin = {};

            readSkin(pControllerElement, skin);
            controller.skin = skin;
        }
        else if (elementName == "asset")
        {
            // TODO implement asset
        }
        else if (elementName == "extra")
        {
            // TODO implement extra
        }
    }
}

void ColladaControllersLibrary::readSkin(TiXmlElement * pNode, Skin & skin)
{
    TiXmlElement* pSkinElement = nullptr;

    for (pSkinElement = pNode->FirstChildElement(); pSkinElement != nullptr; pSkinElement = pSkinElement->NextSiblingElement())
    {
        std::string elementName = pSkinElement->Value();

        if (elementName == "source")
        {
            Source source = {};

            readSource(pSkinElement, source);
            skin.sourceList.push_back(source);
        }
        else if (elementName == "bind_shape_matrix")
        {
            std::string elementText = pSkinElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            Float32 values[16];

            for (size_t i = 0; i < 16; ++i)
            {
                values[i] = strtof(stringValues[((i % 4) * 4) + (i >> 2)].c_str(), 0);
            }

            chs::Matrix4x4 matrix(values);

            skin.bindShapeMatrix = matrix;
        }
        else if (elementName == "joints")
        {
            Joints joints = {};

            readJoints(pSkinElement, joints);
            skin.joints = joints;
        }
        else if (elementName == "vertex_weights")
        {
            VertexWeights vertexWeights = {};

            vertexWeights.count = std::strtol(pSkinElement->Attribute("count"), 0, 0);

            readVertexWeights(pSkinElement, vertexWeights);
            skin.vertexWeights = vertexWeights;
        }
        else if (elementName == "extra")
        {
            // TODO implement extra
        }
    }
}

void ColladaControllersLibrary::readSource(TiXmlElement * pNode, Source & source)
{
    TiXmlElement* pSourceElement = nullptr;

    source.id = pNode->Attribute("id");

    for (pSourceElement = pNode->FirstChildElement(); pSourceElement != nullptr; pSourceElement = pSourceElement->NextSiblingElement())
    {
        std::string sourceElementName = pSourceElement->Value();

        if (sourceElementName == "asset")
        {
            // TODO implement asset
        }
        else if (sourceElementName == "float_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::FLOAT_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Float32* floatValues = new Float32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                floatValues[i] = strtof(stringValues[i].c_str(), 0);
            }

            source.arrayElement.arrayList = (void*)floatValues;
        }
        else if (sourceElementName == "IDREF_array" || sourceElementName == "Name_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = sourceElementName == "IDREF_array" ? ArrayElementType::IDREF_ARRAY : ArrayElementType::NAME_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Int64* hashValues = new Int64[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                hashValues[i] = hashString(stringValues[i].c_str());
            }

            source.arrayElement.arrayList = (void*)hashValues;
        }
        else if (sourceElementName == "bool_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::BOOL_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Bool32* boolValues = new Bool32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                boolValues[i] = stringValues[i] == "true" ? true : false;
            }

            source.arrayElement.arrayList = (void*)boolValues;
        }
        else if (sourceElementName == "int_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::INT_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Int32* intValues = new Int32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                intValues[i] = std::strtol(stringValues[i].c_str(), nullptr, 0);
            }

            source.arrayElement.arrayList = (void*)intValues;
        }
        else if (sourceElementName == "technique_common")
        {
            // TODO implement technique_common
        }
    }
}

void ColladaControllersLibrary::readJoints(TiXmlElement * pNode, Joints & joints)
{
    TiXmlElement* pJointElement = nullptr;

    for (pJointElement = pNode->FirstChildElement(); pJointElement != nullptr; pJointElement = pJointElement->NextSiblingElement())
    {
        std::string elementName = pJointElement->Value();

        if (elementName == "input")
        {
            InputUnshared input = {};

            input.semantic = pJointElement->Attribute("semantic");
            input.source = pJointElement->Attribute("source");

            joints.inputList.push_back(input);
        }
        else if (elementName == "extra")
        {
            // TODO implement extra
        }
    }
}

void ColladaControllersLibrary::readVertexWeights(TiXmlElement * pNode, VertexWeights & vertexWeights)
{
    TiXmlElement* pVertexWeightsElement = nullptr;
    
    for (pVertexWeightsElement = pNode->FirstChildElement(); pVertexWeightsElement != nullptr; pVertexWeightsElement = pVertexWeightsElement->NextSiblingElement())
    {
        std::string elementName = pVertexWeightsElement->Value();

        if (elementName == "input")
        {
            InputShared input = {};

            input.semantic = pVertexWeightsElement->Attribute("semantic");
            input.source = pVertexWeightsElement->Attribute("source");
            input.offset = (Uint32)std::strtol(pVertexWeightsElement->Attribute("offset"), 0, 0);

            vertexWeights.inputList.push_back(input);
        }
        else if (elementName == "vcount")
        {
            std::vector<Uint32> vCount;

            std::string elementText = pVertexWeightsElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                vCount.push_back((Uint32)std::strtoul(stringValues[i].c_str(), 0, 0));
            }

            vertexWeights.vCount = vCount;
        }
        else if (elementName == "v")
        {
            std::vector<Int32> v;

            std::string elementText = pVertexWeightsElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); i++)
            {
                v.push_back((Int32)std::strtol(stringValues[i].c_str(), 0, 0));
            }

            vertexWeights.v = v;
        }
    }
}
