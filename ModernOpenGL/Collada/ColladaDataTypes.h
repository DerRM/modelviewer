#pragma once

#include <unordered_map>

#include "../Common.h"

typedef struct
{

} Asset;

typedef struct
{
    std::string name;
    std::string type;
} Param;

typedef struct
{
    Uint32 count;
    Uint32 offset;
    Uint32 stride;
    Uint32 padding;
    Uint64 source;
    std::vector<Param> paramList;
} Accessor;

typedef struct
{
    std::string profile;
    std::string xmlNamespace;
} Technique;

typedef struct
{
    Accessor accessor;
} TechniqueCommon;

typedef struct
{
    std::string semantic;
    std::string source;
    Uint32 offset;
    Uint32 set;
} InputShared;

typedef struct
{
    std::string semantic;
    std::string source;
} InputUnshared;

typedef struct
{
    Asset asset;
    char padding[7];
    std::vector<Technique> techniqueList;
} Extra;

typedef struct
{
    std::vector<Uint32> indexList;
} Primitive;

typedef struct
{
    std::vector<Uint32> polygonIndexList;
    std::vector<std::vector<Uint32> > holeIndexList;
} PrimitiveWithHole;

typedef struct
{
    Uint32* vCountList;
} VCount;

enum ArrayElementType
{
    FLOAT_ARRAY = 0,
    INT_ARRAY = 1,
    IDREF_ARRAY = 2,
    NAME_ARRAY = 3,
    BOOL_ARRAY = 4
};

struct ArrayElement
{
    ArrayElement() : count(0), id(nullptr), name("") {}

    Uint32 count;
    ArrayElementType type;
    const char* id;
    void* arrayList;
    std::string name;
};

struct IDREFArray : public ArrayElement
{
};

struct NameArray : public ArrayElement
{
} ;

struct BoolArray : public ArrayElement
{
};

struct FloatArray : public ArrayElement
{
    Uint16 digits;
    Uint16 magnitude;
};

struct IntArray : public ArrayElement
{
    Int32 minInclusive;
    Int32 maxInclusive;
};

typedef struct
{
    std::string id;
    std::string name;
    Asset asset;
    char padding[7];
    std::vector<Technique> techniqueList;
    ArrayElement arrayElement;
    TechniqueCommon techniqueCommon;
} Source;

typedef struct
{
    Uint64 id;
    std::string name;
    std::vector<InputUnshared> inputList;
    std::vector<Extra> extraList;
} Vertices;

struct PrimitiveElements
{
    PrimitiveElements() {}
    
    std::string name;
    Uint32 count;
    const char* materialId;
    std::vector<InputShared> inputList;
    std::vector<Extra> extraList;
};

struct Lines : public PrimitiveElements
{
    Primitive primitive;
};

struct LineStrips : public PrimitiveElements
{
    std::vector<Primitive> primitiveList;
};

struct Polygons : public PrimitiveElements
{
    Primitive* primitiveList;
    PrimitiveWithHole* primitiveHList;
};

struct PolyList : public PrimitiveElements
{
    VCount vCount;
    Primitive primitive;
};

struct Triangles : public PrimitiveElements
{
    Primitive primitive;
};

struct Trifans : public PrimitiveElements
{
    Primitive* primitiveList;
};

struct Tristrips : public PrimitiveElements
{
    Primitive* primitiveList;
};

typedef struct
{

} ConvexMesh;

typedef struct
{

} Spline;

typedef struct
{
    std::unordered_map<std::string, Source> sourceList;
    Vertices vertices;
    std::vector<Triangles> primitiveElementList;
    std::vector<Extra> extraList;
} Mesh;

typedef struct
{
    Uint64 id;
    std::string name;
    Mesh mesh;
} Geometry;

typedef struct
{
    const char* id;
    std::vector<InputUnshared> inputList;
} Sampler;

typedef struct
{
    std::string source;
    std::string target;
} Channel;

typedef struct
{
    const char* id;
    const char* name;
    std::unordered_map<std::string, Source> sourceList;
    std::unordered_map<std::string, Sampler> samplerList;
    std::vector<Channel> channelList;
} Animation;

typedef struct
{
    std::string skeletonUri;
} Skeleton;

typedef struct
{
    std::vector<Param> paramList;
    TechniqueCommon techniqueCommon;
    std::vector<Technique> techniqueList;
    std::vector<Extra> extraList;
} BindMaterial;

typedef struct
{
    std::vector<Skeleton> skeletonList;
    BindMaterial bindMaterial;
    std::vector<Extra> extraList;
} InstanceController;

struct Node
{
    std::string id;
    std::string name;
    Uint64 sid;
    std::string type;
    Asset asset;
    std::vector<InstanceController> instanceControllerList;
    std::vector<chs::Matrix4x4> matrixList;
    std::vector<Node> nodeList;
    std::vector<Extra> extraList;
};

typedef struct
{
    std::string id;
    std::string name;
    std::vector<Node> nodeList;
    std::vector<Extra> extraList;
} VisualScene;

typedef struct
{
    std::vector<InputUnshared> inputList;
    std::vector<Extra> extraList;
} Joints;

typedef struct
{
    Uint32 count;
    std::vector<InputShared> inputList;
    std::vector<Uint32> vCount;
    std::vector<Int32> v;
    std::vector<Extra> extraList;
} VertexWeights;

typedef struct
{
    chs::Matrix4x4 bindShapeMatrix;
    std::vector<Source> sourceList;
    Joints joints;
    VertexWeights vertexWeights;
    std::vector<Extra> extraList;
} Skin;

typedef struct
{

} Morph;

typedef struct
{
    std::string id;
    std::string name;
    Asset asset;
    Skin skin;
    Morph morph;
    std::vector<Extra> extraList;
} Controller;
