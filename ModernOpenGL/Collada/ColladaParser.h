#pragma once

#include <tinyxml.h>
#include <vector>
#include <stack>

#include "../Common.h"
#include "../Resource/Mesh.h"
#include "../Parser.h"

#include "ColladaAnimationsLibrary.h"
#include "ColladaControllersLibrary.h"
#include "ColladaEffectsLibrary.h"
#include "ColladaLightsLibrary.h"
#include "ColladaMaterialsLibrary.h"
#include "ColladaImagesLibrary.h"
#include "ColladaVisualScenesLibrary.h"
#include "ColladaGeometriesLibrary.h"

typedef chs::Vector3 Position;
typedef Vector2 Texcoord2;
typedef chs::Vector3 Normal;
typedef std::vector<std::string> StringArray;
typedef std::vector<Position> PositionArray;
typedef std::vector<Texcoord2> TexCoord2Array;
typedef std::vector<Normal> NormalArray;
typedef std::vector<Uint32> IndexArray;
typedef std::unordered_map<Uint64, chs::Matrix4x4> SkeletonMap;

typedef struct NodeStruct
{
	Uint64 sid;
	NodeStruct* parent;
	chs::Matrix4x4 worldMatrix;
	chs::Matrix4x4 boneMatrix;
	std::vector<NodeStruct*> childList;
	std::string id;
	std::string type;
} NodeStruct;

typedef struct SkeletonStruct
{
	NodeStruct* nodeRoot;
} SkeletonStruct;

typedef struct VertexWeight
{
    std::vector<Uint64> bones;
    std::vector<Float32> weights;
} VertexWeight;

typedef std::vector<VertexWeight> VertexWeightArray;

class ColladaParser : public Parser
{
public:
    ColladaParser();
    ~ColladaParser();

    Bool32 openFile(const char* filename);
    void readFile();
    void closeFile();
    Mesh getMesh() { return m_mesh; }

    PositionArray getSkinningArray() { return m_skinningArray; }
    PositionArray& getPositionArray() { return m_positionArray; }
    NormalArray& getNormalArray() { return m_normalArray; }
    TexCoord2Array& getTexCoordArray() { return m_texCoordArray; }
    IndexArray& getIndexArray() { return m_indexArray; }
	BoneIndexArray& getBoneIndexArray() { return m_boneIndexArray; }
	BoneWeightArray& getBoneWeightArray() { return m_boneWeightArray; }
	std::vector<chs::Matrix4x4>& getBones() { return m_bones; }

    chs::Vector3 getMaxVector() { return m_maxVector; }
    chs::Vector3 getMinVector() { return m_minVector; }

    void makeOpenGlReady();

    void prepareAnimation();
    void prepareSkeleton();
	void prepareSkeletonFrames(Uint32 frameIndex);
    void updateSkeleton(Float64 frameTime);
    void prepareSkinning();
    void applySkinning();
    void prepareSkeletonForOGL();
    void renderSkeleton();

    std::vector<CMesh*>& parseMesh();
private:
    const unsigned int NUM_INDENTS_PER_SPACE = 2;

    TiXmlDocument* m_pDocument;
    ColladaGeometriesLibrary m_geometryLibrary;
    ColladaAnimationsLibrary m_animationLibrary;
    ColladaVisualScenesLibrary m_visualScenesLibrary;
    ColladaControllersLibrary m_controllersLibrary;
    Mesh m_mesh;

    chs::Vector3 m_minVector;
    chs::Vector3 m_maxVector;

    PositionArray m_positionArray;
    TexCoord2Array m_texCoordArray;
    NormalArray m_normalArray;
    IndexArray m_indexArray;
	BoneIndexArray m_boneIndexArray;
	BoneWeightArray m_boneWeightArray;

    StringArray m_stringArray;
    VertexWeightArray m_vertexWeightArray;

    PositionArray m_tempPositionArray;
    TexCoord2Array m_tempTexCoordArray;
    NormalArray m_tempNormalArray;
    VertexWeightArray m_tempVertexWeightArray;
	BoneIndexArray m_tempBoneIndexArray;
	BoneWeightArray m_tempBoneWeightArray;
    PositionArray m_skinningArray;

    void readNode(TiXmlNode* pNode);
    void readGeometry(TiXmlNode* pNode);
    void readMesh(TiXmlNode* pNode);

    uint32_t m_array;
    uint32_t m_buffer;
    std::vector<chs::Vector4> m_lines;
	SkeletonStruct m_skeleton;
	std::vector<SkeletonStruct> m_skeletonList;
	std::vector<chs::Matrix4x4> m_bones;
	std::vector<SkeletonMap> m_skeletonMaps;
	std::vector<std::vector<chs::Matrix4x4>> m_boneLists;

    std::vector<CMesh*> m_meshes;
};

