#include "ColladaAnimationsLibrary.h"


ColladaAnimationsLibrary::ColladaAnimationsLibrary()
{
}


ColladaAnimationsLibrary::~ColladaAnimationsLibrary()
{
}

void ColladaAnimationsLibrary::readLibraryAnimation(TiXmlElement* pNode)
{
    TiXmlElement* pChild = nullptr;

    for (pChild = pNode->FirstChildElement(); pChild != nullptr; pChild = pChild->NextSiblingElement())
    {
        std::string elementName = pChild->Value();

        if (elementName == "animation")
        {
            Animation animation = {};

            readAnimation(pChild, animation);
            m_animationList.push_back(animation);
        }
    }
}

void ColladaAnimationsLibrary::readAnimation(TiXmlElement* pNode, Animation& animation)
{
    animation.id = pNode->Attribute("id");
    
    TiXmlElement* pAnimationElement = nullptr;

    for (pAnimationElement = pNode->FirstChildElement(); pAnimationElement != nullptr; pAnimationElement = pAnimationElement->NextSiblingElement())
    {
        std::string elementName = pAnimationElement->Value();

        if (elementName == "asset")
        {
            // TODO implement asset
        }
        else if (elementName == "animation")
        {
            // TODO implement animation
        }
        else if (elementName == "source")
        {
            Source source = {};

            readSource(pAnimationElement, source);
            animation.sourceList.insert(std::make_pair(source.id, source));
        }
        else if (elementName == "sampler")
        {
            Sampler sampler = {};

            readSampler(pAnimationElement, sampler);
            animation.samplerList.insert(std::make_pair(sampler.id, sampler));
        }
        else if (elementName == "channel")
        {
            Channel channel = {};

            readChannel(pAnimationElement, channel);
            animation.channelList.push_back(channel);
        }
    }
}

void ColladaAnimationsLibrary::readSource(TiXmlElement* pNode, Source& source)
{
    TiXmlElement* pSourceElement = nullptr;

    source.id = pNode->Attribute("id");

    for (pSourceElement = pNode->FirstChildElement(); pSourceElement != nullptr; pSourceElement = pSourceElement->NextSiblingElement())
    {
        std::string sourceElementName = pSourceElement->Value();

        if (sourceElementName == "asset")
        {
            // TODO implement asset
        }
        else if (sourceElementName == "IDREF_array" || sourceElementName == "Name_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = sourceElementName == "IDREF_array" ? ArrayElementType::IDREF_ARRAY : ArrayElementType::NAME_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Int64* hashValues = new Int64[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                hashValues[i] = hashString(stringValues[i].c_str());
            }

            source.arrayElement.arrayList = (void*)hashValues;
        }
        else if (sourceElementName == "bool_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::BOOL_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Bool32* boolValues = new Bool32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                boolValues[i] = stringValues[i] == "true" ? true : false;
            }

            source.arrayElement.arrayList = (void*)boolValues;
        }
        else if (sourceElementName == "float_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::FLOAT_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Float32* floatValues = new Float32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                floatValues[i] = strtof(stringValues[i].c_str(), 0);
            }

            source.arrayElement.arrayList = (void*)floatValues;
        }
        else if (sourceElementName == "int_array")
        {
            source.arrayElement = {};
            source.arrayElement.type = ArrayElementType::INT_ARRAY;
            source.arrayElement.id = pSourceElement->Attribute("id");
            source.arrayElement.count = (Uint32)std::strtol(pSourceElement->Attribute("count"), nullptr, 0);

            Int32* intValues = new Int32[source.arrayElement.count];

            std::string elementText = pSourceElement->GetText();
            std::vector<std::string> stringValues = tokenizeString(elementText, " ");

            for (size_t i = 0; i < stringValues.size(); ++i)
            {
                intValues[i] = (Int32)std::strtol(stringValues[i].c_str(), nullptr, 0);
            }

            source.arrayElement.arrayList = (void*)intValues;
        }
        else if (sourceElementName == "technique_common")
        {
            // TODO implement technique_common
        }
    }
}

void ColladaAnimationsLibrary::readSampler(TiXmlElement* pNode, Sampler& sampler)
{
    TiXmlElement* pInputElement = nullptr;

    sampler.id = pNode->Attribute("id");

    for (pInputElement = pNode->FirstChildElement(); pInputElement != nullptr; pInputElement = pInputElement->NextSiblingElement())
    {
        InputUnshared input = {};

        input.semantic = pInputElement->Attribute("semantic");
        input.source = pInputElement->Attribute("source");

        sampler.inputList.push_back(input);
    }
}

void ColladaAnimationsLibrary::readChannel(TiXmlElement* pNode, Channel& channel)
{
    channel.source = pNode->Attribute("source");
    channel.target = pNode->Attribute("target");
}
