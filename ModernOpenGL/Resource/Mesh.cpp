#include "Mesh.h"

CMesh::CMesh()
{
}

void CMesh::addSubMesh(SSubMesh *submesh)
{
    m_subMeshes.push_back(submesh);
}

CMesh::~CMesh()
{
    for (size_t submeshIndex = 0; submeshIndex < m_subMeshes.size(); ++submeshIndex)
    {
        delete m_subMeshes[0];
        m_subMeshes[0] = nullptr;
    }
}
