#include "JPEG.h"

Uint8 zigzag[64] = {
    0, 1, 8, 16, 9, 2, 3, 10,
    17, 24, 32, 25, 18, 11, 4, 5,
    12, 19, 26, 33, 40, 48, 41, 34,
    27, 20, 13, 6, 7, 14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36,
    29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46,
    53, 60, 61, 54, 47, 55, 62, 63,
};

JPEG::JPEG()
    : m_imageHeight(0),
    m_imageWidth(0),
    m_pImageData(nullptr),
    m_FilePointer(nullptr),
    m_insertDone(false),
    m_numberOfComponents(0),
    m_bitPosition(0),
    m_currentByte(0),
    m_bitString("")
{
}


JPEG::~JPEG()
{
}

void JPEG::loadFile(const char* fileName)
{
    m_FilePointer = (Uint8*)ResourceLoader::openFile(fileName);

    Uint16 startValue = EndianSwitch16(*(Uint16*)m_FilePointer);

    if (startValue != StartOfImage) return;

    m_FilePointer += sizeof(startValue);

    Uint16 jfifMarker = EndianSwitch16(*(Uint16*)m_FilePointer);

    if (jfifMarker != JFIF) return;

    m_FilePointer += sizeof(jfifMarker);

    //JFIFSegment* pJfifSegment = (JFIFSegment*)m_FilePointer;

    m_FilePointer += sizeof(JFIFSegment);

    Uint16 marker = EndianSwitch16(*(Uint16*)m_FilePointer);

    while (marker != EndOfImage)
    {
        m_FilePointer += sizeof(marker);

        switch (marker)
        {
        case QuantizationTable:
        {
            QuantizationSegment* pQuantizationSegment = (QuantizationSegment*)m_FilePointer;

            Uint16 length = EndianSwitch16(pQuantizationSegment->length);

            m_FilePointer += length;
        
            SQuantizationTable quantizationTable;
            quantizationTable.pValues = &pQuantizationSegment->values[0];
            m_quantizationTables.push_back(quantizationTable);
        }
        break;

        case HuffmanTable:
        {
            HuffmanSegment* pHuffmanSegment = (HuffmanSegment*)m_FilePointer;

            Uint16 length = EndianSwitch16(pHuffmanSegment->length);

            Uint16 numberHuffmanValues = 0;

            for (size_t i = 0; i < 16; i++)
            {
                numberHuffmanValues += pHuffmanSegment->bits[i];
            }

            Uint8* pHuffmanValues = m_FilePointer + sizeof(HuffmanSegment);

            m_FilePointer += length;

            SHuffmanTable huffmanTable;
            huffmanTable.pValues = pHuffmanValues;
            huffmanTable.length = length;
            huffmanTable.pBits = pHuffmanSegment->bits;

            if (pHuffmanSegment->index > 0x0f)
            {
                m_acHuffmanTables.push_back(huffmanTable);
            }
            else
            {
                m_dcHuffmanTables.push_back(huffmanTable);
            }

        }
        break;
        case StartOfFrame:
        {
            SOFSegment* pSofSegment = (SOFSegment*)m_FilePointer;

            m_imageHeight = EndianSwitch16(pSofSegment->y);
            m_imageWidth = EndianSwitch16(pSofSegment->x);

            Uint8* componentPtr = ((Uint8*)pSofSegment) + sizeof(SOFSegment);
            m_numberOfComponents = pSofSegment->numberComponents;

            for (size_t componentIndex = 0; componentIndex < m_numberOfComponents; ++componentIndex)
            {
                Component component;

                component.id = *componentPtr++;
                Uint8 hv = *componentPtr++;
                component.H = (hv >> 4) & 0xf;
                component.V = hv & 0xf;
                component.quantizationTable = *componentPtr++;

                m_compontents.push_back(component);
            }

            Uint16 length = EndianSwitch16(pSofSegment->length);

            m_FilePointer += length;
        }
        break;
        case StartOfScan:
        {
            SOSSegment* pSosSegment = (SOSSegment*)m_FilePointer;

            Uint8* componentPtr = ((Uint8*)pSosSegment) + sizeof(SOSSegment);

            for (size_t componentIndex = 0; componentIndex < pSosSegment->numberComponents; ++componentIndex)
            {
                Uint8 id = *componentPtr++;

                for (size_t i = 0; i < m_compontents.size(); ++i)
                {
                    if (m_compontents[i].id == id)
                    {
                        Uint8 dcac = *componentPtr++;

                        m_compontents[i].dcId = (dcac >> 4) & 0xf;
                        m_compontents[i].acId = dcac & 0xf;
                        break;
                    }
                }
            }

            Uint16 length = EndianSwitch16(pSosSegment->length);

            m_FilePointer += length;
            processHuffmanTable();

            m_pImageData = new Uint8[m_imageHeight * m_imageWidth * 3];
            scanBits(m_FilePointer);

            return;
        }
        break;
        case Comment:
            break;
        }

        marker = EndianSwitch16(*(Uint16*)m_FilePointer);
    }


}

void JPEG::processHuffmanTable()
{
    for (size_t index = 0; index < m_dcHuffmanTables.size(); ++index)
    {
        HuffmanTableMap dcTableMap;
        Uint8* pValues = m_dcHuffmanTables[index].pValues;
        //Uint16 length = m_dcHuffmanTables[index].length;
        Uint8* pBits = m_dcHuffmanTables[index].pBits;

        Uint16 count = 0;

        for (size_t valueIndex = 0; valueIndex < 16; ++valueIndex)
        {
            Uint16 elementCount = pBits[valueIndex];

            for (size_t bitIndex = 0; bitIndex < pBits[valueIndex]; ++bitIndex)
            {
                m_insertDone = false;

                recursiveTree(dcTableMap, std::string("0"), pValues[count], valueIndex, elementCount);
                recursiveTree(dcTableMap, std::string("1"), pValues[count], valueIndex, elementCount);
                ++count;
            }

        }


        m_dcTableMaps.push_back(dcTableMap);
    }

    for (size_t index = 0; index < m_acHuffmanTables.size(); ++index)
    {
        HuffmanTableMap acTableMap;
        Uint8* pValues = m_acHuffmanTables[index].pValues;
        //Uint16 length = m_acHuffmanTables[index].length;
        Uint8* pBits = m_acHuffmanTables[index].pBits;

        Uint16 count = 0;

        for (size_t valueIndex = 0; valueIndex < 16; ++valueIndex)
        {
            Uint16 elementCount = pBits[valueIndex];

            for (size_t bitIndex = 0; bitIndex < pBits[valueIndex]; ++bitIndex)
            {
                m_insertDone = false;

                recursiveTree(acTableMap, std::string("0"), pValues[count], valueIndex, elementCount);
                recursiveTree(acTableMap, std::string("1"), pValues[count], valueIndex, elementCount);
                ++count;
            }

        }


        m_acTableMaps.push_back(acTableMap);
    }
}

void JPEG::recursiveTree(HuffmanTableMap& map, const std::string& key, Uint8 value, size_t depth, Uint16& elementCount)
{
    if (elementCount == 0 || m_insertDone)
    {
        return;
    }

    if (map.count(key) > 0)
    {
        return;
    }
    
    if (depth == 0)
    {
        map.insert(std::make_pair(key, value));
        --elementCount;
        m_insertDone = true;
        return;
    }
    
    --depth;

    recursiveTree(map, key + "0", value, depth, elementCount);
    recursiveTree(map, key + "1", value, depth, elementCount);
}

int dcCount = 0;
Int16 oldValue[3] = { 0, 0, 0 };
Uint8 componentCounter = 0;

void JPEG::scanBits(Uint8* filePointer)
{
    std::string bitString = "";
    
    bool useDc = true;

    //Int16 mcu[64] = {};
    Int16 result[64] = {};

    Int16* mcuList = new Int16[64 * 6];

    memset(mcuList, 0, sizeof(Int16) * 64 * 6);

    Uint8 mcuListCounter = 0;

    Uint8 counter = 0;

    Component component = m_compontents[0];

    Uint8 hv = component.H * component.V;

    Uint32 mcuCount = 0;

    JPEGState state = OK;
    bool testStuff = true;

    for (;;)
    {
        if (useDc)
        {
            state = processDcValues(&filePointer, component.dcId, &mcuList[mcuListCounter * 64]);

            if (state == EOI)
            {
                break;
            }

            //if (state == EOB)
            //{
            //    mcuListCounter = (mcuListCounter + 1) % 6;
            //    continue;
            //}

            oldValue[componentCounter] = mcuList[mcuListCounter * 64];
            useDc = false;
        }
        else
        {
            state = processAcValues(&filePointer, component.acId, &mcuList[mcuListCounter * 64]);

            if (state == EOB)
            {
                useDc = true;
                counter = (counter + 1) % hv;

                mcuListCounter = (mcuListCounter + 1) % 6;

                if (counter == 0)
                {
                    componentCounter = (componentCounter + 1) % m_numberOfComponents;
                    component = m_compontents[componentCounter % m_numberOfComponents];
                    counter = 0;
                    hv = component.H * component.V;
                }

                if (mcuListCounter == 0 /*&& testStuff*/)
                {
                    testStuff = false;

                    for (size_t duIndex = 0; duIndex < 6; ++duIndex)
                    {
                        Uint8 quantIndex = duIndex < 4 ? 0 : duIndex == 4 ? 1 : 2;

                        dctTimesQuantMat(result, &mcuList[duIndex * 64], m_quantizationTables[m_compontents[quantIndex].quantizationTable].pValues);
                        inverseDCT(result);

                        memcpy(&mcuList[duIndex * 64], result, sizeof(Int16) * 64);
                    }

                    size_t widthtOffset = ((mcuCount * 2) % (m_imageWidth / 8));
                    size_t heightOffset = ((mcuCount * 2 * 8) / m_imageWidth) * 2;

                    size_t heightBlocks = (m_imageHeight / 8) - 1;

                    for (int j = 0; j < 8; ++j)
                    {
                        for (int i = 0; i < 8; ++i)
                        {
                            int cx = i / 2;
                            int cy = j / 2;

                            RGB rgb = colorConvert(mcuList[i + j * 8], mcuList[cx + cy * 8 + 4 * 64], mcuList[cx + cy * 8 + 5 * 64]);

                            m_pImageData[blockIndex(widthtOffset, heightBlocks - heightOffset, i, 7 - j)] = rgb.b;
                            m_pImageData[blockIndex(widthtOffset, heightBlocks - heightOffset, i, 7 - j) + 1] = rgb.g;
                            m_pImageData[blockIndex(widthtOffset, heightBlocks - heightOffset, i, 7 - j) + 2] = rgb.r;

                            rgb = colorConvert(mcuList[i + j * 8 + 64], mcuList[cx + 4 + cy * 8 + 4 * 64], mcuList[cx + 4 + cy * 8 + 5 * 64]);

                            m_pImageData[blockIndex(widthtOffset + 1, heightBlocks - heightOffset, i, 7 - j)] = rgb.b;
                            m_pImageData[blockIndex(widthtOffset + 1, heightBlocks - heightOffset, i, 7 - j) + 1] = rgb.g;
                            m_pImageData[blockIndex(widthtOffset + 1, heightBlocks - heightOffset, i, 7 - j) + 2] = rgb.r;

                            rgb = colorConvert(mcuList[i + j * 8 + 2 * 64], mcuList[cx + (cy + 4) * 8 + 4 * 64], mcuList[cx + (cy + 4) * 8 + 5 * 64]);

                            m_pImageData[blockIndex(widthtOffset, heightBlocks - heightOffset - 1, i, 7 - j)] = rgb.b;
                            m_pImageData[blockIndex(widthtOffset, heightBlocks - heightOffset - 1, i, 7 - j) + 1] = rgb.g;
                            m_pImageData[blockIndex(widthtOffset, heightBlocks - heightOffset - 1, i, 7 - j) + 2] = rgb.r;

                            rgb = colorConvert(mcuList[i + j * 8 + 3 * 64], mcuList[cx + 4 + (cy + 4) * 8 + 4 * 64], mcuList[cx + 4 + (cy + 4) * 8 + 5 * 64]);

                            m_pImageData[blockIndex(widthtOffset + 1, heightBlocks - heightOffset - 1, i, 7 - j)] = rgb.b;
                            m_pImageData[blockIndex(widthtOffset + 1, heightBlocks - heightOffset - 1, i, 7 - j) + 1] = rgb.g;
                            m_pImageData[blockIndex(widthtOffset + 1, heightBlocks - heightOffset - 1, i, 7 - j) + 2] = rgb.r;
                        }
                    }

                    ++mcuCount;

                    memset(mcuList, 0, sizeof(Int16) * 64 * 6);
                }
            }
            else if (state == EOI)
            {
                break;
            }
        }
    }

    Log("Dc counter: %d", dcCount);
}

size_t JPEG::blockIndex(size_t xBlock, size_t yBlock, size_t x, size_t y)
{
    size_t resx = xBlock * 8 * 3 + x * 3 + y * m_imageWidth * 3;
    size_t resy = yBlock * 8 * m_imageWidth * 3;

    return resx + resy;
}

JPEGState JPEG::processDcValues(Uint8** filePointer, Uint8 index, Int16* mcu)
{
    for (;;)
    {
        if (**filePointer == 0xff && *((*filePointer) + 1) == 0xd9)
        {
            return EOI;
        }

        std::string& bit = bitString(filePointer);

        if (m_dcTableMaps[index].count(bit) > 0)
        {
            Uint16 value = m_dcTableMaps[index][bit];
            mcu[0] = oldValue[componentCounter];

            bit = "";

            if (value == 0)
            {
                return OK;
            }

            Int16 result = bitScan(filePointer, value);

            mcu[0] += result;

            ++dcCount;

            break;
        }
    }

    return OK;
}

Uint8 offset = 0;

JPEGState JPEG::processAcValues(Uint8** filePointer, Uint8 index, Int16* mcu)
{
    for (;;)
    {
        if (**filePointer == 0xff && *((*filePointer) + 1) == 0xd9)
        {
            return EOI;
        }

        std::string& bit = bitString(filePointer);
       
        if (m_acTableMaps[index].count(bit) > 0)
        {
            Uint16 value = m_acTableMaps[index][bit];

            bit = "";

            if (value == 0x00)
            {
                offset = 0;
                return EOB;
            }

            if (value == 0xf0)
            {
                offset += 16;
                return ZRL;
            }

            Uint8 run = (value >> 4) & 0xf;
            Uint8 size = value & 0xf;

            Int16 result = bitScan(filePointer, size);

            if (run == 0)
            {
                ++offset;
            }
            else
            {
                ++offset;
                offset += run;
            }

            mcu[offset] = result;

            break;
        }
    }

    return OK;
}

Int16 JPEG::bitScan(Uint8** filePointer, Uint16 length)
{
    Uint16 count = 0;

    Int16 value = 0;

    while (count != length)
    {
        m_currentByte = **filePointer;

        value <<= 1;
        value |= (m_currentByte >> (7 - m_bitPosition)) & 0x1;

        m_bitPosition = (m_bitPosition + 1) % 8;

        if (m_bitPosition == 0)
        {
            ++*filePointer;

            if (m_currentByte == 0xff && **filePointer == 0)
            {
                ++*filePointer;
            }
        }

        ++count;
    }

    if (!(value >> (length - 1)))
    {
        Int16 stuff = -(2 << (length - 1));
        value = stuff + value + 1;
    }

    return value;
}

std::string& JPEG::bitString(Uint8** filePointer)
{
    m_currentByte = **filePointer;

    if (m_currentByte & (1 << (7 - m_bitPosition)))
    {
        m_bitString += "1";
    }
    else
    {
        m_bitString += "0";
    }

    m_bitPosition = (m_bitPosition + 1) % 8;

    if (m_bitPosition == 0)
    {
        ++*filePointer;

        if (m_currentByte == 0xff && **filePointer == 0)
        {
            ++*filePointer;
        }
    }

    return m_bitString;
}

#define ONE_SQRT2 0.70710678118f
#define ONE_OVER_SIXTEEN 0.0625f

float alpha(int i)
{
    if (i == 0)
    {
        return ONE_SQRT2;
    }

    return 1.0f;
}

void JPEG::inverseDCT(Int16* result)
{
    Int16 tmp[64] = {};

    for (size_t y = 0; y < 8; ++y)
    {
        for (size_t x = 0; x < 8; ++x)
        {
            tmp[x + y * 8] = (Int16)roundf(0.25f * sum(x, y, result));
        }
    }

    for (size_t i = 0; i < 64; i++)
    {
        Int16 test = tmp[i] + 128;

        result[i] = test < 0 ? 0 : (test > 255) ? 255 : test;
    }
}

float JPEG::sum(size_t x, size_t y, Int16* result)
{
    float sum = 0.0f;

    for (int v = 0; v < 8; ++v)
    {
        for (int u = 0; u < 8; ++u)
        {
            sum += alpha(u) * alpha(v) * result[u + v * 8] *
                cosf(((2.0f * x + 1.0f) * u * (float)M_PI) * ONE_OVER_SIXTEEN) *
                cosf(((2.0f * y + 1.0f) * v * (float)M_PI) * ONE_OVER_SIXTEEN);
        }
    }

    return sum;
}

void JPEG::dctTimesQuantMat(Int16* result, Int16* mcu, Uint8* quantMat)
{
    for (size_t k = 0; k < 64; ++k)
    {
        result[zigzag[k]] = mcu[k] * quantMat[k];
    }
}

float clamp0_255(float value)
{
    return value < 0 ? 0 : value > 255 ? 255 : value;
}

RGB JPEG::colorConvert(Int16 y, Int16 cb, Int16 cr)
{
    RGB rgb;

    //rgb.r = (Uint8)roundf(1.164f * (y - 16)                       + 1.596f * (cr - 128));
    //rgb.g = (Uint8)roundf(1.164f * (y - 16) - 0.392f * (cb - 128) - 0.813f * (cr - 128));
    //rgb.b = (Uint8)roundf(1.164f * (y - 16) + 2.017f * (cb - 128));

    rgb.r = (Uint8)clamp0_255(roundf(y + 1.402f * (cr - 128)));
    rgb.g = (Uint8)clamp0_255(roundf(y - 0.34414f * (cb - 128) - 0.71414f * (cr - 128)));
    rgb.b = (Uint8)clamp0_255(roundf(y + 1.772f * (cb - 128)));

    return rgb;
}

YCbCr JPEG::rgb2ycrcb(int r, int g, int b)
{
    Int16 y = (Int16)roundf(0.299f * r + 0.587f * g + 0.114f * b);
    Int16 cb = (Int16)roundf(128.0f - (0.1687f * r) - (0.3313f * g) + (0.5f * b));
    Int16 cr = (Int16)roundf(128.0f + (0.5f * r) - (0.4187f * g) - (0.0813f * b));
    return {y, cb, cr};
}
