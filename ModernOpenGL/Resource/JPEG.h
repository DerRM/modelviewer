#pragma once

#include <unordered_map>
#include <cstdio>
#include <cstring>
#include "../Common.h"
#include "ResourceLoader.h"

#define EndianSwitch16(x) (((x) << 8) | ((x) >> 8))

enum JPEGMarker
{
    StartOfImage = 0xffd8,
    JFIF = 0xffe0,
    QuantizationTable = 0xffdb,
    HuffmanTable = 0xffc4,
    StartOfFrame = 0xffc0,
    StartOfScan = 0xffda,
    Comment = 0xfffe,
    EndOfImage = 0xffd9
};

enum JPEGState
{
    OK,
    EOB,
    ZRL,
    EOI
};

#pragma pack(push, 1)
struct JFIFSegment
{
    Uint16 length;
    Uint8 identifier[5];
    Uint16 version;
    Uint8 units;
    Uint16 xDensity;
    Uint16 yDensity;
    Uint8 xThumbnail;
    Uint8 yThumbnail;
};

struct SOFSegment
{
    Uint16 length;
    Uint8 samplePrecision;
    Uint16 y;
    Uint16 x;
    Uint8 numberComponents;
};

struct HuffmanSegment
{
    Uint16 length;
    Uint8 index;
    Uint8 bits[16];
};

struct QuantizationSegment
{
    Uint16 length;
    Uint8 precision;
    Uint8 values[64];
};

struct SOSSegment
{
    Uint16 length;
    Uint8 numberComponents;
};
#pragma pack(pop)

struct SHuffmanTable
{
    Uint8* pValues;
    Uint8* pBits;
    Uint16 length;
};

struct SQuantizationTable
{
    Uint8* pValues;
};

struct Component
{
    Uint8 id;
    Uint8 H;
    Uint8 V;
    Uint8 quantizationTable;
    Uint8 dcId;
    Uint8 acId;
};

struct RGB
{
    Uint8 r;
    Uint8 g;
    Uint8 b;
};

struct YCbCr
{
    Int16 y;
    Int16 cb;
    Int16 cr;
};

typedef std::unordered_map<std::string, Uint16> HuffmanTableMap;

extern Uint8 zigzag[64];

class JPEG
{
public:
    JPEG();
    ~JPEG();

    void loadFile(const char* fileName);
    Uint16 GetImageHeight() { return m_imageHeight; }
    Uint16 GetImageWidth() { return m_imageWidth; }
    Uint8* GetImageData() { return m_pImageData; }

private:
    Uint16 m_imageHeight;
    Uint16 m_imageWidth;

    Uint8* m_pImageData;

    Uint8* m_FilePointer;
    bool m_insertDone;

    Uint8 m_numberOfComponents;
    Uint8 m_bitPosition;
    Uint8 m_currentByte;
    std::string m_bitString;

    std::vector<SHuffmanTable> m_acHuffmanTables;
    std::vector<SHuffmanTable> m_dcHuffmanTables;
    std::vector<SQuantizationTable> m_quantizationTables;

    std::vector<HuffmanTableMap> m_acTableMaps;
    std::vector<HuffmanTableMap> m_dcTableMaps;
    std::vector<Component> m_compontents;

    void processHuffmanTable();
    void recursiveTree(HuffmanTableMap& map, const std::string& key, Uint8 value, size_t depth, Uint16& elementCount);
    void scanBits(Uint8* filePointer);

    JPEGState processAcValues(Uint8** filePointer, Uint8 index, Int16* mcu);
    JPEGState processDcValues(Uint8** filePointer, Uint8 index, Int16* mcu);

    Int16 bitScan(Uint8** filePointer, Uint16 length);
    std::string& bitString(Uint8** filePointer);

    void inverseDCT(Int16* result);
    void dctTimesQuantMat(Int16* result, Int16* mcu, Uint8* quantMat);

    float sum(size_t x, size_t y, Int16* mcu);

    RGB colorConvert(Int16 y, Int16 cb, Int16 cr);
    YCbCr rgb2ycrcb(int r, int g, int b);

    size_t blockIndex(size_t xBlock, size_t yBlock, size_t x, size_t y);
};

