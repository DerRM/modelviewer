#pragma once

#include "../Common.h"
#include <iostream>
#include <fstream>

class ResourceLoader
{
public:
    ResourceLoader();
    ~ResourceLoader();

    static Int8* openFile(const char* filename);
};
