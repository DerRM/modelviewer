#ifndef __MESH_H__
#define __MESH_H__

#include "../Common.h"

typedef std::vector<Uint32> MeshIndexBuffer;
typedef std::vector<chs::Vector3> MeshPositionBuffer;
typedef std::vector<chs::Vector3> MeshNormalBuffer;
typedef std::vector<Vector2> MeshTexCoordBuffer;

struct SSubMesh {
    MeshPositionBuffer positions;
    MeshNormalBuffer normals;
    MeshTexCoordBuffer uvs;
    Uint32 indexBufferOffset;
};

class CMesh {

public:
    CMesh();
    ~CMesh();

    void addSubMesh(SSubMesh* submesh);

    inline MeshIndexBuffer& getIndexBuffer();

private:
    MeshIndexBuffer m_indexBuffer;
    std::vector<SSubMesh*> m_subMeshes;
};

MeshIndexBuffer& CMesh::getIndexBuffer()
{
    return m_indexBuffer;
}

#endif
