#include "ResourceLoader.h"

ResourceLoader::ResourceLoader()
{
}


ResourceLoader::~ResourceLoader()
{
}

Int8* ResourceLoader::openFile(const char* filename)
{
    char* result = NULL;
#ifdef _WIN32
    DWORD bytesRead = 0;

    HANDLE fileHandle = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (fileHandle == INVALID_HANDLE_VALUE)
    {
        // TODO: logging
        return NULL;
    }

    LARGE_INTEGER fileSize;
    if (GetFileSizeEx(fileHandle, &fileSize))
    {
        Int8* buffer = (Int8*)VirtualAlloc(NULL, (SIZE_T)fileSize.QuadPart, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

        if (ReadFile(fileHandle, buffer, (DWORD)fileSize.QuadPart, &bytesRead, NULL))
        {
            result = buffer;
        }
    }

    CloseHandle(fileHandle);

#elif defined(__ANDROID__)
    AAssetManager* assetManager = nativeActivity->assetManager;
    AAsset* asset = AAssetManager_open(assetManager, filename, AASSET_MODE_UNKNOWN);
    size_t size = (size_t)AAsset_getLength(asset);
    result = (char*)malloc(size);
    AAsset_read(asset, result, size);
#elif defined(__linux__) || defined(__APPLE__) || defined(__EMSCRIPTEN__)
    std::ifstream ifs(filename, std::ios::in | std::ios::binary);
    ifs.seekg(0, std::ios::end);
    long fileLength = ifs.tellg();

    result = new char[fileLength];
    ifs.seekg(0, std::ios::beg);
    ifs.read(result, fileLength);
    ifs.close();
#endif

    return result;
}
