#pragma once

class IWindow {

public:
    virtual ~IWindow() {}

    typedef void(*reshapeFunc)(int width, int height);

    virtual void createWindow() = 0;
    virtual void hideWindow() = 0;
    virtual void showWindow() = 0;
    virtual void destroyWindow() = 0;
    virtual void* getNativeHandle() = 0;
    virtual const char* getTitle() = 0;
    virtual int getWidth() = 0;
    virtual int getHeight() = 0;  
    virtual void setWidth(int width) = 0;
    virtual void setHeight(int height) = 0;
    virtual void setReshapeFunction(reshapeFunc function) = 0;
    virtual bool shouldClose() = 0;


};
